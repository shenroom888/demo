const http = require('http');   //node 内置模块无需安装
const fs = require('fs');		//node 内置模块无需安装
const path = require('path');

const domain = require('domain');  //错误处理， node 内置模块无需安装
/**---------------基础函数----------------------**/
/*
  1.创建一个对象储存接口,例如：apiObj = {
	'/test':function(){ 
		当请求 .../test 接口时做些什么 
	},
	'/api/userInfo':function(){ },
  }

*/
	const apiObj = {}
	function api(url,fn){
		apiObj[url] = fn
	}

/*
  2.开启一个服务
*/
	http.createServer((req, res) => {
		//设置可跨域的头部
		res.setHeader("Access-Control-Allow-Origin", "*");
	    res.setHeader("Access-Control-Allow-Headers", "X-Requested-With");
	    res.setHeader("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
	    // res.setHeader('content-type','text/html;charset=utf-8')

	    var url = req.url.split('?')[0];
	    var fn = apiObj[url];
	    var d = domain.create();	//创建错误处理对象，与try catch作用类似。
	    if(fn){
	    	d.run(function() {
	    		fn(req, res)        //当接口存在时，执行对应操作
	    	})
			d.once('error', function(err) {   //捕获错误
				console.log('错误：',err)
			});	    	
	    }else{
		    try{  //接口不存在时执行读取文件，如果没有指定，则默认读取 index.html
		        var path = req.url == '/' ? '/index.html' : url;
		        var fileContent = fs.readFileSync(__dirname + path,'binary');
		        res.write(fileContent,'binary');  //返回文件流，如各种文件，图片等等。
		    }catch(e){
		        res.writeHead(404,'not found')
		    }
		    res.end()
	    }
	}).listen(3000);
	console.log('开启服务： http://localhost:3000');
/**------------------end-----------------------**/


//示例：
//自定义接口 http://localhost:3000/edit
api('/edit',(req,res)=>{
	//接受GET传递的数据在 req.url 中提取
	var data = req.url;   

	var data = '';
	//当接受到POST传递的数据时触发，如果数据较大,会多次触发，所以 data +=
	req.on('data',function(chunk){
		//data += chunk.toString();
	})
	//数据接受完成时执行	
	req.on('end',function(){
		//var obj = JSON.parse(data);
		res.end(JSON.stringify({ state:1, msg: 'ok' }));  //返回前端信息
	})
});

//自定义接口 http://localhost:3000/edit/upload 上传图片等
// const formdata = require('formidable');
const multiparty = require('./index');
api('/edit/upload',(req,res)=>{
	res.writeHead(200,{'content-type':'text/html;charset=utf-8'})

	/* 生成multiparty对象，并配置上传目标路径 */
	let form = new multiparty.Form();
	/* 设置编辑 */
	form.encoding = 'utf-8';
	//设置文件存储路劲
	// form.uploadDir = 'www/imgs';
	form.uploadDir = './';
    //设置单文件大小限制 2MB
    form.maxFilesSize = 2 * 1024 * 1024;
    //form.maxFields = 1000;  设置所以文件的大小总和
    form.parse(req, function(err, fields, files) {
    	// console.log(fields)
    	// console.log(files)
        if(err){
            res.end(JSON.stringify({ state: -1, msg: '数据解析失败！'}));
            return;
        }
        if(files == undefined){
        	res.end(JSON.stringify({ state: -1, msg: '没有接受到文件数据！'}));
        	return;
        }  
    	for(var k in files){
    		files[k].forEach((item)=>{ 
    			var destPath = path.resolve(__dirname, form.uploadDir+item.originalFilename);
				fs.rename(item.path, destPath, function (err) {
				  if (err){
				  	res.end(JSON.stringify({ state: -1, msg: '文件保存失败！'}));
				  }else{
				  	res.end(JSON.stringify({ state: -1, msg: '文件上传成功！'}));
				  }
				  	
				})    			
    		})
    	}
    })
});