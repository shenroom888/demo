$.jsname__03 =`
            <h3>八佾篇</h3>
            <div class="yuanwen list fayin" onclick="run(this)">孔子谓季氏<sup yin="">①</sup>，“八佾<sup yin="">①</sup>舞于庭，是可忍也，孰不可忍也？”</div>
            <div class="shuoming list">❶ 季氏：指季子平，鲁国大夫。❷八佾（yì）：古代舞蹈八人为一行，一行叫一佾；天子使用八行共六十四人的舞蹈，称为“八佾”。按规矩，诸侯使用六行，大夫只能使用四行，即三十二人的舞蹈。季氏是大夫，却使用“八佾”，这是僭礼的行为。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子谈到季氏，说：“（季氏作为一个大夫）在自家庭院里按照天子的规格使用六十四人的舞蹈列队，这种事如果可以容忍，那还有什么不可容忍的事呢？”</div>
            <div class="yuanwen list fayin" onclick="run(this)">三家<sup yin="">①</sup>者，以《雍》彻<sup yin="">②</sup>。子曰：“‘相维辟公，天子穆穆’，奚取于三家之堂？”</div>
            <div class="shuoming list">❶三家：指在鲁国当政的仲孙氏，叔孙氏，季孙氏三家。三家都是鲁桓公的后代，故又称“三桓”。 ❷《雍》：《诗经·周颂》中的一篇。彻：同“撤”，撤除。</div>
            <div class="shiyi list fayin" onclick="run(this)">仲孙、叔孙、季孙三家大夫（在祭祀完祖先时，也跟天子祭祀一样）唱着《雍》诗来撤去祭品。孔子（批评）说：“‘助祭的是四方的诸侯，主祭的是庄严肃穆的天子’，这诗怎么能在三家祭祖的庙堂上唱呢？”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“人而不仁，如礼何？人而不仁，如乐何<sup yin="">①</sup>？”</div>
            <div class="shuoming list">❶乐（yuè）：音乐，古代的乐也包括舞蹈。孔子重视乐，认为好的乐有宣泄情感，协调人际关系的功效。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子说：“做人没有仁爱，礼仪怎么能到位？做人没有仁爱，音乐怎么能到位？”</div>
            <div class="yuanwen list fayin" onclick="run(this)">林放问礼之本<sup yin="">①</sup>。子曰：“大哉问！礼，与其奢也，宁<sup yin="另"></sup>俭；丧<sup yin="桑"></sup>，与其易也<sup yin="">②</sup>，宁<sup yin="另"></sup>戚<sup yin="">③</sup>。”</div>
            <div class="shuoming list">❶林放：鲁国人。有人认为是孔子的弟子。本：根本，本质。 ❷易：铺张。 ❸戚：悲伤。</div>
            <div class="shiyi list fayin" onclick="run(this)">林放请教礼制的根本。孔子说：“提了个大问题啊！就一般的礼仪来说，与其大事铺张，不如力求俭朴；至于丧礼，与其过分周到，不如万分哀痛。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“夷狄之有君<sup yin="">①</sup>，不如诸夏之亡<sup yin="">②</sup>也。”</div>
            <div class="shuoming list">❶夷狄：夷、狄是我国古代居住在中原的华夏族统治阶级分别对东方异族和北方异族的蔑称，这里泛指当时各方异族。 ❷亡：同“无”，没有。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子说：“（没有道德礼仪的）蛮夷边鄙之地有君王，不如（有道德礼仪的）中原华夏之地没君王。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">季氏旅<sup yin="">①</sup>于泰山。子谓冉<sup yin="">②</sup>有曰：“女<sup yin="汝"></sup>弗能救与<sup yin="">③</sup>？”对曰：“不能。”子曰：“呜呼！曾谓泰山不如林放乎<sup yin="">④</sup>？”</div>
            <div class="shuoming list">❶旅：祭山。 ❷冉有：姓冉，字子有。孔子的弟子，当时是季氏家臣。 ❸女：同“汝” ，您。救：阻止。与：同“欤”，语气词。❹曾：竟。按周礼规定，天子才有资格祭天下的名山大川，诸侯只有资格祭祀在其封地境内的名山大川。季氏只是鲁国大夫，他去祭泰山是僭礼行为。孔子的这句反问，是说泰山的神灵是不会接受季氏非礼的祭祀的。</div>
            <div class="shiyi list fayin" onclick="run(this)">季氏要去祭祀泰山。孔子对冉有说：“你不能劝阻他吗？”冉有说：“不能。”孔子叹道：“唉，难道说泰山的神灵还不如林放懂礼吗？”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“君子无所争，必也射乎！揖<sup yin="">①</sup>让而升，下而饮。其争也君子。”</div>
            <div class="shuoming list">❶揖：作揖，谦让。这是表示敬意。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子说：“君子没什么好争的，一定要争的话那就比比射箭吧。射手首先相互揖让，登堂射箭；射完后相互作揖下堂，下堂后还要相互敬酒。这种比争是有君子风度的。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子夏问曰：“‘巧笑倩兮，美目盼兮，素以为绚兮’，何谓也？”子曰：“绘事后素。”曰：“礼后<sup yin="">①</sup>乎？”子曰：“起予者商<sup yin="">②</sup>也，始可与言《诗》已矣。”</div>
            <div class="shuoming list">❶礼后：学礼要放在后边。放在什么后边，原文没有说；旧注多认为具有了忠信品质的人，才能谈得上学礼。 ❷商：子夏，姓卜，名商。</div>
            <div class="shiyi list fayin" onclick="run(this)">子夏问道：“‘（《诗经》上说）漂亮的脸笑得美呀，美丽的眼睛，黑白多分明呀，洁白的脂粉更把她妆扮得楚楚动人啊！’这几句是什么意思呢？” 孔子说：“（像绘画一样）先有了白底子，然后才画上画。” 子夏又问：“学礼要放在仁义后边是吗？” 孔子说：“启发我的是卜商啊！现在可以跟你一起谈论《诗经》了。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“夏礼，吾能言之，杞<sup yin="">①</sup>不足征也；殷礼，吾能言之，宋<sup yin="">②</sup>不足征也。文献不足故也。足，则吾能征<sup yin="">③</sup>之矣。”</div>
            <div class="shuoming list">❶杞（qǐ）：古国名，相传其君主是夏禹的后代，其地在今河南杞县。 ❷宋：古国名，相传其君主是商汤的后代，其地在今河南商丘一带。 ❸征：证明，作证。 </div>
            <div class="shiyi list fayin" onclick="run(this)">孔子说：“夏代的礼制，我能说出来，（但它的后代）杞国，不足以作证；殷代的礼制，我也能讲出来，（但它的后代）宋国也不足以作证。这是因为（杞，宋两国现存的）资料和（熟悉历史的）贤人不够的缘故。否则，我就可以拿它们来作证的。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“禘自既灌而往者<sup yin="">①</sup>，吾不欲观之矣<sup yin="">②</sup>。”</div>
            <div class="shuoming list">❶禘（dì）：古代一种祭祀祖先的极其隆重的祭礼，只有天子才能举行。灌：第一次献酒。 ❷吾不欲观之矣：鲁国是周公旦的封地。周公旦死后，周成王追念他为建立周朝所做的重大贡献，特许他的后代用禘礼祭祀他，因此鲁国一直实行禘祭。但到了春秋时，鲁国的禘祭在先君排列次序上，有违反等级名分的做法，所以孔子不想看下去了。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子说：“禘祭大礼，从开头献酒完毕后再往下，我就不想看下去了。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">或问禘之说。子曰：“不知也。知其说者之于天下也，其如示<sup yin="">①</sup>诸斯乎！”指其掌。</div>
            <div class="shuoming list">❶示：同“置”，摆，放置。</div>
            <div class="shiyi list fayin" onclick="run(this)">有人请教禘礼的学问。孔子说：“我不知道。知道这个道理的人治理天下，大概就像把一件东西摆在这里一样容易吧？”一边说，一边指着自己的手掌。</div>
            <div class="yuanwen list fayin" onclick="run(this)">3.12祭如在，祭神如神<sup yin="">①</sup>在。子曰：“吾不与<sup yin="玉">②</sup>祭，如不祭。”</div>
            <div class="shuoming list">❶神：各种存在的精神主宰，包括天神、山川之神、土地之神以及人鬼（祖先神）。 ❷与（yù）：参与。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子祭祀祖先，真如祖先就在眼前；祭祀神灵，真如神灵就在头上。孔子说过：“我如果不亲自参加祭祀（而叫别人代替），那就跟没祭一样。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">王孙贾<sup yin="">①</sup>问曰：“与其媚于奥，宁媚于灶<sup yin="">②</sup>，何谓也？” 子曰：“不然！获罪于天，无所祷也。”</div>
            <div class="shuoming list">❶王孙甲：卫国大夫。 ❷与其媚于奥，宁媚于灶：屋内西南角叫奥，烧火做饭的地方叫灶，古人认为这两处都有神，都要祭祀。奥神比灶神尊贵，但因为灶神直接管人吃饭的大事，所以就有了“与其讨好奥神，宁可讨好灶神”的说法，用来比喻与其讨好地位高的人，不如讨好地位低但有实权的人。</div>
            <div class="shiyi list fayin" onclick="run(this)">王孙贾问道：“俗话说‘与其讨好奥神，不如巴结灶神’，什么意思呢？”孔子说：“不对！如果得罪的上天，就没有可祈祷的地方了。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“周监<sup yin="鉴">①</sup>于二代，郁郁乎文哉！吾从周。”</div>
            <div class="shuoming list">❶监（jian）：同“鉴”，借鉴。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子说：“周代的礼仪制度借鉴了夏，商两代，（制定的礼乐制度）多么丰富多彩啊！我赞成周代（的礼乐制度）。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子入太庙<sup yin="">①</sup>，每事问。或曰：“孰谓鄹人之子<sup yin="">②</sup>知礼乎？入太庙，每事问。” 子闻之，曰：“是礼也。”</div>
            <div class="shuoming list">❶太庙：祭祀开国君主（太祖）的庙，这里指鲁国开国君主周公旦的庙。 ❷鄹人之子：鄹（zōu），鲁国邑名。孔子父亲叔梁纥（hé）曾做过鄹邑大夫，此处指称孔子，含有轻视之意。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子进周公庙，对每件事都要问问。有人便说：“谁说鄹人叔梁纥的儿子懂得礼啊？到了太庙，每件事都要向人请教。” 孔子听后说：“这就是礼啊。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“射不主皮<sup yin="">①</sup>，为力不同科，古之道也。”</div>
            <div class="shuoming list">❶皮：兽皮做成的箭靶子。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子说：“演习射艺，不是非要以穿透靶子为主，因为各人力气不同的缘故，这是自古以来的规矩。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子贡欲去告朔之饩羊<sup yin="">①</sup>。子曰：“赐也！尔爱其羊，我爱其礼<sup yin="">②</sup>。”</div>
            <div class="shuoming list">❶欲：想。去：去掉。告朔之饩（xì）羊：周天子于每年冬季将下年历书颁发于诸侯，诸侯将历书藏于祖庙，每月朔（初一）在祖庙杀羊祭祀，请出历书以示尊行，此礼称“告朔”，所用之羊称“饩羊” 。❷我爱其礼：鲁国自文公后即不行告朔之礼，只在每月朔日于祖庙杀一羊虚应故事。子贡认为鲁公既不行礼，杀羊是浪费；孔子认为礼虽不全，但毕竟还在，如果连羊也不杀，礼便完全没有了。</div>
            <div class="shiyi list fayin" onclick="run(this)">子贡想把每月初一祭祀祖庙时所用的活羊省去不用。孔子说：“赐啊！你爱惜那只羊，我珍惜那种祭礼。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“事<sup yin="">①</sup>君尽礼，人以为谄<sup yin="">②</sup>也。”</div>
            <div class="shuoming list">❶事：事奉，服务于。 ❷谄（chǎn）：谄媚，用卑贱的态度向人讨好，奉承。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子说：“完全按照礼节的规定侍奉君主，别人反认为他是在向君主谄媚讨好呢。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">定<sup yin="">①</sup>公问：“君使臣，臣事君，如之何？”孔子对曰：“君使臣以礼，臣事君以忠。”</div>
            <div class="shuoming list">❶定公：即鲁定公，鲁国国君。</div>
            <div class="shiyi list fayin" onclick="run(this)">鲁定公问：“君王领导臣下，臣下服事君王，该怎么做？”孔子说：“君王领导臣下靠礼，臣下服事君王靠忠。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“《关睢》<sup yin="">①</sup>，乐而不淫<sup yin="">②</sup>，哀而不伤。”</div>
            <div class="shuoming list">❶《关雎》：《诗经·国风》的第一篇。 ❷淫：过分而到了不恰当的地步。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子说：“《关雎》这首诗，欢乐而不放荡，哀愁却不伤痛。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">哀公问社于宰我<sup yin="">①</sup>。宰我对曰：“夏后氏以松，殷人以柏，周人以栗，曰：使民战栗。”子闻之曰：“成事不说，遂事不谏，既往不咎。”</div>
            <div class="shuoming list">❶社：土地神。这里指社主，即代表社神的木牌位。宰我：名予，字子我。孔子弟子。</div>
            <div class="shiyi list fayin" onclick="run(this)">鲁哀公问宰我，应该用哪种木头做社主。宰我回答说：“夏代用的是松木，殷代用的是柏木，周代用的是栗木--用意是要使百姓（害怕的）瑟瑟发抖。” 孔子听到这番话后，（用批评的语气）说：“已经做了的事不必再解释它了，已经做成了的事不必再规劝了，过去了的事就不要再责备了。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“管仲之器小哉<sup yin="">①</sup>！” 或曰：“管仲俭乎？” 曰：“管氏有三归<sup yin="">②</sup>，官事不摄<sup yin="">③</sup>，焉得俭？” “然则管仲知礼乎？” 曰：“邦君树塞门，管氏亦树塞门。邦君为两君之好，有反坫<sup yin="">④</sup>，管氏亦有反坫。管氏而知礼，孰不知礼？”</div>
            <div class="shuoming list">❶管仲：春秋初期政治家。名夷吾，字仲。曾担任齐国的卿，辅佐齐桓公成为春秋时第一个霸主。 ❷三归：三归指市租。 ❸摄：兼职。 ❹反坫（diàn）：坫为土筑设备，供祭祀或宴会时放礼器和酒具的土台子。反坫即反爵之坫，在两楹之间，诸侯若与邻国之君进行友好会见，献酬之礼过后将用过的爵（酒杯）放在坫上。诸侯以上才能享用。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子说：“管仲器量太狭小啊！”有人就问：“管仲俭朴吗？”孔子说：“管仲有大量的市租收入，他手下的官员都各司其职、从不兼职，怎么能算俭朴呢？” “那么管仲懂礼吗？” 孔子说：“国君在宫门前立个屏风，管仲也在自家门前立个照壁。国君为款待外国君主，在堂前设有饮酒台，管仲也设有饮酒台。管仲要是懂礼，还有谁不懂礼呢？”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子语<sup yin="玉"></sup>鲁大<sup yin="太"></sup>师乐<sup yin="月">①</sup>，曰：“乐<sup yin="月"></sup>其可知也：始作，翕<sup yin="">②</sup>如也；从<sup yin="纵">③</sup>之，纯如也，皦如也，绎如也，以成。”</div>
            <div class="shuoming list">❶语（yù）：告诉。大师：乐官之长。大（tai）同“太”。❷翕（xī）：盛。❸从：同“纵”，展示。皦（jiǎo）</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子对鲁国掌管音乐的太师讲解演奏的心法，说：“音乐演奏的过程是可以知道的：开始演奏时，（钟鼓齐鸣）音乐合奏；继而，音调纯正和谐，洪亮清晰，经久不见，由此完成演奏。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">仪封人<sup yin="">①</sup>请见，曰：“君子之至于斯也，吾未尝不得见也。”从者见之。出曰：“二三子何患于丧乎？天下之无道也久矣，天将以夫子为木铎<sup yin="">②</sup>。”</div>
            <div class="shuoming list">❶仪封人：仪，地名，在今河南兰考县境内。封，疆界。封人，管理疆界的官。 ❷木铎：木舌铜铃，古代官员摇木铎召集百姓来听政令训导。</div>
            <div class="shiyi list fayin" onclick="run(this)">仪县的边防官求见孔子，说：“但凡君子到这里来，我没有见不到的。”孔子的随行弟子把他引见给孔子。这人出来后，说：“诸位为什么要为失去官位而忧虑呢？天下失去正道已经很长时间了，上天将要以你们的老师为木铎来澄清政治，号令百姓。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子谓《韶》<sup yin="">①</sup>：“尽美矣！又尽善也！” 谓《武》<sup yin="">②</sup>：“尽美矣！未尽善也！”</div>
            <div class="shuoming list">❶《韶》：舜帝时的乐曲。 ❷《武》：周武王时的乐曲。传说舜以禅让继承尧的帝位，而武王以征伐取代纣的帝位，故孔子认为前者的乐曲尽善尽美，而后者的乐曲尽美而未尽善。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子评论《韶》乐说：“美到极点！而且好到极至！” 评论《武》乐说：“美到极点了！但内容还不十分好。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“居上不宽<sup yin="">①</sup>，为礼不敬<sup yin="">②</sup>，临丧不哀，吾何以观之哉！”</div>
            <div class="shuoming list">❶上：上位，高位。宽：待人宽厚，宽宏大量。 ❷敬：恭敬，郑重，慎重。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子说：“身处高位却不能宽宏大量，行礼时不恭敬严肃，居丧时不悲哀，这些我怎么看得下去呢！”</div>            
        `