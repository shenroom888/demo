$.jsname__15 =`
<h3>卫灵公篇</h3>
<div class="yuanwen list fayin" onclick="run(this)">卫灵公问陈<sup yin="阵">①</sup>于孔子。孔子对曰：“俎豆之事<sup yin="">②</sup>，则尝闻之矣；军旅之事，未之学也。”明日遂行。</div>
<div class="shuoming list">❶陈：同“阵”。❷俎（zǔ）豆之事：俎、豆，都是古代盛食物的器皿，举行礼仪时可用作礼器。俎豆之事，即指礼仪之事。</div>
<div class="shiyi list fayin" onclick="run(this)">卫灵公问孔子如何排兵布阵，孔子说：“祭祀的礼仪，我还听说过一点；军队方面的事，我可从来没学过。”第二天，孔子就离开了卫国。</div>
<div class="yuanwen list fayin" onclick="run(this)">在陈绝粮，从者病<sup yin="">①</sup>，莫能兴<sup yin="">②</sup>。子路愠<sup yin="yùn">yùn</sup>见曰：“君子亦有穷乎？”子曰：“君子固穷<sup yin="">③</sup>，小人穷斯滥矣<sup yin="">④</sup>。”</div>
<div class="shuoming list">❶病：苦，困。这里指饿极了，饿坏了。❷兴：起来，起身。这里指行走。❸固：安守，固守。❹滥：像水一样漫溢、泛滥。比喻人不能约束自己，什么事都干得出来。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子一行在陈国被围困，直到把粮食吃光。随行弟子饿的晕倒了，都爬不起来。子路带着一肚子的不满来见孔子，问道：“君子也有穷困的时候吗？”孔子说：“君子在穷困时能坚守住节操，小人穷困就会为所欲为。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“赐也<sup yin="">①</sup>，女<sup yin="汝">rǔ</sup>以予为多学而识之者与<sup yin="欤"></sup>？”对曰：“然，非与<sup yin="欤"></sup>？”曰：“非也，予一<sup yin="">②</sup>以贯之。”</div>
<div class="shuoming list">❶赐：端木赐，字子贡。❷一：一个基本的原则、思想。孔子这里指的是“忠恕”之道。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“赐啊，你以为我是学得知识多又记得住吗？”子贡说：“对啊，难道不是这样吗？”孔子说：“不是的，我是能够用一个基本思想贯穿学问的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“由，知德者鲜矣。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“仲由呀，懂得‘德’的人确实太少啦！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“无为而治者，其舜也与<sup yin="欤">①</sup>？夫何为哉？恭己正南面而已矣。”</div>
<div class="shuoming list">❶无为而治：传说舜善于任用贤人来管理各方面的事情，所以不需要自己亲自操劳政事而天下太平。这就是“无为而治”的意思。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“自己不做什么而使天下太平的人，大概只有舜吧？他做了些什么呢？只是自己恭敬端正地面朝南，坐在君王的位置上罢了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子张问行。子曰：“言忠信，行笃敬，虽蛮貊<sup yin="">①</sup>之邦，行矣。言不忠信，行不笃敬，虽州里，行乎哉？立则见其参于前也；在舆则见其倚于衡也，夫然后行。”子张书诸绅。</div>
<div class="shuoming list">❶蛮：南蛮，泛指南方边疆少数民族。貊（mò）：北狄，泛指北方边疆少数民族。❷参：本意为直、高。这里引申为像一个高大的东西直立在眼前。</div>
<div class="shiyi list fayin" onclick="run(this)">子张问怎样（使自己的主张）行得通，孔子说：“说话真诚守信，办事厚道谨慎，那么即使到了少数民族的偏僻国家也姓得通。如果说话不真诚守信，办事不厚道谨慎，那么即使在本乡本土，又怎能行得通呢？站着时，似乎就看见‘忠信笃敬’几个字展现在自己面前；坐车时，似乎就看见这几个字呈现在车辕的横木上，做到这样就能使自己的主张处处行得通了。”子张把这些话写在自己的衣带上。</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“直哉史鱼<sup yin="">①</sup>！邦有道，如矢；邦无道，如矢。君子哉蘧<sup yin="qú">qú</sup>伯玉！邦有道，则仕；邦无道，则可卷而怀之。”</div>
<div class="shuoming list">❶史鱼：卫国大夫，姓史，名䲡（qiū），字子鱼。《韩诗外传》记载：史鱼曾多次劝谏卫灵公起用蘧（qú）伯玉，贬退弥子瑕，未被接受。史鱼因此感到没有尽职，临终前告诉儿子不要在正堂上为自己治丧。死后，儿子遵嘱治丧，卫灵公得知此事后便起用了蘧伯玉，贬黜了弥子瑕。史鱼生以身谏，死以尸谏，人们赞扬他正直。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“史鱼是多么正直啊！国家有道时，他像箭一样刚直；国家无道时，还像箭一样刚直。蘧伯玉真是个君子啊！国家有道时，他就出来当官；国家无道时，他就（不做官）收起才能退隐起来”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“可与言而不与言，失人；不可与言而与之言，失言。知<sup yin="智">①</sup>者不失人，亦不失言。”</div>
<div class="shuoming list">❶知：同“智”，智者，聪明人。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“可以和他交谈却不去交谈，这就会失掉人才；不可以和他交谈，却去和他交谈，这是白费口舌。聪明人即不损失人才，又不白费口舌。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“志士仁人，无求生以害仁<sup yin="">①</sup>，有杀身以成仁<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶求生：贪生怕死，为保活命苟且偷生。❷杀身：勇于自我牺牲，为仁义当死而死，心安德全。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“有志之士，仁义之人，绝不会苟且偷生去损害仁义，只会豁出性命去保全仁德。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子贡问为仁。子曰：“工欲善其事<sup yin="">①</sup>，必先利其器<sup yin="">②</sup>。居是邦也，事其大夫之贤者，友其士之仁者。”</div>
<div class="shuoming list">❶善：用作动词。做好，干好，使其完善。❷利：用作动词。搞好，弄好，使其精良。</div>
<div class="shiyi list fayin" onclick="run(this)">子贡问怎样实行仁道。孔子说：“工匠要使他的活儿干得好，必定先要使他的工具顺手、锋利。（要想实行仁德）我们居住在一个国家，就要侍奉那些大夫中的贤者，结交那些士人中的仁者。”</div>
<div class="yuanwen list fayin" onclick="run(this)">颜渊问为邦。子曰：“行夏之时<sup yin="">①</sup>，乘殷之辂<sup yin="lù">②</sup>，服周之冕<sup yin="">③</sup>，乐则《韶》《舞》<sup yin="">④</sup>。放郑声，远佞人，郑声淫，佞人殆<sup yin="">⑤</sup>。”</div>
<div class="shuoming list">❶夏之时：时，指历法。古代以冬至所在月为子月，其下顺次为丑月、寅月等等。三代历法不同，在于各有不同的春正月。周以子月为春正月，殷以丑月为春正月，夏以寅月为春正月。夏历符合四季更迭，便于人们从事农业生产，所以即使在殷、周，夏历仍然实行。❷乘殷之辂（lù）：辂，绑在车辕上用来牵引车子的横木，这里指车子。周制有五辂，玉、金、象、革、木、并多文饰，其中木辂最质朴。殷之辂也是木辂，孔子崇尚质朴，所以主张“乘殷之辂”。❸服周之冕：冕，指礼帽。周代礼帽较前代华贵精致，孔子不反对礼服华美，所以主张“服周之冕”。❹ 《韶》《舞》：《韶》，传说是舜时的乐曲；《舞》，同《武》，传说是周武王时的乐曲。❺殆：危险。</div>
<div class="shiyi list fayin" onclick="run(this)">颜渊问如何治国。孔子说：“用夏代的历法，��商代的车子，戴周代的帽子，音乐就用《韶》曲《舞》曲，不用郑国音乐，疏远花言巧语的人。因为郑国音乐淫秽，花言巧语的人危险。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“人无远虑，必有近忧。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“一个人没有长远的考虑，就一定会有近在眼前的忧患。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“已矣乎，吾未见好德如好色者也。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“算了吧，我没见过喜欢美德如同喜欢美色的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“臧文仲其窃位者与<sup yin="欤">①</sup>？知柳下惠之贤而不与立也<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶臧文仲：即臧孙辰，鲁国大夫。❷柳下惠：鲁国人，以贤著称，姓展，名获，字禽。柳下，封地名；惠，谥号。❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“臧文仲该算是个窃居官位的人吧？明知柳下惠是个贤才，却不举荐人家和自己同朝做官。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“躬自厚而薄责于人<sup yin="">①</sup>，则远怨矣<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶厚：这里指厚责，重责。❷远：远离，避开。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“对自己要多反省责备，对别人要少审查责备，这样做可以避免怨恨了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“不曰‘如之何，如之何’者<sup yin="">①</sup>，吾末如之何也已矣。”</div>
<div class="shuoming list">❶如之何：犹言怎么办。孔子这里的意思是：做事一定要经过深思熟虑，多问几个“该怎么办”。因为只有深谋远虑的人，才能真正想出解决问题的好办法。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“遇事从不说‘怎么办怎么办’的人，我也不知道对他怎么办才好。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“群居终日，言不及义，好行小慧，难矣哉！”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“大家整天呆在一起，言谈从不提到道义，只喜欢耍弄小聪明，这种人是难有作为了！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子义以为质<sup yin="">①</sup>，礼以行之，孙<sup yin="逊">②</sup>以出之，信以成之。君子哉！”</div>
<div class="shuoming list">❶质：本意为本质、质地。引申为基本原则，根本。❷孙：同“逊”。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子把道义看作做人的根本，按照礼的规范去做，用谦逊的言语来表达，用诚实的态度来完成。这才是真君子啊！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子病无能焉<sup yin="">①</sup>，不病人之不己知也。”</div>
<div class="shuoming list">❶病：怕，担心。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子之担心自己没有能力，不担心别人不了解自己。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子疾没<sup yin="mò">mò</sup>世而名不称<sup yin="chèng">chèng</sup>焉<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶没（mò）世：死亡。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子所担心的是直到死时还不被人称道。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子求诸己<sup yin="">①</sup>，小人求诸人。”</div>
<div class="shuoming list">❶求：要求。也有说是求助，求得。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子要求自己，小人要求别人。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子矜而不争<sup yin="">①</sup>，群而不党<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶矜：庄重，矜持，谨慎拘谨。❷党：结党营私，拉帮结伙，搞小宗派。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子端庄自持，不爱争面子；君子合群，不拉帮结派。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子不以言举人，不以人废言。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子不因为一个人话说得动听而举荐他，不因为一个人品德差而鄙弃他正确的话。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子贡问曰：“有一言而可以终身行之者乎？”子曰：“其‘恕’乎！己所不欲，勿施于人。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">子贡问道：“有没有一句话可以终身奉行的呢？”孔子说：“大概是‘恕’吧！自己不乐意的，就不要强加于人。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“吾之于人也，谁毁谁誉<sup yin="">①</sup>？如有所誉者，其有所试矣。斯民也，三代之所以直道而行也。”</div>
<div class="shuoming list">❶毁：诋毁。指称人之恶而失其真。誉：赞誉，溢美。指扬人之善而过其实。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“我对于别人，（何曾出于私心）诋毁过谁，称赞过谁？如果有所称赞，必定是实际检验过（此人是确实值得称赞）的。夏、商、周三代的人都这样，所以三代能在正道上顺利进行。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“吾犹及史之阙文也<sup yin="">①</sup>。有马者借人乘之，今亡矣夫！”</div>
<div class="shuoming list">❶阙：同“缺”。阙文即空缺的字。史官记事，有疑难之处就却出空白以存疑。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“我曾看到过史官因存疑而有空缺的记载，（我又曾听说过）有马的人将马借给别人乘用。这种好风气，现在是没有了！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“巧言乱德。小不忍，则乱大谋。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“花言巧语会败坏人的美德。小事不能容忍，就会坏了大事。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“众恶<sup yin="ｗù">ｗù</sup>之，必察焉；众好<sup yin="hào">hào</sup>之，必察焉。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“大家都讨厌他，（自己不能轻从）必定要亲自明察一下；大家都喜欢他，（自己也不能轻从）必定要亲自明察一下。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“人能弘道<sup yin="">①</sup>，非道弘人。”</div>
<div class="shuoming list">❶弘：弘扬，光大。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“人的才干能使道弘扬，不是道能扩大人的才干。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“过而不改，是谓过矣。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“有个过错而不改正，这才叫真正的过错。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“吾尝终日不食，终夜不寝，以思，无益，不如学也。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“我曾经整天不吃，整夜不睡，一天到晚想啊想，发现没什么长进，不如学点东西。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子谋道不谋食。耕也，馁在其中矣<sup yin="">①</sup>；学也，禄在其中矣<sup yin="">②</sup>。君子忧道不忧贫。”</div>
<div class="shuoming list">❶馁：饥饿。❷禄：做官的俸禄。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子考虑道行问题，不考虑饭碗问题。耕地，免不了挨饿；学道，可以做官得到俸禄。君子担心道行不足，不担生活贫困。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“知<sup yin="智">①</sup>及之，仁不能守之，虽得之，必失之。知<sup yin="智"></sup>及之，仁能守之，不庄以莅<sup yin="">②</sup>之，则民不敬。知<sup yin="智"></sup>及之，仁能守之，庄以莅之，动之不以礼，未善也。”</div>
<div class="shuoming list">❶知：同“智”。❷莅（lì）：临，到。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“凭才智可以谋得职位，但如果不用仁德去保持它，即使得到了，也必定会失掉。凭才智谋得了职位，又能用仁德保持它，但不能以庄重的态度对待百姓，百姓就不会敬重他。凭才智得到官职，仁德足以守住它，又能以庄重得态度对待百姓，但不能按礼的规定使用百姓，仍然不能算好。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子不可小知<sup yin="">①</sup>而可大受也；小人不可大受而可小知也。”</div>
<div class="shuoming list">❶知：主持，主管。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子不可以做小事情却可授予他大责任；小人不可以授予他大责任，却可以做小事情。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“民之于仁也，甚于水火。水火，吾见蹈而死者矣<sup yin="">①</sup>，未见蹈仁而死者也！”</div>
<div class="shuoming list">❶蹈：蹈，踩，投入。引申为追求，实行，实践。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“老百姓对仁德的需要，超过了对水火的需要。我只见过踏进水火而丧生的，从没见过实行仁德而丢命的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“当仁，不让于师。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“在实行仁的问题上，（要抢先去做）同老师也不要谦让。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子贞而不谅<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶贞：正，固守正道，恪守节操。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子一身正气，但不死板。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“事君，敬其事而后其食<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶食：食禄，俸禄，官吏的薪水。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“侍奉国君，要先敬守自己的职责，后考虑享受俸禄。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“有教无类<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶无类：不分类，没有富贵贫贱、天资优劣智愚、等级地位高低、地域远近、善恶不同等等区别与限制。孔子提倡全民教育，希望教育所有的人而同归于善。他的弟子中，富有的（如冉有、子贡），贫穷的（如颜回、原思），地位高的（如孟懿子为鲁国贵族），地位低的（如子路为卞之野人），鲁钝一点的（如曾参），愚笨一点的（如高柴），各种人都有。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“给人人以教育，不要有（贵贱，贫富等）区别。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“道不同<sup yin="">①</sup>，不相为谋。”</div>
<div class="shuoming list">❶道：道路，主张，所追求的目标。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“思想主张不同，不能一起商议谋划事情。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“辞达而已矣。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“言辞只要能表达清楚意思就行了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">师冕见<sup yin="">①</sup>，及阶，子曰：“阶也。”及席，子曰：“席也。”皆坐，子告之曰：“某在斯，某在斯。”师冕出，子张问曰：“与师言之道与<sup yin="欤"></sup>？”子曰：“然，固相<sup yin="xiàng">②</sup>师之道也。”</div>
<div class="shuoming list">❶师冕：师，乐师；这位乐师名冕。古代的乐师一般由盲人担任。❷相（xiàng）：帮助。</div>
<div class="shiyi list fayin" onclick="run(this)">盲人乐师冕来见孔子，走到台阶前，孔子说：“小心台阶。”走到坐席前，孔子说：“这是坐席。”大家都坐下之后，孔子向乐师冕介绍：“某某某坐在哪里，某某某坐在哪里。”乐师冕离开之后。子张问：“这是同盲人乐师谈话的方式吗？”孔子说：“是啊。帮助盲人的方式就是这样的。”</div>

`