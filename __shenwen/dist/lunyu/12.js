$.jsname__12 =`
<h3>颜渊篇</h3>
<div class="yuanwen list fayin" onclick="run(this)">颜渊问仁。子曰：“克己复礼为仁。一日克己复礼，天下归仁焉。为仁由己，而由人乎哉？” 颜渊曰：“请问其目<sup yin="">①</sup>？” 子曰：“非礼勿视，非礼勿听，非礼勿言，非礼勿动。” 颜渊曰：“回虽不敏，请事斯语矣。”</div>
<div class="shuoming list">❶目：纲目，条目，具体要点。</div>
<div class="shiyi list fayin" onclick="run(this)">颜渊问什么是仁，孔子说：“克制自己，言行符合礼就是仁。一旦做到这样，天下就会归于仁德了。修仁德，靠自己，怎么会靠别人呢？”颜渊问：“请问具体怎么做呢？”孔子说：“不合礼的东西不看，不合礼的话不听，不合礼的话不说，不合礼的事不做。”颜渊说：“我虽然愚钝，还是想遵照这句话去做。”</div>
<div class="yuanwen list fayin" onclick="run(this)">仲弓问仁<sup yin="">①</sup>。子曰：“出门如见大宾，使民如承大祭。己所不欲，勿施于人。在邦无怨，在家无怨。”仲弓曰：“雍虽不敏，请事斯语矣。”</div>
<div class="shuoming list">❶仲弓：冉雍，字仲弓。</div>
<div class="shiyi list fayin" onclick="run(this)">仲弓问什么是仁，孔子说：“出门在外，好比拜见稀有贵客，必恭必敬；使用民力，好比承办重大祭祀，诚惶诚恐。自己不乐意的，不要强加于人。在诸侯国任职没有人怨恨你，在卿大夫那里任职没有人怨恨你。”仲弓说：“我虽然迟钝，还是想做到这句话。”</div>
<div class="yuanwen list fayin" onclick="run(this)">司马牛<sup yin="">①</sup>问仁。子曰：“仁者，其言也讱<sup yin="">②</sup>。”曰：“其言也讱，斯谓之仁已乎？”子曰：“为之难，言之得无讱乎？”</div>
<div class="shuoming list">❶司马牛：姓司马，名耕，字子牛。孔子弟子。❷讱（rèn）：不轻易出言，说话谨慎。</div>
<div class="shiyi list fayin" onclick="run(this)">司马牛问什么是仁，孔子说：“仁者说话谨慎。”司马牛又问：“说话谨慎，这就叫仁吗？”孔子说：“事情总是做起来难，说事能不慎重吗？”</div>
<div class="yuanwen list fayin" onclick="run(this)">司马牛问君子。子曰：“君子不忧不惧。”曰：“不忧不惧，斯谓之君子已乎？”子曰：“内省不疚<sup yin="">①</sup>，夫何忧何惧？”</div>
<div class="shuoming list">❶疚：对于自己的错误感到内心惭愧，痛苦不安。</div>
<div class="shiyi list fayin" onclick="run(this)">司马牛问怎样才算是君子，孔子说：“君子不忧愁，不畏惧。”司马牛问：“不忧愁不畏惧，这就叫君子吗？”孔子说：“自我反省，问心无愧，还忧愁什么？畏惧什么？”</div>
<div class="yuanwen list fayin" onclick="run(this)">司马牛忧曰：“人皆有兄弟，我独亡<sup yin="">①</sup>！”子夏曰：“商闻之矣，死生有命，富贵在天。君子敬而无失，与人恭而有礼。四海之内，皆兄弟也，君子何患乎无兄弟也？”</div>
<div class="shuoming list">❶亡：同“无”。</div>
<div class="shiyi list fayin" onclick="run(this)">司马牛忧心忡忡，叹道：“人家都有兄弟，唯独我没有！”子夏开导说：“我听说生死各有命运主宰、富贵都是上天安排的。君子如果内心诚敬，没有过失，对人谦恭有礼，那么天下的人都是你的兄弟姊妹啊，还担心没有兄弟吗？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子张问明。子曰：“浸润之谮<sup yin="">①</sup>，肤受之愬<sup yin="诉">②</sup>，不行焉，可谓明也已矣。浸润之谮，肤受之愬，不行焉，可谓远也已矣。”</div>
<div class="shuoming list">❶谮（zèn）：诬陷、中伤人的谗言。❷愬（sù）：诬告。</div>
<div class="shiyi list fayin" onclick="run(this)">子张问孔子什么是明见远识。孔子说：“如果对慢慢渗透的谗言和劈头盖脸的诽谤，都不被其左右，就可以称得上有明见了。如果对慢慢渗透的谗言和劈头盖脸的诽谤都不在乎，就可以称得上有远见了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子贡问政。子曰：“足食，足兵<sup yin="">①</sup>，民信之矣。”子贡曰：“必不得已而去，于斯三者何先？”曰：“去兵。”子贡曰：“必不得已而去，于斯二者何先？”曰：“去食。自古皆有死，民无信不立。”</div>
<div class="shuoming list">❶兵：兵器，武器。这里指军备。</div>
<div class="shiyi list fayin" onclick="run(this)">子贡问怎么搞政治。孔子说：“粮食充足，兵力充足，百姓信任政府。”子贡问：“如果不得已要去掉一个，先去掉哪一个？”孔子说：“去掉兵足。”子贡问：“如不得已再去掉一个，先去掉哪一个？”孔子说：“去掉粮足。自古以来人总是要死的，政府得不到民众信任那就没法立足了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">棘子成曰<sup yin="">①</sup>：“君子质<sup yin="">②</sup>而已矣，何以文<sup yin="">③</sup>为？”子贡曰：“惜乎，夫子之说君子也，驷不及舌。文犹质也，质犹文也。虎豹之鞟<sup yin="">④</sup>犹犬羊之鞟。”</div>
<div class="shuoming list">❶棘子成：卫国大夫。❷质：质地。此指思想品质。❸文：文采。此指礼节仪式。❹鞟（kuò）：去掉了毛的兽皮。</div>
<div class="shiyi list fayin" onclick="run(this)">棘子成问：“君子只需质朴就行了，为什么还要那些仪式呢？”子贡说：“可惜啊，先生竟这样说君子！四马的飞车也赶不上说话快。思想品德和礼节仪式同等重要。如果把虎豹的皮和犬羊的皮都去掉花纹和色彩，那么这两类皮革就一样了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">哀公问于有若曰：“年饥，用不足，如之何？”有若对曰：“盍彻乎<sup yin="">①</sup>？”曰：“二，吾犹不足，如之何其彻也？”对曰：“百姓足，君孰与不足？百姓不足，君孰与足？”</div>
<div class="shuoming list">❶彻：周代的一种田税制度，以收获量的十分之一作为田税。</div>
<div class="shiyi list fayin" onclick="run(this)">鲁哀公问有若，说：“饥荒之年，国家财用不足，怎么办呢？”有若回答说：“何不按十分之一征税？”哀公说：“按十分之二征税我都不够用，哪还能按十分之一？”有若答道：“如果百姓够用了，您怎么会不够用呢？如果百姓不够用，您又怎么会够用呢？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子张问崇德辨惑。子曰：“主忠信，徙义，崇德也。爱之欲其生，恶之欲其死。既欲其生，又欲其死，是惑也。‘诚不以富，亦<sup yin="祗"></sup>只以异<sup yin="">①</sup>。’”</div>
<div class="shuoming list">❶诚不以富，亦只以异：出自《诗经·小雅·我行其野》，但原句诗意与本章之旨很不契合，孔子引用在此，大约是断章取义，不必拘泥于原意理解。</div>
<div class="shiyi list fayin" onclick="run(this)">子张问如何提高品德，辨别是非。孔子说：“以忠信为宗旨，遵从道义，就能提高品德。如果爱起来希望对方长寿不死，恨起来巴不得人家立即死掉，这样（好恶无常）便是不辨是非。（《诗经》上说）这样做‘肯定不会因此得到好处，只会使人觉得怪异罢了’。”</div>
<div class="yuanwen list fayin" onclick="run(this)">齐景公<sup yin="">①</sup>问政于孔子。孔子对曰：“君君、臣臣、父父、子子。”公曰：“善哉！信如君不君、臣不臣、父不父、子不子，虽有粟，吾得而食诸？”</div>
<div class="shuoming list">❶齐景公：齐庄公异母弟。鲁昭公末年，孔子到齐国时，齐大夫陈氏权势日重，而齐景公爱奢侈，厚赋敛，施重刑，不立太子，不听从晏婴的劝谏，国内政治混乱。所以，当齐景公问政时，孔子作了以上的回答。景公虽然口头上赞许孔子的意见，却未能真正采纳实行，为君而不尽君道，后来齐国终于被陈氏篡夺。</div>
<div class="shiyi list fayin" onclick="run(this)">齐景公问孔子如何治国。孔子说：“国君要像国君的样子，臣子要像臣子的样子，父亲要像父亲的样子，儿子要像儿子的样子。”齐景公说：“好极了！真要是君不像君，臣不像臣，父不像父，子不像子，即使有粮食，我能吃得到吗？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“片言可以折狱者，其由也与<sup yin="欤"></sup>？”子路无宿诺<sup yin="">①</sup>。</div>
<div class="shuoming list">❶子路无宿诺：这一句，朱熹认为是《论语》的编者因上一句的内容而附记在这里的，借以说明子路平时诚实守信，使人受到感化，因而也在他面前讲真话。但唐陆德明《经典释文》说，有将此句另作一章的。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“根据一方的言辞就能判定案情的，大概只有仲由吧！”子路履行诺言，从不拖延（子路为人忠信果决，做事雷厉风行，人们信服他，在他面前不弄虚作假，因此他可以只听一面之词，就可断案）。</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“听讼<sup yin="">①</sup>，吾犹人也，必也使无讼乎。”</div>
<div class="shuoming list">❶听：判断，审理，处理。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“断案子，我和别人差不多，（所不同的是）总要竭力使大家没有官司可打了才满意。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子张问政。子曰：“居之无倦，行之以忠。”</div>
<div class="shiyi list fayin" onclick="run(this)">子张问怎样从事政事，孔子说：“在位不知疲倦，办事忠实可靠。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“博学于文，约之以礼，亦可以弗畔<sup yin="叛">①</sup>矣夫！”</div>
<div class="shuoming list">❷畔：同“叛”，背离。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“广泛地学习文化典籍，并且用礼来约束自己，也就可以做到不背离正道了呀！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子成人之美，不成人之恶。小人反是。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子成全人家的好事，不促成人家的坏事。小人跟这种做法相反。”</div>
<div class="yuanwen list fayin" onclick="run(this)">季康子问政于孔子。孔子对曰：“政者，正也。子帅以正，孰敢不正？”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">季康子问孔子如何从政，孔子回答说：“‘政’这个字的意思就是‘正’。先生自己带头公正、平正，谁敢不公正不平正？”</div>
<div class="yuanwen list fayin" onclick="run(this)">季康子患盗，问于孔子。孔子对曰：“苟子之不欲<sup yin="">①</sup>，虽赏之不窃。”</div>
<div class="shuoming list">❶苟：假如，如果。</div>
<div class="shiyi list fayin" onclick="run(this)">季康子担心自家被盗，请孔子出个主意。孔子回答说：“假如先生自己不贪财，您就是悬大赏让人去偷，也没有人偷窃的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">季康子问政于孔子曰：“如杀无道，以就有道，何如？”孔子对曰：“子为政，焉用杀？子欲善而民善矣。君子之德风，小人之德草。草上之风，必偃<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶偃：扑倒，倒下。</div>
<div class="shiyi list fayin" onclick="run(this)">季康子问孔子如何执政，说：“如果杀掉坏人，以此来使人们走正道，怎么样？”孔子答道：“您治理政事，哪用得着杀人？您想做个德行好的人，百姓的德行自然也会好的。君子的德行好比是风，百姓的德行好比是草。风吹在草上，草必定随风而倒。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子张问：“士何如斯可谓之达矣？”子曰：“何哉，尔所谓达<sup yin="">①</sup>者？”子张对曰：“在邦必闻，在家必闻<sup yin="">②</sup>。”子曰：“是闻也，非达也。夫达也者，质直而好义，察言而观色，虑以下人。在邦必达，在家必达。夫闻也者，色取仁而行违，居之不疑。在邦必闻，在家必闻。”</div>
<div class="shuoming list">❶达：通达，显达，处事通情达理，做官地位显贵。❷闻：有名声，名望。</div>
<div class="shiyi list fayin" onclick="run(this)">子张问：“一个读书人，怎样做才可以叫通达呢？”孔子说：“你所说的通达，是指什么呢？”子张回答说：“在诸侯国任职一定有名望，在卿大夫那里任职也一定有名望。”孔子说：“这叫名望而不叫通达。所谓通达，是品质正直，爱好道义，善于察言观色，对人谦让。（这种人）在诸侯国任职必然会通达，在卿大夫那里任职也必然会通达。至于有名望，表面上装出仁德的样子，而行动上违背仁德，并且以仁人自居还心安理得。（这种人）在诸侯国任职时必定会骗取名望，在卿大夫那里任职时也必定会骗取名望。”</div>
<div class="yuanwen list fayin" onclick="run(this)">樊迟从游于舞雩<sup yin="yú">yú</sup>之下，曰：“敢问崇德，修慝<sup yin="tè">①</sup>，辨惑。”子曰：“善哉问！先事后得，非崇德与<sup yin="欤"></sup>？攻其恶，无攻人之恶，非修慝与<sup yin="欤"></sup>？一朝<sup yin="zhāo">zhāo</sup>之忿，忘其身，以及其亲，非惑与<sup yin="欤"></sup>？”</div>
<div class="shuoming list">❶慝（tè）：邪恶的念头。</div>
<div class="shiyi list fayin" onclick="run(this)">樊迟随同孔子出游到舞雩台下，问道：“请问怎样培养美德、修正错误、明辨是非？”孔子说：“问得好！吃苦在前，享受在后，不就是提高品德了吗？批评自己的缺点，不指责别人的缺点，不就除去邪念了吗？忍不住一时的忿恨，忘了自己的性命安危，以至于连累自己的亲人，这不是糊涂吗？”</div>
<div class="yuanwen list fayin" onclick="run(this)">樊迟问仁。子曰：“爱人。”问知<sup yin="智">①</sup>。子曰：“知人。”樊迟未达。子曰：“举直错<sup yin="措">②</sup>诸枉，能使枉者直。”樊迟退，见子夏曰：“乡也吾见于夫子而问知知<sup yin="智">①</sup>，子曰：‘举直错<sup yin="措"></sup>诸枉，能使枉者直’。何谓也？”子夏曰：“富哉言乎！舜有天下，选于众，举皋陶<sup yin="">③</sup>，不仁者远矣。汤有天下，选于众，举伊尹<sup yin="">④</sup>，不仁者远矣。”</div>
<div class="shuoming list">❶知：同“智”。❷错：同“挫”。❸皋陶（yáo）：传说是舜的贤臣。❹伊尹：传说是汤的臣子。</div>
<div class="shiyi list fayin" onclick="run(this)">樊迟问什么是仁，孔子说：“爱人。”问什么是智，孔子说：“能识别人。”樊迟没明白。孔子又说：“把正直的人提拔出来，使他们的地位在不正直的人之上，能使不正直的人正直起来。”樊迟出门见到子夏，说：“刚才我拜见老师，请教什么是智，老师说‘把正直的人提拔出来，使他们的地位在不正直的人之上，能使不正直的人正直起来。’这话是什么意思呢？”子夏说：“这话含义丰富啊！舜治理天下的时候，在众人中选拔贤人，选了皋陶之后，那些不仁的人就躲得远远的。商汤治理天下的时候，在众人中选拔贤才，选了伊尹之后，不仁的人也躲得远远的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子贡问友。子曰：“忠告而善道<sup yin="导">①</sup>之，不可则止，毋<sup yin="">②</sup>自辱焉。”</div>
<div class="shuoming list">❶道：同“导”，引导，诱导。❷毋：不要。</div>
<div class="shiyi list fayin" onclick="run(this)">子贡问交友之道，孔子说：“（如果朋友有了错误）真心诚意地劝告他，好好地引导他，如果他不听，就应该停止，不要自找侮辱。”</div>
<div class="yuanwen list fayin" onclick="run(this)">曾子曰：“君子以文会友，以友辅仁。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">曾子说：“君子靠文章学问结交朋友，靠朋友的交往来帮助培养仁德。”</div>

`