$.jsname__02 =`
			<h3>为政篇</h3>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“为政以德，譬如北辰，居其所而众星共之<sup yin="拱">①</sup>。”</div>
			<div class="shuoming list">❶共：同“拱”，环绕。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“（国君）依靠品德教化统治国家，他就会像北极星一样，泰然处在自己的位置上，群星环绕在它周围。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“《诗》三百<sup yin="">①</sup>，一言以弊之，曰：‘思无邪<sup yin="">②</sup>。’”</div>
			<div class="shuoming list">❶《诗》三百：《诗经》共三百零五篇，这里说的“三百”，是举其大数。 ❷思无邪：本是《诗经·鲁颂》中的一句，此处孔子引用之。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“《诗经》的三百篇诗，可以用一句话概括其特点，就是：‘内容思想纯正。’”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“道<sup yin="导">①</sup>之以政，齐<sup yin="">②</sup>之以刑，民免<sup yin="">③</sup>而无耻；道<sup yin="导"></sup>之以德，齐之以礼，有耻且格<sup yin="">④</sup>。”</div>
			<div class="shuoming list">❶道：同“导”，治理，引导。 ❷齐：整治，约束，统一。 ❸免：避免，指避免犯错误。 ❹格：正，纠正。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“靠政令来训导百姓，用刑法来规范，百姓只会尽量避免犯罪，但没有羞耻心；用道德来引导，用礼教来规范，百姓不但有羞耻心，并且会真心归服。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“吾十有五而志与学，三十而立，四十而不惑，五十而知天命<sup yin="">①</sup>，六十而耳顺，七十而从心所欲，不逾矩。”</div>
			<div class="shuoming list">❶天命：天是周族的至上神，天命即至上神的旨意、安排。孔子时代，天命思想已经动摇。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“我十五岁时立志做学问；三十岁时按照礼仪要求立足于世；四十岁时便不再有疑惑；五十岁时便懂得了天命；六十岁时能听进去各种不同的意见；七十岁时随心所欲地行事，怎么做都不会超越规矩，法度。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">孟懿子<sup yin="">①</sup>问孝。子曰：“无违。”  樊迟御<sup yin="">②</sup>，子告之曰：“孟孙问孝于我，我对曰：‘无违’。”樊迟曰：“何谓也？”子曰：“生，事之以礼；死，葬之以礼，祭之以礼。”</div>
			<div class="shuoming list"> ❶ 孟懿子：鲁国大夫，姓仲孙，名何忌，“懿”是谥号。❷樊迟：姓樊，名须，字子迟。孔子弟子。</div>
			<div class="shiyi list fayin" onclick="run(this)">孟懿子问什么是孝。孔子说：“不要违背礼的规定。” 樊迟（给孔子）驾车，孔子告诉他说：“孟孙氏问我孝的问题，我回答他说，不要违背礼的规定。” 樊迟问：“您说的是什么意思呢？” 孔子说：“父母在世时，按照礼的规定侍奉他们；父母去世以后按照礼的规定安葬他们，祭祀他们。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">孟武伯<sup yin="">①</sup>问孝。子曰：“父母唯其<sup yin="">②</sup>疾之忧。”</div>
			<div class="shuoming list">❶孟武伯：孟懿子的儿子，名彘（zhì） ,“武”是谥号，“伯”是排行。❷其：有人认为是指父母，有人认为是指子女。本处是指子女。</div>
			<div class="shiyi list fayin" onclick="run(this)">孟武伯问怎样做到孝。孔子说：“对于父母，只让他们担心子女的疾病，其他不用担心就是孝。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子游<sup yin="">①</sup>问孝。子曰：“今之孝者，是谓能养。至于犬马，皆能有养。不敬，何以别乎？”</div>
			<div class="shuoming list">❶子游：姓言，名偃，字子游。孔子弟子。</div>
			<div class="shiyi list fayin" onclick="run(this)">子游问孔子什么是孝。孔子说：“现在说的孝，只是说能做到供养父母。（然而）就是（家里的）狗和马，也能得到人的饲养。如果不敬重父母，那么赡养父母和养狗养马又有什么区别呢？”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子夏问孝。子曰：“色难。有事，弟子服其劳；有酒食，先生馔<sup yin="">①</sup>，曾是以为孝乎？”</div>
			<div class="shuoming list">❶馔（zhuàn）：饮食，吃喝。</div>
			<div class="shiyi list fayin" onclick="run(this)">子夏问怎样才是孝。孔子说：“奉养父母始终和颜悦色是件难事。遇到事情，仅仅替父母做；有酒食让父母享用，但是子女的脸色却很难看，就能算作孝吗？”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“吾与回<sup yin="">①</sup>言终日，不违，如愚。退而省<sup yin="醒"></sup>其私，亦足以发，回也不愚。”</div>
			<div class="shuoming list">❶回：即颜回，字子渊。孔子最赏识的弟子。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“我整天给颜回讲学，他从来不提与我不同的见解，好像很愚笨。过后，我考察他私下的言谈，却（发现他）能发挥我的观点，可见颜回并不愚笨。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“视其所以<sup yin="">①</sup>，观其所由<sup yin="">②</sup>，察其所安<sup yin="">③</sup>。人焉痩<sup yin="">④</sup>哉？人焉痩哉？”</div>
			<div class="shuoming list">❶以：作为、行动。 ❷由：经由、经历。 ❸安：习惯。 ❹廋：隐藏，藏匿。 </div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“要了解一个人，注意他的所作所为，观察他以往的经历，考察他的秉性。其能隐藏到哪里去呢？能隐藏到哪里去呢？”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“温故<sup yin="">①</sup>而知新，可以为师矣。”</div>
			<div class="shuoming list">❶ 故：旧的，原先的。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“温习学过的知识，能从中悟出新的见解来，就可以做别人的老师了。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子不器<sup yin="">①</sup>。”</div>
			<div class="shuoming list">❶器：器具，只有一种固定用途的东西。比喻人只具备一种知识，一种才能，一种技艺。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子不能像器具那样仅有一才一艺。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子贡问君子。子曰：“先行其言而后从之。”</div>
			<div class="shuoming list"> ❶君子：古代有学问有道德有作为的人，人格高尚的人，或有官职、地位高的人都可以称“君子”。</div>
			<div class="shiyi list fayin" onclick="run(this)">子贡问怎样做一个君子。孔子说：“先实践要说的话，做了以后再说出来。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子周而不比，小人<sup yin="">①</sup>比而不周。”</div>
			<div class="shuoming list">❶小人：与君子相对，本指社会地位低下的人，引申为道德水平低下的人。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子（靠忠信）团结人，不结党营私；小人互相勾结，不（靠忠信）团结人。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“学而不思则罔<sup yin="">①</sup>，思而不学则殆<sup yin="">②</sup>。”</div>
			<div class="shuoming list">❶ 罔（wǎng）：同“惘”。迷惑；蒙蔽；无，没有。❷殆：不前，凝滞，有害。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“只学习而不思考，就会受蒙蔽而无收货；只思考而不学习，就会疑惑而无所得。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“攻<sup yin="">①</sup>乎异端，斯害也已。”</div>
			<div class="shuoming list"> ❶攻：专攻，治。异端：事物两端之一，从任何一端看对方都是“异端”。如果专门攻治一端，则无论什么事情都有害处。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“看问题要全面，处理事情要掌握度，治学问要全面通脱，如果只在一个方面下功夫，或只站在一个立场上考虑问题，这样是有危害的。”
			选择</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“由<sup yin="">①</sup>，诲<sup yin="">②</sup>女知之乎？知之为知之，不知为不知，是知也。”</div>
			<div class="shuoming list">❶由：即仲由，字子路。孔子弟子。 ❷诲：教导。 </div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“由呀，我教给你的你懂了吗？懂了就是懂了，不懂就是不懂，这才是真正的智慧呀。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子张<sup yin="">①</sup>学干禄。子曰：“多闻阙疑，慎言其余，则寡尤；多见阙殆，慎行其余，则寡悔。言寡尤，行寡悔，禄在其中矣。”</div>
			<div class="shuoming list">❶子张：姓（zhuān）孙，名师，字子张。孔子弟子。</div>
			<div class="shiyi list fayin" onclick="run(this)">子张向孔子请教求官职俸禄的方法。孔子说：“多听听别人的意见，保留有疑问的地方，其余（有把握的）要谨慎地发表意见，这样就能少犯错误；多看看别人的行事，不稳妥的事不要做，其余（有把握的）事要小心地去做，这样就能减少后悔。说话少犯错误，做事少有后悔，谋求做官的秘诀就在其中了。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">哀公<sup yin="">①</sup>问曰：“何为则民服？” 孔子对曰：“举直错诸枉<sup yin="">②</sup>，则民服；举枉错诸直，则民不服。”</div>
			<div class="shuoming list">❶ 哀公：指鲁哀公，鲁国国君，姓姬，名蒋，“哀”是谥号。❷举：选拔，任用。直：正直。枉：邪曲不正，邪恶。</div>
			<div class="shiyi list fayin" onclick="run(this)">哀公问道：“怎样做才能是百姓服从？” 孔子回答说：“提拔举用正直的人，把他安置在邪恶之人的上面，百姓就会服从；提拔邪恶之人，使他居于正直之人的上面，百姓就不服。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">季康子<sup yin="">①</sup>问：“使民敬、忠以劝<sup yin="">②</sup>，如之何？”子曰：“临之以庄，则敬；孝慈<sup yin="">③</sup>，则忠；举善而教不能，则劝。”</div>
			<div class="shuoming list">❶ 季康子：鲁哀公时的大夫，姓季孙，名肥，“康”是谥号。❷以：连词，和。劝：勤勉。 ❸ 孝慈：孝敬父母，慈爱众人。</div>
			<div class="shiyi list fayin" onclick="run(this)">季康子问道：“要使百姓（对上）恭敬、忠心，而且勤勉，该怎么做？” 孔子说：“当政者对百姓庄重严肃，他们就会对上敬重；对待父母孝顺，百姓就会忠诚；任用品德高尚的人，教育能力差的人，百姓就会勤勉了。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">或<sup yin="">①</sup>谓孔子曰：“子奚<sup yin="">②</sup>不为政？”子曰：“《书》<sup yin="">③</sup>云：‘孝乎惟孝，友于兄弟，施于有政<sup yin="">④</sup>。’是亦为政，奚其为为政<sup yin="">⑤</sup>？”</div>
			<div class="shuoming list">❶或：代词。有人。 ❷奚：疑问词。何，怎么。 ❸《书》：指《尚书》。是商周时期的政治文告和历史资料的汇编。孔子在这里引用的三句，见于伪古文《尚书·君陈》篇。 ❹施：推广，延及，影响于。有：助词，无意义。 ❺奚其为为政：“奚”，为什么。“其”，代词，指做官。“为”，是。“为政”，参与政治。</div>
			<div class="shiyi list fayin" onclick="run(this)">有人问孔子：“您为什么不去做官从政？”孔子说：“《尚书》上说：‘孝敬父母，友爱兄弟，用这种风气影响当政者。’这也就是参与了政事，为什么只有做官才算从政呢？”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“人而无信，不知其可也。大车无<sup yin="">①</sup>，小车无<sup yin="">②</sup>，其何以行之哉？”</div>
			<div class="shuoming list">❶大车：指牛车。 （ní）：古代车两旁有两根长木杠，称为辕，两辕的前端用一根横木联结起来，以便驾牲口。横木与辕是用木销联结起来的，大车用的木销叫，小车用的木销叫（yuè）。❷小车：指马车。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“一个人如果不讲信用，真不知他怎么为人处事呢！（就像）大车少了，小车少了，车子还怎么能走呢？”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子张问：“十世可知也<sup yin="">①</sup>？” 子曰：“殷因于夏礼<sup yin="">②</sup>，所损益<sup yin="">③</sup>，可知也；周因于殷礼，所损益，可知也；其或继周者，虽百世，可知也。”</div>
			<div class="shuoming list">❶世：古时称三十年为一世。这里指朝代。 ❷殷：就是商朝。商朝传至盘庚，从奄迁都于殷，遂称殷。商是国名，殷是国度之名。 ❸损益：减少和增加。</div>
			<div class="shiyi list fayin" onclick="run(this)">子张问道：“十代以后的礼制可以预知吗？” 孔子说：“殷代因袭夏代的礼制，有废除、增加的地方，是可以知道的；周代因袭商朝的礼制，有删除、增加的地方，也是可以知道的；将来假如有继承周代的朝代，即使过了一百年以后，（它的礼制）也是可以推知的。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“非其鬼<sup yin="">①</sup>而祭之，谄也。见义不为，无勇也。”</div>
			<div class="shuoming list">❶鬼：人死后脱离肉体的灵魂，此处指已故祖先。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“不是自己的祖先却去祭祀它，这是谄媚。见了正义的事却不去奋不顾身地做，这是没有勇气。”</div>
			`