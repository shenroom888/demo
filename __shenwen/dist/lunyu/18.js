$.jsname__18 =`
<h3>微子篇</h3>
<div class="yuanwen list fayin" onclick="run(this)">微子去之<sup yin="">①</sup>，箕子为之奴<sup yin="">②</sup>，比干<sup yin="gàn">gàn</sup>谏而死<sup yin="">③</sup>。孔子曰：“殷有三仁焉！”</div>
<div class="shuoming list">❶微子：名启，纣王的哥哥。❷箕（jī）子：纣王的叔叔。他进谏纣王，纣王不听。于是披发佯狂，后被纣王降为奴隶。❸比干（gàn）：纣王叔父，曾多次力谏纣王，纣王恼羞成怒，将比干剖心杀害。</div>
<div class="shiyi list fayin" onclick="run(this)">（纣王无道）微子离他而去，箕子被降为奴隶，比干<sup yin="gàn"></sup>由于竭力进谏而惨死。孔子说：“殷商有三位仁人啊！”</div>
<div class="yuanwen list fayin" onclick="run(this)">柳下惠为士师<sup yin="">①</sup>，三黜<sup yin="">②</sup>。人曰：“子未可以去乎<sup yin="">③</sup>？”曰：“直道而事人，焉往而不三黜？枉道而事人，何必去父母之邦<sup yin="">④</sup>？”</div>
<div class="shuoming list">❶士师：古代掌管司法刑狱的官员。❷三黜（chù）：多次被罢免。“三”，表示多次，不一定只有三次。❸去：离开。❹父母之邦：父母所在之国，即本国，祖国。</div>
<div class="shiyi list fayin" onclick="run(this)">柳下惠担任（鲁国的）法官，多次被罢官。有人对他说：“您不能离开鲁国吗？”柳下惠说：“坚持按正道侍奉君主，到哪里能不遭到多次罢官？如果不按正道侍奉君主，那又何必离开祖国？”</div>
<div class="yuanwen list fayin" onclick="run(this)">齐景公待孔子曰<sup yin="">①</sup>：“若季氏，则吾不能；以季、孟之间待之<sup yin="">②</sup>。”曰：“吾老矣，不能用也。”孔子行。</div>
<div class="shuoming list">❶齐景公：齐国国君。这里的“待孔子”，具体是指赋予孔子职权。❷季、孟之间：季，即季孙氏，鲁国大夫，位在上卿，权力很大。孟，即孟孙氏，鲁国大夫，位在下卿。</div>
<div class="shiyi list fayin" onclick="run(this)">齐景公给孔子定待遇，说：“要是照鲁君给季孙氏的待遇，我做不到；我想用介于季氏和孟孙氏之间的礼遇对待他。”后来又说：“我老了，不能用他了。”孔子于是就离开了齐国。</div>
<div class="yuanwen list fayin" onclick="run(this)">齐人归<sup yin="kuì">kuì</sup>馈女乐<sup yin="yuè">yuè</sup>，季桓子受之<sup yin="">①</sup>，三日不朝，孔子行。</div>
<div class="shuoming list">❶季桓子：鲁国宰相季孙斯，是当时鲁国实际执掌国政的人。</div>
<div class="shiyi list fayin" onclick="run(this)">齐国人赠送了一批歌女，季桓子接受了，连续三天不上朝问政，孔子于是离开了鲁国。</div>
<div class="yuanwen list fayin" onclick="run(this)">楚狂接舆歌而过孔子曰：“凤兮凤兮！何德之衰<sup yin="">①</sup>？往者不可谏，来者犹可追。已而，已而！今之从政者殆而。”孔子下，欲与之言。趋而辟<sup yin="避">避</sup>之，不得与之言。</div>
<div class="shuoming list">❶古代传说，天下太平时凤凰就出现，天下混乱时凤凰就隐去。此处以凤凰喻孔子，批评孔子在天下混乱时隐居是德行差。</div>
<div class="shiyi list fayin" onclick="run(this)">楚国的狂人接舆唱着歌从孔子车驾旁经过，嘴里唱道：“凤凰呀！凤凰呀！你的德行为什么这样衰微？过去的已无法挽回，将来的还来得及改变。算了吧，算了吧，如今从政的人都很危险啊！（怎么能同他们在一起？）”孔子就下了车，想和他谈谈，那个人加快步子避开了，孔子终于没能同他说上话。</div>
<div class="yuanwen list fayin" onclick="run(this)">长沮<sup yin="jū">jū</sup>、桀溺耦而耕<sup yin="">①</sup>，孔子过之，使子路问津焉。长沮<sup yin="jū">jū</sup>曰：“夫执舆者为谁？”子路曰：“为孔丘。”曰：“是鲁孔丘与<sup yin="欤">欤</sup>？”曰：“是也。”曰：“是知津矣。”问于桀溺。桀溺曰：“子为谁？”曰：“为仲由。”曰：“是鲁孔丘之徒与<sup yin="欤">欤</sup>？”对曰：“然。”曰：“滔滔者天下皆是也，而谁以易之？且而与其从辟<sup yin="避">避</sup>人之士也，岂若从辟<sup yin="避">避</sup>世之士哉！”櫌<sup yin="yōu">yōu</sup>而不辍<sup yin="">②</sup>。子路行以告。夫子怃<sup yin="wǔ">wǔ</sup>然曰：“鸟兽不可与同群，吾非斯人之徒与而谁与？天下有道，丘不与易也。”</div>
<div class="shuoming list">❶长沮（cháng jū）：姓名身世不详，大约是两个隐士。耦：两个人在一起耕地。❷櫌（yōu）：种子播下后，用土覆盖。</div>
<div class="shiyi list fayin" onclick="run(this)">长沮、桀溺一起耕地，孔子路过，派子路去问渡口在哪。长沮说：“那位拿着缰绳的是谁？”子路说：“是孔丘。”长沮问：“是鲁国的孔丘吗？”子路说：“是啊。”长沮说：“他是知道渡口在哪里。”子路又问桀溺，桀溺说：“您是谁呢？”子路说：“是仲由。”桀溺说：“是鲁国孔丘的门徒吗？”子路回答说：“是的。”桀溺说：“天下大势，如江河滔滔，泥沙俱下，谁能够改变呢？您与其跟从躲避无道君主的，还不如跟从躲避乱世的啊！”说着继续耕地。子路回来把这些告诉了孔子。孔子心有感触，叹道：“我是无法躲进山林和鸟兽在一起的，我不和天下的人在一起，那和谁在一起呢？天下如果太平了，我孔丘也就不出来和你们一起去改变它了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子路从而后，遇丈人，以杖荷<sup yin="hè">hè</sup>蓧<sup yin="diào">diào</sup>。子路问曰：“子见夫子乎？”丈人曰：“四体不勤，五谷不分，孰为夫子？”植其杖而芸<sup yin="耘">耘</sup>。子路拱而立。止子路宿，杀鸡为黍而食之，见其二子焉。明日，子路行以告。子曰：“隐者也。”使子路反<sup yin="返">返</sup>见之。至则行矣。子路曰：“不仕无义。长幼之节，不可废也；君臣之义如之何其废之？欲洁其身而乱大伦<sup yin="">①</sup>。君子之仕也，行其义也。道之不行，已知之矣。”</div>
<div class="shuoming list">❶子路认为，自己对隐者恭敬有礼，隐者便款待自己，并让儿子出来相见，这说明隐者没有废弃“长幼之别”，但隐者不出仕，却是放弃了臣对君的应尽之责，因而废弃了“君臣之义”。</div>
<div class="shiyi list fayin" onclick="run(this)">子路跟着孔子出游，落在了后面，遇见一位老人用木杖挑着锄草的农具。子路问到：“先生看见我老师吗？”丈人说：“（你这个人呀）四体不劳动，五谷分不清，谁知道哪个是你老师？”说着就竖起拐杖，用蓧子锄草。子路打拱行礼，站到一边。（老人见子路懂礼貌便）留子路过夜，杀鸡煮黄米饭款待他，又叫两个儿子出来相见。第二天，子路上路，（赶上孔子后）如实相告。孔子说：“这是一位隐士。”就派子路回去拜访。到了那里，老人已经出门了。子路说：“有本事却不做官，是不道义的。尊老爱幼的礼节既然不可以废除，忠君礼臣的礼义又怎么可以废掉呢？本想洁身自爱（而隐居不仕），却破坏了（君臣之间）重大的伦理关系。君子出来做官，是实践君臣大义。至于我们的政治主张实现不了，那是早就知道的了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">逸民：伯夷、叔齐、虞仲、夷逸、朱张、柳下惠、少<sup yin="shào">shào</sup>连<sup yin="">①</sup>。子曰：“不降其志，不辱其身，伯夷、叔齐与<sup yin="欤">欤</sup>！”谓“柳下惠、少连，降志辱身矣。言中<sup yin="zhòng">zhòng</sup>伦，行中<sup yin="zhòng">zhòng</sup>虑，其斯而已矣。”谓“虞仲、夷逸，隐居放言，身中<sup yin="zhòng"></sup>清，废中<sup yin="zhòng"></sup>权。我则异于是，无可无不可<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶虞仲、夷逸、朱张、柳下惠、少（shào）连：四人身世不详。❷无可无不可：孟子曾说孔子：“可以速而速，可以久而久，可以处而处，可以仕而仕。”</div>
<div class="shiyi list fayin" onclick="run(this)">自古隐居之士有：伯夷、叔齐、虞仲、夷逸、朱张、柳下惠、少连。孔子说：“不降低自己志向，不辱没自己身份，恐怕是伯夷、叔齐吧！” 又说“柳下惠、少<sup yin="shào"></sup>连降低了志向，辱没了自己。不过说话合乎人伦，做事合乎情理，他们也就是这样罢了”。又说“虞仲、夷逸隐居起来，不谈世事，一身清白，不做官符合权宜之计。我和他们都不同，我没有什么可以，也没有什么不可以。”</div>
<div class="yuanwen list fayin" onclick="run(this)">太师挚适齐<sup yin="">①</sup>，亚饭干<sup yin="gān">gān</sup>适楚<sup yin="">②</sup>，三饭缭适蔡，四饭缺适秦，鼓方叔入于河，播鼗<sup yin="táo">táo</sup>武入于汉<sup yin="">③</sup>，少师阳<sup yin="">④</sup>、击磬襄，入于海。</div>
<div class="shuoming list">❶大师挚：大师，乐官中的领班，“挚”是人名。❷亚饭：古代天子、诸侯吃饭时要奏乐，第二次吃饭时奏乐的乐师叫亚饭乐师。第三次、第四次吃饭时奏乐的乐师，分别为三饭乐师、四饭乐师。“干”和下文的“缭”“缺”都是人名。❸鼗（táo）：一种小鼓，鼓有两耳，系有小槌（chuí），摇动时带动小槌击鼓发声，类似后代的拨浪鼓。“武”是人名。❹少师阳：少师，副乐官，“阳”是人名。磬，古代一种乐器，用石或玉雕成，悬挂于架上，以物击之而发声。“襄”是人名。本章记鲁哀公时，鲁国乐师纷纷离开鲁国流散四方，反映了当时礼坏乐崩的情况。</div>
<div class="shiyi list fayin" onclick="run(this)">太师挚去了齐国，二饭乐师干去了楚国，三饭乐师缭去了蔡国，四饭乐师缺去了秦国，击鼓乐师方叔去了黄河边，摇拨浪鼓的武去了汉水边，少师阳和击磬的襄都去了海边。</div>
<div class="yuanwen list fayin" onclick="run(this)">周公谓鲁公曰<sup yin="">①</sup>：“君子不施<sup yin="驰">驰</sup>其亲<sup yin="">②</sup>，不使大臣怨乎不以；故旧无大过，则不弃也。无求备于一人。”</div>
<div class="shuoming list">❶周公：指周公旦。鲁公：指周公旦的儿子伯禽。本章所记是周公对鲁公的训诫。❷施：同“驰”，松弛。此指疏远，怠慢。</div>
<div class="shiyi list fayin" onclick="run(this)">周公告诉鲁公说：“君子不疏远自己的亲族，不让大臣抱怨得不到重用；老友老臣没有大的过失，就不要抛弃他们。不要求全责备一个人。”</div>
<div class="yuanwen list fayin" onclick="run(this)">周有八士：伯达、伯适<sup yin="kuò">kuò</sup>、仲突、仲忽、叔夜、叔夏、季随、季騧<sup yin="guā">guā</sup>。</div>
<div class="shuoming list">❶旧说这八人是一母所生的四对双生子。一家之中便出了八个名士，可见当时人才之盛。</div>
<div class="shiyi list fayin" onclick="run(this)">周代有八个（著名的）士人：伯达、伯适、仲突、仲忽、叔夜、叔夏、季随、季騧。</div>

`