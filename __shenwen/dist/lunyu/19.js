$.jsname__19 =`
<h3>子张篇</h3>
<div class="yuanwen list fayin" onclick="run(this)">子张曰：“士见危致命<sup yin="">①</sup>，见得思义<sup yin="">②</sup>，祭思敬，丧思哀，其可已矣。”</div>
<div class="shuoming list">❶致命：授命，舍弃生命。❷思：反省，考虑。</div>
<div class="shiyi list fayin" onclick="run(this)">子张说：“一个士人见到危难奋不顾身，见到好处考虑是否符合道义，祭祀充满敬意，丧礼充满哀伤，那就可以了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子张曰：“执德不弘，信道不笃，焉能为有，焉能为亡<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶“焉能”两句：这两句是说这种人的德与道虽有若无。</div>
<div class="shiyi list fayin" onclick="run(this)">子张说：“拥有道德但不能弘扬光大，信仰道义但不能忠诚执着，这种人有他不为多，没他不为少。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子夏之门人问交于子张。子张曰：“子夏云何？”对曰：“子夏曰：‘可者与之，其不可者拒之。’”子张曰：“异乎吾所闻：君子尊贤而容众，嘉善而矜不能<sup yin="">①</sup>。我之大贤与<sup yin="欤">欤</sup>，于人何所不容？我之不贤与<sup yin="欤">欤</sup>，人将拒我，如之何其拒人也？”</div>
<div class="shuoming list">❶矜：怜悯，怜恤，同情。</div>
<div class="shiyi list fayin" onclick="run(this)">子夏的门徒问子张如何交际。子张说：“子夏怎么讲的？”那门徒答道：“子夏说，‘可以交往的就交往，不可交往的就拒绝。’”子张说：“这和我听说的道理不一样：‘君子尊重贤者，同时容得下一般的人，称赞有德才的人，同情没能力的人。’如果我自己是个很好的人，那还有什么人我不能包容的？如果我不是贤人，别人就拒绝和我交往，哪里用得着我去拒绝人家呢？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子夏曰：“虽小道，必有可观者焉，致远恐泥<sup yin="nì">①</sup>，是以君子不为也！”</div>
<div class="shuoming list">❶泥（nì）：留滞，拘泥。</div>
<div class="shiyi list fayin" onclick="run(this)">子夏说：“即便是小技艺，也一定有可取之处，但要想靠它去达到远大的目标，怕是行不通，所以君子不搞那些小技艺。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子夏曰：“日知其所亡<sup yin="">①</sup>，月无忘其所能，可谓好学也已矣。”</div>
<div class="shuoming list">❶亡：同“无”。这里指自己所没有的知识、技能，所不懂得道理等。</div>
<div class="shiyi list fayin" onclick="run(this)">子夏说：“每天学到自己所没有的知识，每月不忘自己已掌握的知识，这样就可以说是好学了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子夏曰：“博学而笃志，切问而近思，仁在其中矣。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">子夏说：“广泛学习，坚定志向，恳切地提出疑问，联系当前情况思考，仁道就在其中了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子夏曰：“百工居肆以成其事<sup yin="">①</sup>，君子学以致其道。”</div>
<div class="shuoming list">❶肆：指作坊，即古代制造物品的场所。</div>
<div class="shiyi list fayin" onclick="run(this)">子夏说：“各类工匠在作坊礼完成他们的工作，君子通过学习来掌握他所追求的道理。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子夏曰：“小人之过也，必文。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">子夏说：“小人对自己的过失，必定会加以掩饰。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子夏曰：“君子有三变：望之俨然，即之也温，听其言也厉。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">子夏说：“君子给人有三种不同的印象：远看他，端庄严肃的样子；接触他，又觉得他温和；听他说话，却又十分严正。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子夏曰：“君子信而后劳其民<sup yin="">①</sup>；未信，则以为厉己也<sup yin="">②</sup>。信而后谏；未信，则以为谤己也。”</div>
<div class="shuoming list">❶劳：指役使，让百姓去服劳役。❷厉：虐待，折磨，坑害。</div>
<div class="shiyi list fayin" onclick="run(this)">子夏说：“君子先要取得百姓信任，然后才能役使百姓；如果还未取得信任（就役使他们），百姓就会认为是在虐待他们。君子先要取得国君信任，然后才能进谏；如果还没取得信任就去进谏，国君就会认为是在毁谤他。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子夏曰：“大德不逾闲<sup yin="">①</sup>，小德出入可也。”</div>
<div class="shuoming list">❶闲：本义是阑，栅栏。引申为限制，界限，法度。</div>
<div class="shiyi list fayin" onclick="run(this)">子夏说：“人在大的节操上不能超越一定界限，小节上有点儿出入是可以的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子游曰：“子夏之门人小子，当洒扫应对进退，则可矣，抑末也。本之则无，如之何？”子夏闻之，曰：“噫！言游过矣！君子之道，孰先传焉？孰后倦焉<sup yin="">①</sup>？譬诸草木，区以别矣。君子之道，焉可诬也？有始有卒者，其惟圣人乎！”</div>
<div class="shuoming list">❶倦：一说，是“传”字之误。</div>
<div class="shiyi list fayin" onclick="run(this)">子游说：“子夏的学生，让他们做些洒水扫地、陪客说话、迎送尊长等事情还可以，不过，这些只是细枝末节的小事。至于礼乐之道这些根本的东西，他们却没有学到，这怎么行呢？。”子夏听了这话，说道：“唉！子游错了！君子的道，哪些先传授，哪些后传授，就好比草木一样，应当区别种类对待。君子的道，哪能（不根据情况）乱传授呢？至于从头至尾完全学通的，大概只有圣人吧！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子夏曰：“仕而优则学<sup yin="">①</sup>，学而优则仕。”</div>
<div class="shuoming list">❶优：知足，富裕。此指有余力。</div>
<div class="shiyi list fayin" onclick="run(this)">子夏说：做官的，有余力就去学习；学习的，有余力就去做官了。</div>
<div class="yuanwen list fayin" onclick="run(this)">子游曰：“丧致乎哀而止。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">子游说：“服丧时，能充分表现出哀伤之情就行了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子游曰：“吾友张也，为难能也<sup yin="">①</sup>，然而未仁。”</div>
<div class="shuoming list">❶张：即颛孙师，字子张。朱熹说：“子张行过高，而少诚实恻怛（cè dá）之意”。才高意广，人所难能，而心驰于外，不能全其心德，未得为仁。</div>
<div class="shiyi list fayin" onclick="run(this)">子游说：“我的朋友子张呀，算是难能可贵的了，不过还没有达到仁。”</div>
<div class="yuanwen list fayin" onclick="run(this)">曾子曰：“堂堂乎张也<sup yin="">①</sup>，难与并为仁矣。”</div>
<div class="shuoming list">❶堂堂：形容仪表壮伟，气派十足。据说子张外有余而内不足，他的为人重在“言语形貌”，不重在“正心诚意”，故人不能助他为仁，他也不能助人为仁。</div>
<div class="shiyi list fayin" onclick="run(this)">曾子说：“子张呀，气概不凡，别人难以同他一起做到仁。”</div>
<div class="yuanwen list fayin" onclick="run(this)">曾子曰：“吾闻诸夫子：人未有自致者也<sup yin="">①</sup>，必也亲丧乎！”</div>
<div class="shuoming list">❶致：尽致，指人的感情全都表露出来。</div>
<div class="shiyi list fayin" onclick="run(this)">曾子说：“我听老师说过：人一般没有充分宣泄自己感情的，（要说有）肯定是在父母过世的时候吧！”</div>
<div class="yuanwen list fayin" onclick="run(this)">曾子曰：“吾闻诸夫子，孟庄子之孝也<sup yin="">①</sup>，其他可能也；其不改父之臣与父之政，是难能也。”</div>
<div class="shuoming list">❶孟庄子：鲁国大夫，名速。</div>
<div class="shiyi list fayin" onclick="run(this)">曾子说：“我听老师说过：孟庄子的孝道，其他各项都不难做到；但他不换掉父亲的旧臣僚和政策措施，这是别人难做到的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">孟氏使阳肤为士师<sup yin="">①</sup>，问于曾子。曾子曰：“上失其道，民散久矣。如得其情，则哀矜而勿喜！”</div>
<div class="shuoming list">❶阳肤：旧注说他是曾子弟子。</div>
<div class="shiyi list fayin" onclick="run(this)">孟氏让阳肤担任法官，阳肤向曾子请教应该怎么去做。曾子说：“在上位的人背离正道，民心离散已经很久了。你如果审出了犯罪的实情，就该同情怜悯他们，不要自以为明察而沾沾自喜！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子贡曰：“纣之不善，不如是之甚也<sup yin="">①</sup>。是以君子恶<sup yin="wù">②</sup>居下流，天下之恶<sup yin="è">③</sup>皆归焉。”</div>
<div class="shuoming list">❶是：代词。指人们传说的那样。❷恶（wù）：讨厌，憎恨，憎恶。❸恶（è）：坏事，罪恶。子贡说这番话的意思，当然不是为纣王去辩解开脱，而是要提醒世人应当经常自我警戒反省，在台上的时候律己要严。否则一旦失势，置身“下流”，天下的“恶名”将集于一身而遗臭万年。</div>
<div class="shiyi list fayin" onclick="run(this)">子贡说：“纣王的恶行，不像传说中的那么厉害。所以君子最讨厌身有污行，（一旦如此）天下的坏事都算到他头上了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子贡曰：“君子之过也，如日月之食<sup yin="蚀">①</sup>焉：过也，人皆见之；更也，人皆仰之。”</div>
<div class="shuoming list">❶食：同“蚀”。</div>
<div class="shiyi list fayin" onclick="run(this)">子贡说：：“君子的过失，好比日食月蚀：有了过错，人人都看见；改了过错，人人都景仰。”</div>
<div class="yuanwen list fayin" onclick="run(this)">卫公孙朝<sup yin="cháo">①</sup>问于子贡曰：“仲尼焉学？”子贡曰：“文武之道，未坠于地，在人。贤者识其大者，不贤者识其小者，莫不有文武之道焉。夫子焉不学？而亦何常师之有？”</div>
<div class="shuoming list">❶公孙朝：卫国大夫。</div>
<div class="shiyi list fayin" onclick="run(this)">卫国的公孙朝问子贡说：“仲尼从哪里学到学问的？”子贡说：“文王、武王之道，并没有散失，还在人间流传。贤人记得其中的大道理，不贤的人记得其中的小道理，但都有文武之道在里面。我的老师哪里不学？又哪有什么固定的老师呢？”</div>
<div class="yuanwen list fayin" onclick="run(this)">叔孙武叔语<sup yin="yù">yù</sup>大夫于朝<sup yin="cháo">cháo</sup>曰<sup yin="">①</sup>：“子贡贤于仲尼。”子服景伯以告子贡<sup yin="">②</sup>。子贡曰：“譬之宫墙，赐之墙也及肩，窥见室家之好。夫子之墙数仞<sup yin="">③</sup>，不得其门而入，不见宗庙之美，百官之富<sup yin="">④</sup>。得其门者或寡矣。夫子之云，不亦宜乎！”</div>
<div class="shuoming list">❶叔孙武叔：鲁国大夫，名州仇。❷子服景伯：鲁国大夫。❸仞：古代长度单位，七尺为仞。❹官：房舍。</div>
<div class="shiyi list fayin" onclick="run(this)">叔孙武叔在朝廷对大夫们说：“子贡比孔子贤明。”子服景伯把这话告诉了子贡。子贡说：“用围墙打比方，我的围墙只有肩膀高，在外面就看得见里面院子漂亮。老师的围墙却有几丈高，如果不找到门进去，就看不到里面像宗庙大殿般宏大壮美的各式各样的房屋。能找到门的人也许是太少了。那位先生那么说，不也是正常的吗！”</div>
<div class="yuanwen list fayin" onclick="run(this)">叔孙武叔毁仲尼。子贡曰：“无以为也！仲尼不可毁也。他人之贤者，丘陵也，犹可逾也；仲尼，日月也，无得而逾焉。人虽欲自绝<sup yin="">①</sup>，其何伤于日月乎？多见其不知量也<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶自觉：自行断绝跟对方之间的关系。❷多：只是，徒然。</div>
<div class="shiyi list fayin" onclick="run(this)">叔孙武叔诋毁仲尼。子贡说：“这样做事没有用的！仲尼是不可以诋毁的。别人的贤德，好比是丘陵，还是能超越的；仲尼（的贤德），好比是日月，是无法超越的。一个人即使想自绝于日月，但对日月来说，又有什么损伤呢？只不过显出这个人毫不自量罢了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">陈子禽谓子贡曰：“子为恭也，仲尼岂贤于子乎？”子贡曰：“君子一言以为知<sup yin="智">智</sup>，一言以为不知<sup yin="智">智</sup>，言不可不慎也！夫子之不可及也，犹天之不可阶而升也。夫子之得邦家者，所谓立之斯立，道<sup yin="导">导</sup>之斯行，绥之斯来<sup yin="">①</sup>，动之斯和。其生也荣，其死也哀，如之何其可及也？”</div>
<div class="shuoming list">❶绥（suí）：安抚。</div>
<div class="shiyi list fayin" onclick="run(this)">陈子禽对子贡说：“您（对仲尼）表现出恭敬，仲尼难道真的比你强吗？”子贡说：“君子说一句话就可以显出他的聪明，同样说一句话也可以表现出他的无知，所以说话不可不慎重啊！我的老师是无法赶得上的，就像青天无法架梯子登上去一样。老师如果做了诸侯或大夫，那真可说是要百姓立于礼，百姓就会立于礼，引导百姓，百姓就会跟他走，安抚百姓，百姓就会来投奔，动员百姓，百姓就会万众响应。老师生得荣耀，死得可哀，（这样的老师）我怎么能赶得上他呢？”</div>

`