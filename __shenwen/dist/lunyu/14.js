$.jsname__14 =`
<h3>宪问篇</h3>
<div class="yuanwen list fayin" onclick="run(this)">宪问耻<sup yin="">①</sup>。子曰：“邦有道，谷。邦无道，谷，耻也。”“克、伐、怨、欲，不行焉，可以为仁矣？”子曰：“可以为难矣，仁则吾不知也。”</div>
<div class="shuoming list">❶宪：姓原，名宪，字子思。孔子的学生。</div>
<div class="shiyi list fayin" onclick="run(this)">原宪问什么是耻辱。孔子说：“国家有道时，可以做官享用俸禄；国家无道时，也去做官享用俸禄，这就是耻辱。”原宪又问：“好胜、自夸、怨恨、贪欲，这些毛病都没有，可以算仁了吧？”孔子说：“可以算难能可贵了。至于是否算仁，我就不知道了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“士而怀<sup yin="">①</sup>居，不足以为士矣。”</div>
<div class="shuoming list">❶怀：留恋，思念。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“一个读书人如果眷恋在家的安逸生活，就不配称作读书人了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“邦有道，危言危行<sup yin="">①</sup>；邦无道，危行言孙<sup yin="逊"></sup>。”</div>
<div class="shuoming list">❶危：正、正直。❷孙（xùn）：同“逊”，谦逊，恭顺。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“国家有道时，要正直地说话，正直地做人；国家政治昏乱时，要正直地做人，说话却要小心谨慎。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“有德者必有言，有言者不必有德。仁者必有勇，勇者不必有仁。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“有德行的人必定有好言论，有好言论的人不一定就有德行。仁人必定勇敢，勇敢的人不一定就有仁德。”</div>
<div class="yuanwen list fayin" onclick="run(this)">南宫适<sup yin="kuò">①</sup>问于孔子曰：“羿<sup yin="yì">②</sup>善射，奡<sup yin="">③</sup>荡舟，俱不得其死然。禹、稷躬稼而有天下<sup yin="">④</sup>。”夫子不答。南宫适出，子曰：“君子哉若人！尚德哉若人！”</div>
<div class="shuoming list">❶南宫适（kuò）：即孔子弟子南容。❷羿（yì）：古代传说中有三个羿，都是射箭能手。这里指夏代有穷困的君主，曾篡夺夏太康的王位，后被其臣寒浞（zhuó）杀掉。❸奡（ào）：传说是一个善于水战的大力士，后被夏少康所杀。荡舟：指水战。❹禹、稷：禹，传说是尧、舜之臣，因治水有功，继承了舜的帝位，成为夏代开国君主。稷，即后稷，名弃，舜时农官，曾教民农作，是周朝先祖。</div>
<div class="shiyi list fayin" onclick="run(this)">南宫适问孔子：“羿擅长射箭，奡很会水战，但都没有一个好结果。而禹、稷亲自种庄稼却得了天下？”孔子不答话。南宫适出去后，孔子说：“这个人真是君子啊！这个人真是崇尚道德啊！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子而不仁者有矣夫，未有小人而仁者也。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子里面不仁的人是有的，但小人里面却没有有仁德的人。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“爱之，能勿劳乎<sup yin="">①</sup>？忠焉，能勿诲乎？”</div>
<div class="shuoming list">❶劳：勤劳，劳苦，操劳。此有进行劳动教育的含意。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“爱一个人，能不让他勤劳吗？诚心对待一个人，能不教诲他吗？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“为命<sup yin="">①</sup>，裨谌<sup yin="">②</sup>草创之，世叔<sup yin="">③</sup>讨论之，行人子羽修饰之<sup yin="">④</sup>，东里子产润色之<sup yin="">⑤</sup>。”</div>
<div class="shuoming list">❶为命：拟定辞令。❷裨谌（bì chén）：郑国大夫。❸世叔：郑国大夫。❹行人字羽：行人，官名，管朝觐聘问等外事；子羽，郑国大夫公孙挥的字。 ❺东里子产：郑国大夫。东里是地名，子产所居之地。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“郑国外交文件的制定，是由裨谌起草，世叔审议，然后由外交官子羽修改加工，最后由东里子产进行文字上的润色修饰完成。”</div>
<div class="yuanwen list fayin" onclick="run(this)">或问子产<sup yin="">①</sup>。子曰：“惠人也。”问子西<sup yin="">②</sup>。曰：“彼哉！彼哉！”问管仲。曰：“人也。夺伯氏骈邑三百<sup yin="">③</sup>，饭疏食，没齿无怨言。”</div>
<div class="shuoming list">❶子产：见《公冶长》篇第十六章注。❷子西：即郑子西，与子产为同宗兄弟，因两人在郑相继执政，而使优劣相形见绌。下说“彼哉！彼哉！”含有不值得称许之意。❸伯氏：齐国大夫，因罪被管仲依法下令剥夺采邑三百户。由于管仲执法公允，故伯氏至死无怨言。骈（pián）邑：齐国地名。</div>
<div class="shiyi list fayin" onclick="run(this)">有人问子产的为人，孔子说：“是个宽厚慈爱的人。”问子西怎么样，孔子说：“他啊，他啊！”问管仲怎么样，孔子说，“是个人才。他剥夺了伯氏骈邑三百户的封地，使伯氏只得吃粗粮过日子，但至死（他对管仲）都没有怨言。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“贫而无怨难，富而无骄易。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“贫困但没有怨恨，很难做到；富有却不傲慢，则容易做到。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“孟公绰为赵、魏老则优<sup yin="">①</sup>，不可以为滕、薛大夫<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶孟公绰：鲁国大夫，为人清静寡欲，受孔子尊敬。老：大夫家臣称为老。优：宽绰，有余裕。❷滕、薛：春秋时的两个小国，都在鲁国附近。赵氏、魏氏手下贤人多，家臣清闲，故孟公绰足以担任；而滕、薛，国虽然小而政务繁，大夫职责重，故孟公绰不能胜任。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“孟公绰要是做晋国赵氏、魏氏的家臣那是绰绰有余，但是不能去滕国、薛国这样的小地方做大夫。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子路问成人。子曰：“若臧武仲之知<sup yin="智">①</sup>，公绰之不欲，卞庄子之勇<sup yin="">②</sup>，冉求之艺，文之以礼乐，亦可以为成人矣。”曰：“今之成人者何必然？见利思义，见危授命，久要<sup yin="yāo">③</sup>不忘平生之言，亦可以为成人矣。”</div>
<div class="shuoming list">❶臧武仲：即臧武纥（gē），鲁国大夫，以知识丰富著称。❷卞庄子：鲁国大夫，以勇力著称。❸要（yāo）：同“约”，穷困。</div>
<div class="shiyi list fayin" onclick="run(this)">子路问怎样才算一个完善的人，孔子说：“像臧武仲那样有智慧过人，孟公绰那样清心寡欲，卞庄子那样勇猛无畏，冉求那样多才多艺，再加上礼乐的修养，也可以算个完美的人。”孔子又说：“现在的完人何必一定要这样？见到好处先考虑是否合理，遇到危难敢献出生命，身处困境不论多久都不忘记平生诺言，也可以算个完人了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子问公叔文子于公明贾曰<sup yin="">①</sup>：“信乎，夫子不言，不笑，不取乎？”公明贾对曰：“以告者过也。夫子时然后言，人不厌其言；乐然后笑，人不厌其笑；义然后取，人不厌其取。”子曰：“其然？岂其然乎？”</div>
<div class="shuoming list">���公叔文子：卫国大夫，卫献公之孙，名拔，谥号“文”。公明贾：卫国人，姓公明，名贾，公叔文子的使臣。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子向公明贾打听公叔文子的为人，说：“这位先生平时不说，不笑，不取财，这是真的吗？”公明贾答道：“这是传话的传错了。先生该说的时候才说话，人家就不讨厌他的话；心中欢喜才笑，人家就不反感他笑；只有符合道义他才拿钱财，人家不讨厌他拿。”孔子说：“是这样吗？，难道真是这样吗？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“臧武仲以防求为后于鲁<sup yin="">①</sup>，虽曰不要<sup yin="yāo">yāo</sup>君<sup yin="">②</sup>，吾不信也。”</div>
<div class="shuoming list">❶臧武仲曾因得罪季孙氏而逃亡到邾（zhū）国，后又回到其封地防邑派人向鲁君请求为臧氏立后，以不废先人宗庙祭祀，自己愿为此让出封地。后鲁君立臧武仲的同父异母兄弟为臧氏继承人，臧武仲遂献出防邑逃亡到齐国。❷要（yāo）：要挟。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“臧武仲凭借着他的封地防邑，要求鲁君在鲁国为臧氏立下继承人，尽管有人说这不是要挟国君，我不相信。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“晋文公谲<sup yin="jué">jué</sup>而不正<sup yin="">①</sup>，齐桓公正而不谲<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶晋文公：晋国国君，春秋五霸之一，姓姬，名重耳。他曾召周天子而迫使各地诸侯来朝见。谲（jué）：欺诈，玩弄权术。❷齐桓公：齐国国君，春秋五霸之一，姓姜，名小白。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“晋文公诡诈，不正派；齐桓公正派，不诡诈。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子路曰：“桓公杀公子纠，召<sup yin="shào">shào</sup>忽<sup yin="hū">hū</sup>死之，管仲不死<sup yin="">①</sup>。”曰：“未仁乎？”子曰：“桓公九合诸侯<sup yin="">②</sup>，不以兵车，管仲之力也。如其仁！如其仁！”</div>
<div class="shuoming list">❶齐桓公和公子纠是兄弟。齐襄公死后，桓公夺得君位，杀了公子纠。纠的家臣自杀殉主，管仲也是纠的家臣，却不愿自杀，归顺了桓公，后辅佐桓公成就了霸业。❷九合诸侯：多次召集诸侯共商会盟，“九”泛指多次。</div>
<div class="shiyi list fayin" onclick="run(this)">子路说：“齐桓公杀了他哥哥公子纠，（作为公子纠的家臣）召忽为此自杀，（同样作为家臣的）管仲却没有跟着去死。”子路接着说：“管仲不能算是仁吧？”孔子说：“齐桓公多次召集诸侯共商会盟，却不凭借武力，这都是靠管仲的努力。这就是他的仁德！这就是他的仁德！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子贡曰：“管仲非仁者与<sup yin="欤"></sup>？桓公杀公子纠，不能死，又相之。”子曰：“管仲相桓公，霸诸侯，一匡天下，民到于今受其赐。微管仲，吾其被<sup yin="pī">pī</sup>发左衽矣<sup yin="">①</sup>。岂若匹夫匹妇之为谅也<sup yin="">②</sup>，自经于沟渎而莫之知也？”</div>
<div class="shuoming list">❶这句话是说，如果没有管仲，天下混战不止，社会讲倒退，中原地区的人将沦为落后的民族。被：同“披”。左衽（rèn）：衣襟向左边开，这是夷狄的打扮。❷谅：诚信，遵守信用。</div>
<div class="shiyi list fayin" onclick="run(this)">子贡说：“管仲不是仁者吧？桓公杀了公子纠，管仲不以身殉职，反而做了桓公的相国。”孔子说：“管仲做桓公的相国，称霸诸侯，一匡天下，黎民百姓到如今都还在享受他带来的好处。要不是管仲，我们可能还是（像野蛮不开化民族那样）披头散发，左掩着衣襟了。怎么能像普通人一样只顾着为主子尽忠而忘了天下百姓，自杀在山沟沟里，还没有人知道吗？”</div>
<div class="yuanwen list fayin" onclick="run(this)">公叔文子之臣大夫僎<sup yin="zhuàn">①</sup>与文子同升诸公。子闻之，曰：“可以为‘文’矣！”</div>
<div class="shuoming list">❶臣大夫僎(zhuàn)：臣大夫，即家臣大夫，是家臣中最高的一级。</div>
<div class="shiyi list fayin" onclick="run(this)">公叔文子的家臣大夫僎，（由文字推荐）和文子一同做了卫国的大夫。孔子听到后，说：“就凭这一点，公叔文子真可以谥号作‘文’了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子言卫灵公之无道也，康子曰：“夫如是，奚而不丧<sup yin="sàng">sàng</sup>？”孔子曰：“仲叔圉<sup yin="yǔ">yǔ</sup>治宾客，祝鮀<sup yin="tuó">tuó</sup>治宗庙，王孙贾治军旅<sup yin="">①</sup>。夫如是，奚其丧？”</div>
<div class="shuoming list">❶仲叔圉（yǔ）、祝鮀（tuó）、、王孙贾（jiǎ）：都是卫国大夫。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说起卫灵公昏庸无道，季康子说：“既然这样，为什么卫国不亡啊？”孔子说：“卫国有仲叔圉管来宾接待，有祝鮀管宗庙祭祀，王孙贾管带兵打仗。像这样，怎么会亡国呢？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“其言之不怍<sup yin="zuò">①</sup>，则为之也难。”</div>
<div class="shuoming list">❶怍（zuò）：惭愧。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“如果一个人说起话来大言不惭，那么，要他做些实事一定很难。”</div>
<div class="yuanwen list fayin" onclick="run(this)">陈成子<sup yin="">①</sup>弑简公。孔子沐浴而朝，告于哀公曰<sup yin="">②</sup>：“陈恒弑其君，请讨之。”公曰：“告夫三子<sup yin="">③</sup>。”孔子曰：“以吾从大夫之后，不敢不告也。君曰‘告夫三子’者！”之三子告，不可。孔子曰：“以吾从大夫之后，不敢不告也。”</div>
<div class="shuoming list">❶陈成子：齐国大夫，名恒。本姓陈，为陈国公族之后，其先人来到齐国之后改姓田。❷孔子认为臣杀君是大逆不道的，为了表示郑重其事，故“沐浴而朝”。❸三子：指当时把持鲁国政权的季孙，孟孙，叔孙三人。鲁哀公不敢作主，故要孔子向季孙等三人报告。</div>
<div class="shiyi list fayin" onclick="run(this)">（齐国大夫）陈成子杀了齐简公。孔子斋戒沐浴后上朝，报告鲁哀公说：“陈恒杀了他的国君，请发兵讨伐！”哀公说：“把这事报告三位大夫吧！”孔子（出来后）对人说：“因为我曾经做过大夫，所以不敢不来报告。君王却说‘去报告那三位大夫吧！’”于是孔子报告了三位大夫，三位大夫都不愿出兵。孔子（回来）说：“因为我曾经做过大夫，所以不敢不报告。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子路问事君。子曰：“勿欺也，而犯之<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶犯：触犯，冒犯。这里引申为对君主犯颜诤谏。</div>
<div class="shiyi list fayin" onclick="run(this)">子路问如何侍奉君王。孔子说：“不要欺瞒君主，但可以当面指出他的过错规劝他。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子上达，小人下达。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子通达于仁义，小人通达于财利。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“古之学者为己，今之学者为人。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“古代的人求学是为了提高自己的道德学问，现在的人求学是为了装饰给别人看。”</div>
<div class="yuanwen list fayin" onclick="run(this)">蘧<sup yin="qú">qú</sup>伯玉<sup yin="">①</sup>使人于孔子，孔子与之坐而问焉，曰：“夫子何为？”对曰：“夫子欲寡其过而未能也。”使者出，子曰：“使乎！使乎！”</div>
<div class="shuoming list">❶蘧（qú）伯玉：卫国大夫，名瑗。孔子在卫国时曾住在他家。</div>
<div class="shiyi list fayin" onclick="run(this)">蘧伯玉派人去看望孔子。孔子请来人坐下，然后问道：“他老先生最近忙什么呢？”来人答道：“老先生想减少自己的过错，但还没有如愿。”来者告辞后，孔子感叹说：“好使者啊！好使者啊！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“不在其位，不谋其政。”曾子曰：“君子思不出其位。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“不在那个职位上，就不要谋划那个职位的政事。”曾子说：“君子思考问题不超出他的职位范围。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子耻其言而过其行。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子觉得可耻的是说得多，做得少。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子道者三，我无能焉：仁者不忧，知<sup yin="智">①</sup>者不惑，勇者不惧。”子贡曰：“夫子自道也！”</div>
<div class="shuoming list">❶知：同“智”，智慧，聪明。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子有三样，是我做不到的：仁慈无忧，明智无疑，勇敢无畏。”子贡说：“这正是老师说他自己啊。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子贡方<sup yin="谤">①</sup>人。子曰：“赐也，贤乎哉？夫我则不暇。”</div>
<div class="shuoming list">❶方：同“谤”，指责，说别人的短处。</div>
<div class="shiyi list fayin" onclick="run(this)">子贡指责别人。孔子说：“赐啊，你自己就那么好吗？要叫我呀，可没有那份闲工夫（指责别人）。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“不患<sup yin="">①</sup>人之不己知，患其不能也。”</div>
<div class="shuoming list">❶患：忧虑，担心，怕。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“不担心别人不了解自己，只担心自己没由才能。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“不逆<sup yin="">①</sup>诈，不亿<sup yin="臆">②</sup>不信，抑亦先觉者，是贤乎！”</div>
<div class="shuoming list">❶逆：预先，预测。❷亿：同“臆”，主观推测，猜测。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“不事先猜疑别人欺诈，不凭空猜测别人不诚信，却也能够事先识破各种诈骗和失信，这种人就是贤人啊！”</div>
<div class="yuanwen list fayin" onclick="run(this)">微生亩<sup yin="">①</sup>谓孔子曰：“丘何为是栖栖<sup yin="">②</sup>者与<sup yin="欤"></sup>？无乃为佞乎？”孔子曰：“非敢为佞也，疾固也。”</div>
<div class="shuoming list">❶微生亩：姓微生，名亩，传说是个隐者。❷栖栖（xī）：忙忙碌碌的样子。</div>
<div class="shiyi list fayin" onclick="run(this)">微生亩对孔子说：“你为什么这样忙忙碌碌（到处游说）呢？不会是显示口才吧？”孔子说：“我不敢卖弄口才，而是痛心世人顽固不化（不通仁义之道）。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“骥<sup yin="">①</sup>不称其力，称其德<sup yin="">②</sup>也。”</div>
<div class="shuoming list">❶骥：善跑的千里马。❷德：这里指千里马能吃苦耐劳的优良品质。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“对于千里马，不是要赞扬它的力气，而是要赞扬它的品德。”</div>
<div class="yuanwen list fayin" onclick="run(this)">或曰：“以德报怨，何如？”子曰：“何以报德？以直报怨，以德报德。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">有人问孔子：“用自己的恩德来回报别人的怨恨，怎么样？”孔子说：“（如果是这样）那用什么来回报别人的恩德呢？（应该）用正直公平回报怨恨，用恩德回报恩德。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“莫我知也夫<sup yin="">①</sup>！”子贡曰：“何为其莫知子也？”子曰：“不怨天，不尤<sup yin="">②</sup>人，下学而上达。知我者其天乎！”</div>
<div class="shuoming list">❶莫我知：即“莫知我”的倒装。没有人了解我。❷尤：责怪，归咎，怨恨。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子感慨道：“没有人能了解我啊！”子贡问：“为什么没有人了解您呢？”孔子说：“我不埋怨天，不责备人，下学人事而上通达天命，了解我的大概只有天吧！”</div>
<div class="yuanwen list fayin" onclick="run(this)">公伯寮愬<sup yin="诉"></sup>子路于季孙<sup yin="">①</sup>。子服景伯以告<sup yin="">②</sup>，曰：“夫子固有惑志于公伯寮，吾力犹能肆诸市朝<sup yin="">③</sup>。”子曰：“道之将行也与<sup yin="欤"></sup>，命也；道之将废也与<sup yin="欤"></sup>，命也。公伯寮其如命何！”</div>
<div class="shuoming list">❶公伯寮：字子周，孔子弟子，曾任季氏家臣。子路当时担任季孙氏的家臣。愬（sù）：同“诉”，毁谤。❷子服景伯：鲁国大夫。子服是姓氏，“景”是谥号，字伯，名何。❸肆：陈列死尸。</div>
<div class="shiyi list fayin" onclick="run(this)">公伯寮到季孙面前说子路的坏话。子服景伯将这事告诉孔子，并且说：“季孙氏肯定被公伯寮的谗言迷惑了（已经对子路起了疑心），但凭我的能力还能（向季孙氏解释清楚，使季孙氏杀了公伯寮）将他的尸首放在街头示众。”孔子说：“我的主张要能实现，这是天命；我的主张要是被废弃，那也是天命。公伯寮又能把我的命运怎么样！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“贤者辟<sup yin="避">①</sup>世，其次<sup yin="避"></sup>地，其次辟<sup yin="避"></sup>色，其次辟<sup yin="避"></sup>言。”子曰：“作者七人<sup yin="">②</sup>矣。”</div>
<div class="shuoming list">❶辟：同“避”。❷七人：有人认为这七人就是《微子》篇第八章所列的七位“逸民”：伯夷、叔齐、虞仲、夷逸、朱张、柳下惠、少连。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“贤人会躲避乱世（而隐居），次一等的避开一个地方到另一个地方去居住，再次一等的避开别人难看的脸色，再次一等的避开别人的恶言恶语。” 孔子补充说：“这样做的已经有七个人了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子路宿于石门。晨门曰：“奚自<sup yin="">①</sup>？”子路曰：“自孔氏。”曰：“是知其不可而为之者与<sup yin="欤"></sup>？”</div>
<div class="shuoming list">❶奚自：“自奚”的倒装。从哪里来。</div>
<div class="shiyi list fayin" onclick="run(this)">子路在石门住了一夜。第二天一早，看门的问他：“哪里来的？”子路说：“从孔子那里来。”看门的又问：“是那位明知道行不通却还仍然要坚持的那个人吗？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子击磬<sup yin="qìng">qìng</sup>于卫，有荷蒉<sup yin="">①</sup>而过孔氏之门者，曰：“有心哉，击磬乎！”既而曰：“鄙哉，硁硁<sup yin="">kēng</sup>乎，莫己知也，斯己而已矣。‘深则厉，浅则揭<sup yin="">②</sup>’。” 子曰：“果哉！末之难矣。”</div>
<div class="shuoming list">❶蒉（kuì）：盛土的草编器具。❷深则厉，浅则揭：引自《诗经》。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子在卫国，有一天他敲磬奏乐，一个挑草筐的路过门前，说：“有心事啊，这样子敲磬！”过一会儿又说：“档次太低，沉闷鄙俗，没有人了解自己，那就独善其身罢了。‘（好比过河）河水深，索性穿着衣服直涉水过去；河水浅，撩起裤腿趟过去。’”孔子说：“真果决啊！我要说服他难啊。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子张曰：“《书》云，‘高宗谅阴<sup yin="">①</sup>，三年不言。’何谓也？”子曰：“何必高宗，古之人皆然。君薨<sup yin="">②</sup>，百官总己以听于冢宰三年。”</div>
<div class="shuoming list">❶高宗：即商王武丁。❷薨（hōng）：古代称诸侯死叫“薨”。❸冢宰：官名，辅佐天子之官，相当于后世的宰相。</div>
<div class="shiyi list fayin" onclick="run(this)">子张问：“《尚书》讲的‘殷高宗守孝，三年不说话’，这是什么意思？”孔子回答说：“何止是高宗，古代的人都这样。国君死了（继位的国君三年不谈政事），朝廷百官各自统管好自己的职事且听命于宰相，这种状况要持续三年。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“上好礼，则民易使<sup yin="">①</sup>也。”</div>
<div class="shuoming list">❶使：使唤，役使。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“在高位的人遵从礼法，那么百姓就容易役使了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子路问君子。子曰：“修己以敬。”曰：“如斯而已乎？”曰：“修己以安人。”曰：“如斯而已乎？”曰：“修己以安百姓。修己以安百姓，尧、舜其犹病<sup yin="">①</sup>诸。”</div>
<div class="shuoming list">❶病：担心，忧虑。</div>
<div class="shiyi list fayin" onclick="run(this)">子路问怎样做君子，孔子说：“修养自己，使百姓恭敬谦逊。”子路又问：“这样就够了吗？”孔子说：“修养自己，以使贵族、大夫们得到安乐。”子路又问：“这样就够了吗？”孔子说：“修养自己，以使百姓得到安乐。修养自己，以使百姓得到安乐，就连尧舜大概也难以做到吧！”</div>
<div class="yuanwen list fayin" onclick="run(this)">原壤夷俟<sup yin="">①</sup>。子曰：“幼而不孙<sup yin="逊"></sup>弟<sup yin="悌">②</sup>，长而无述焉，老而不死，是为贼。”以杖叩其胫。</div>
<div class="shuoming list">❶原壤：鲁国人，是孔子的老朋友。夷：箕踞，即坐时两脚叉开前申，形状像箕一样，这是无礼的表现。俟（sì）：等待。❷孙弟：同“逊悌”，这里泛指礼节。</div>
<div class="shiyi list fayin" onclick="run(this)">原壤两腿叉开坐着等孔子来。孔子见了说：“你小时候就不讲礼节，长大了没有作为，老了还不快死，这真是害人精。！”说着就操起棍子轻轻敲他的小腿。</div>
<div class="yuanwen list fayin" onclick="run(this)">阙党童子将命<sup yin="">①</sup>。或问之曰：“益者与<sup yin="欤"></sup>？”子曰：“吾见其居于位也，见其与先生并行也。非求益者也，欲速成者也。”</div>
<div class="shuoming list">❶阙党：里巷名，孔子故居所在地。将命：传达宾主的辞令。</div>
<div class="shiyi list fayin" onclick="run(this)">阙党的一个少年来向孔子传话。有人便问孔子：“这少年能有长进吗？”孔子说：“我看见他坐在成年人的位置上，又看见他同长辈并肩走路，（这表明）他不是个求长进的人，而是个急于求成的人罢了。”</div>

`