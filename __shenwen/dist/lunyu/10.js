$.jsname__10 =`
<h3>乡党篇</h3>
<div class="yuanwen list fayin" onclick="run(this)">孔子于乡党，恂恂<sup yin="">①</sup>如也，似不能言者。其在宗庙朝庭，便便<sup yin="">②</sup>言，唯谨尔。</div>
<div class="shuoming list">❶恂恂（xún）：恭顺谦和。❷便便：形容说话清楚明白。便同“辩”。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子在家乡的时候，非常恭顺谦和，就像个不会说话的人。在宗庙里、朝廷上，说话清楚明白，只是很谨慎。</div>
<div class="yuanwen list fayin" onclick="run(this)">朝，与下大夫<sup yin="">①</sup>言，侃侃如也；与上大夫言，訚訚<sup yin="">②</sup>如也。君在，踧<sup yin="cù"></sup>踖<sup yin="jí">③</sup>如也，与与<sup yin="">④</sup>如也。</div>
<div class="shuoming list">❶下大夫：官名。周王室及诸侯各国，卿以下有大夫，分上、中、下三等。❷訚（yín）：和颜悦色，而能中正诚恳，尽言相诤。❸踧踖（cù jí）：恭敬而又不安的样子。❹与与：威仪适度的样子。</div>
<div class="shiyi list fayin" onclick="run(this)">（孔子）上朝时，（在君主来到之前）同下大夫交谈，显出和气愉快的样子；与上大夫交谈，显出恭敬温和的样子。君主临朝时，显出恭敬、威仪适中的样子。</div>
<div class="yuanwen list fayin" onclick="run(this)">君召使摈<sup yin="傧">①</sup>，色勃如也<sup yin="">②</sup>，足躩<sup yin="jué">③</sup>如也。揖所与立，左右手，衣前后，襜<sup yin="chān">④</sup>如也。趋进，翼如也。宾退，必复命曰：“宾不顾矣。”</div>
<div class="shuoming list">❶摈（bin）：同“傧”，是国君派遣的负责接待外国宾客的官员。❷色：面色。勃如：矜持庄重。❸躩（jué）：快步前进。❹襜（chān）：整齐。</div>
<div class="shiyi list fayin" onclick="run(this)">鲁君召孔子，让他去做傧相。（孔子接待宾客时）神情矜持庄重，走路快而平稳。向站在一起的人作揖，时而向左拱手，时而向右拱手，衣服前后摆动，整齐而不乱。（由中庭）快步向前，（两臂拱起）像鸟儿张开了翅膀。宾客告辞后，他总是回复国君说：“宾客已经远去了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">入公门，鞠躬如也，如不容。立不中门，行不履阈<sup yin="">①</sup>。过位，色勃如也，足躩如也，其言似不足者。摄齐<sup yin="zī">②</sup>升堂，鞠躬如也，屏<sup yin="bǐng">③</sup>气似不息者。出，降一等<sup yin="">③</sup>，逞<sup yin="">④</sup>颜色，怡怡如也。没<sup yin="mò">mò</sup>阶<sup yin="">⑤</sup>，趋进，翼如也。复其位，踧<sup yin="cù">cù</sup>踖<sup yin="jí">jí</sup>如也。</div>
<div class="shuoming list">❶阈（yù）：门坎。❷齐（zī）：衣服的下摆。❸等：台阶。❹逞：放开。❺没阶：走完台阶。</div>
<div class="shiyi list fayin" onclick="run(this)">（孔子）进入朝廷的大门时，显出恭敬谨慎的样子，好像没有容身之地。不站立在门的中间，步行不踩门坎。经过国君（空着的）座位时，脸色顿时庄重起来，步子加快，话音放低，像说话力气不足似的。提起衣服的下摆上堂，谨慎小心的样子，敛身憋气，像停住了呼吸一般。从堂上退出，走下了一级台阶，脸色才舒展开来，显得轻松愉快。下完台阶，快步向前走，像鸟儿展翅一样。回到自己位置上，依然显出恭敬谨慎的样子。</div>
<div class="yuanwen list fayin" onclick="run(this)">执圭<sup yin="">①</sup>，鞠躬如也，如不胜。上如揖，下如授。勃如战色，足蹜蹜<sup yin="">②</sup>如有循。享礼，有容色。私觌<sup yin="">③</sup>，愉愉如也。</div>
<div class="shuoming list">❶圭：一种玉器，上圆下方，举行典礼时不同身份的人手执不同的圭。大夫出使外国时，所执之圭是自己代表君主出使的身份凭证。❷蹜蹜（sùsù）：脚步细碎紧密。❸觌（dí）：见面，会见，以礼相见。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子（出使到别国参加典礼时）手里拿着圭，小心谨慎，似乎拿不住的样子。拿上了一点，像是作揖；拿下了一点，像是递给人东西。脸色庄重得战战兢兢，脚步细碎，像在沿着什么行走。献礼物时，和颜悦色。到了以私人身份跟别国君臣相见时，显得非常轻松愉快。</div>
<div class="yuanwen list fayin" onclick="run(this)">君子不以绀緅<sup yin="">①</sup>饰，红紫<sup yin="">②</sup>不以为亵服。当暑，袗絺绤<sup yin="">③</sup>，必表<sup yin="">④</sup>而出之。缁<sup yin="">⑤</sup>衣，羔裘；素<sup yin="">⑥</sup>衣，麑<sup yin="">⑦</sup>裘；黄衣，狐裘。亵裘长，短右袂<sup yin="">⑧</sup>。必有寝衣<sup yin="">⑨</sup>，长一身有又半。狐貉之厚以居。去丧<sup yin="sāng"></sup>，无所不佩。非帷裳<sup yin="">⑩</sup>，必杀<sup yin="shài">⑪</sup>之。羔裘玄冠<sup yin="guān">guān</sup>不以吊。吉月<sup yin="">⑫</sup>必朝<sup yin="cháo"></sup>服而朝<sup yin="cháo"></sup>。</div>
<div class="shuoming list">❶绀（gàn）：为深青透红色。緅（zōu）：微带红的黑色。緅与绀相比，黑多红少，较暗。饰：领子和袖子的边缘。緅与绀是古代礼服的颜色，所以不适合做边缘。❷红紫：贵重的正服所用颜色。亵服：居家日常所穿衣服。❸袗（zhěn）絺绤（chīxì）：袗，单衣。絺，细麻布。绤，粗葛布。❹表：穿在外面的衣服。出：出门，外出。❺缁：黑色。❻素：白色。❼麑：小鹿。❽袂（mèi）：衣袖。❾寝衣：被子。❿帷裳：上朝时穿的礼服。⑪杀（shài）：减省。⑫吉月：农历每月初一。</div>
<div class="shiyi list fayin" onclick="run(this)">君子不用红青色镶衣边，不用红紫色做便服。夏天，穿粗的或细的葛布单衣，一定要外加一件上衣才出门。黑衣套在黑色羔裘上，白衣套在白色麑裘上，黄衣套在黄色狐裘上。居家的皮衣长，但右袖子短。斋戒期间一定有睡衣，比自身长一半。用狐貉的厚毛皮做座垫。服丧期满了后，无论什么饰物都可以佩戴。不是礼服就一定要剪裁。黑色羔裘、黑色帽子不用来吊丧。大年初一，一定穿好上朝礼服去朝拜君主。</div>
<div class="yuanwen list fayin" onclick="run(this)">齐<sup yin="斋">①</sup>，必有明衣<sup yin="">②</sup>，布<sup yin="">③</sup>。齐<sup yin="斋"></sup>必变食<sup yin="">④</sup>，居必迁坐<sup yin="">⑤</sup>。食不厌精，脍不厌细。食饐而餲<sup yin="ài">⑥</sup>，鱼馁而肉败，不食。色恶，不食。臭<sup yin="xiù">⑦</sup>恶，不食。失饪，不食。不时，不食。割不正，不食。不得其酱，不食。肉虽多，不使胜食气。唯酒无量，不及乱。沽酒市脯<sup yin="fǔ">⑧</sup>，不食。不撤姜食，不多食。祭于公，不宿肉<sup yin="">⑨</sup>。祭肉不出三日。出三日，不食之矣。食不语，寝不言。虽疏食菜羹，必祭<sup yin="">⑩</sup>，必齐<sup yin="斋"></sup>如也。席不正<sup yin="">⑪</sup>，不坐。</div>
<div class="shuoming list">❶齐：同“斋”，斋戒。❷明衣：浴衣。❸布：麻布。❹变食：改变日常饮食，不饮酒、不吃荤，禁食辛辣刺激的食物。❺迁坐：改变日常居住处所。❻食：主食、粮食。饐（yì）：食物经久腐臭。餲（ài），食物经久而变味。❼臭（xiù）：气味。❽脯（fǔ）：熟肉干，干肉。❾祭于公，不宿肉：古代有大夫助君祭祀之礼。国君祭祀，在临祭之日的清晨杀牲，祭后的第二天要再祭，称作绎祭，绎祭之后，将祭肉分赐给助祭者，以示均分神惠。这样，颁赐所得的祭肉，至少已历两天时间，不可再存放过夜。❿祭：这里的祭是祭最初发明饮食的人，以示回报。⑪席：坐席。古代没有椅子凳子，在地上铺上席子以为坐具。</div>
<div class="shiyi list fayin" onclick="run(this)">斋戒前沐浴，一定要有清洁的浴衣，用麻布做的。斋戒时一定改变饮食，一定要另居一室（单独住）。粮食不嫌做得精，鱼肉不嫌切得细。饭霉了臭了，不吃。鱼烂了肉腐了，不吃。食物颜色变坏了，不吃。味道难闻，不吃。烹饪不熟，不吃。不是吃饭的时间，不吃。砍割肉的部位方法不对，不吃。没有合适的调味品，不吃。席上肉虽然多，也不要吃得比主食还多。只有酒水不限多少，但不喝醉。买来的酒水和市场上的熟肉干，不吃。斋食总有姜。吃饭不过饱。（孔子）参与国君祭祀典礼，所分得的祭肉不过夜。其他祭肉，不超过三天。存放超过三天，就不吃了。（孔子）吃饭时不说话，入睡时不说话。（孔子吃饭时）虽然是糙米饭、蔬菜汤，饭前都要先分出一些祭祖，一定恭恭敬敬像斋戒一样。坐席摆放得不端正，不坐。</div>
<div class="yuanwen list fayin" onclick="run(this)">乡人饮酒<sup yin="">①</sup>，杖者出<sup yin="">②</sup>，斯出矣。乡人傩<sup yin="">③</sup>，朝服而立于阼<sup yin="zuò">zuò</sup>阶。</div>
<div class="shuoming list">❶乡人饮酒：此指举行乡饮酒礼，古代在乡里举行的一种礼仪，这种礼仪突出敬老的主题。❷杖者：指老人。❸傩（nuó）：古代举行驱逐疫鬼的一种仪式。</div>
<div class="shiyi list fayin" onclick="run(this)">和乡里人一起喝酒后，（孔子）会等老年人先离席了，自己才出去。乡里人举行驱逐疫鬼的仪式时，（孔子）穿着朝服站立在家庙东面的台阶上（以免先祖之神受到惊吓）。</div>
<div class="yuanwen list fayin" onclick="run(this)">问<sup yin="">①</sup>人于他邦，再拜而送之。 康子馈药，拜而受之，曰：“丘未达，不敢尝<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶问：问候，问好。这里指托别人代为致意。❷丘未达，不敢尝：古人对赠送的食物，能尝食的先要尝一下，以表示郑重。孔子因不了解药性，故说“不敢尝”。</div>
<div class="shiyi list fayin" onclick="run(this)">（孔子托人）向其他诸侯国友人问候送礼，要向托付的人连拜两次送行。季康子（派人）送药给孔子，孔子叩拜并接受了药，并说：“我不了解它的药性，不敢试尝。”</div>
<div class="yuanwen list fayin" onclick="run(this)">厩焚<sup yin="">①</sup>。子退朝，曰：“伤人乎？” 不问马。</div>
<div class="shuoming list">❶厩：马棚，马房。后也泛指牲口房。</div>
<div class="shiyi list fayin" onclick="run(this)">马棚失火。孔子从朝廷赶回来，问：“伤着人了吗？” 他却不问马怎么样。</div>
<div class="yuanwen list fayin" onclick="run(this)">君赐食，必正席先尝之。君赐腥<sup yin="">①</sup>，必熟而荐之<sup yin="">②</sup>。君赐生<sup yin="牲"></sup>，必畜之。侍食于君，君祭，先饭。疾，君视之，东首<sup yin="">③</sup>，加朝服，拖绅<sup yin="">④</sup>。君命召，不俟驾行矣<sup yin="">⑤</sup>。</div>
<div class="shuoming list">❶腥：生肉。❷荐：供奉，进献。这里指煮熟了肉先放在祖先灵位前上供，表示进奉。❸东首：指头超东。❹绅：朝服束在腰间的大宽带子。孔子因病卧床，不能穿朝服，故把朝服加盖在身上，把“绅”放在朝服上，拖下带子去，表示对国君的尊敬与迎接。 ❺俟：等待。</div>
<div class="shiyi list fayin" onclick="run(this)">君主赏赐熟食，（孔子）一定要摆正座位先尝一下（然后用来供奉祖先）。君主赏赐生肉，一定煮熟了再进供祖先。君主赏赐的牲畜，一定饲养起来（以示珍惜国君的恩惠）。陪君主用膳，君主举行食前祭祀，先替君主尝一尝（以此代君尝食，以表敬意）。（孔子）得病后，君主来探视，自己头朝东（面对国君来的方向）躺着，把朝服盖在身上，礼服上的大带拖垂下来（象征按正常礼节见国君）。君主召见，（孔子）不等车马准备好，就先动身走了。</div>
<div class="yuanwen list fayin" onclick="run(this)">入太庙，每事问。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子进入太庙，每件事都要问一问。</div>
<div class="yuanwen list fayin" onclick="run(this)">朋友死，无所归<sup yin="">①</sup>，曰：“于我殡。” 朋友之馈，虽车马，非祭肉<sup yin="">②</sup>，不拜。</div>
<div class="shuoming list">❶归：归宿。这里指后事的安排，如装殓，发丧，埋葬等。❷祭肉：指祭祀祖先用的胙肉。为了表示对朋友的祖先像对自己的祖先那样尊敬，在接受祭肉时要拜。</div>
<div class="shiyi list fayin" onclick="run(this)">朋友死了，没有亲属给他办丧事，孔子就说：“丧事由我操办吧。” 朋友的赠品，即便是车马，只要不是祭肉，（孔子在接受时）都不行拜礼。</div>
<div class="yuanwen list fayin" onclick="run(this)">寝不尸，居不客<sup yin="">①</sup>。见齐<sup yin="zī">zī</sup>衰<sup yin="缞">cuī</sup>者，虽狎<sup yin="">②</sup>，必变。见冕者与瞽者，虽亵<sup yin="">③</sup>，必以貌。凶服者式之<sup yin="">④</sup>，式负版者。有盛馔<sup yin="">⑤</sup>，必变色而作。迅雷风烈必变。</div>
<div class="shuoming list">❶客：宾客。这里用作动词。❷狎：亲近。❸亵：常见、熟悉。❹式：同“轼”，古代车厢前用作扶手的横木。 ❺负版：背着国家图籍。 ❻馔（zhuàn）：食物，多指美食。</div>
<div class="shiyi list fayin" onclick="run(this)">（孔子）睡觉，不像死尸那样直挺着；平日在家，不像见客做客那样过分讲究仪式。看见穿丧服的人，即使平日亲密无间，神情也必定变得严肃起来。看见官员和盲人，即使是很熟悉的人，也一定有礼貌。乘车时，遇见路上有穿丧服的，一定身子前倾、按住扶手（表示同情）；遇到背负国家图籍的，也一定前倾身子、按住扶手（表示敬意）。入席看见菜肴丰盛，一定改变神色，起身（对主人表示谢意）。打大雷刮大风时，一定一改常态，正襟危坐（表示对天敬畏）。</div>
<div class="yuanwen list fayin" onclick="run(this)">升车，必正立，执绥<sup yin="suí">①</sup>。车中，不内顾<sup yin="">②</sup>，不疾言，不亲指。</div>
<div class="shuoming list">❶绥（suí）：车上绳子，等车时作拉手用。❷不内顾：《鲁颂》无“不”字。“内顾”即收敛视线，不乱看之意。</div>
<div class="shiyi list fayin" onclick="run(this)">（孔子）上车前一定站直了，然后拉着车上的绳子（等车）。上车后不朝车内东看西看，不急冲冲地说话，不用手指指点点。</div>
<div class="yuanwen list fayin" onclick="run(this)">色斯举矣，翔而后集。曰：“山梁雌雉，时哉时哉！” 子路共<sup yin="拱"></sup>之，三嗅而作<sup yin="">①</sup>。</div>
<div class="shuoming list">❶嗅：唐代石经《论语》作“戛”字。戛，像鸟叫声。这一章寓意费解，很多人质疑文字有脱漏。译文暂据朱熹的理解。朱熹《四书集注》云：“言鸟见人之颜色不善，则飞去；回翔审视而后下止。人之见机而作，审择所处，亦当如此。”</div>
<div class="shiyi list fayin" onclick="run(this)">孔子一行惊动了一群野鸡飞起来。野鸡空中飞了一会发现这些人并没恶意。于是，后又停落在树丛中。（孔子）感慨说：“这些山梁上的雌雉鸡，识时务啊，识时务啊！”子路向它们肃然拱手，野鸡长叫了几声飞走了。</div>

`