$.jsname__13 =`
<h3>子路篇</h3>
<div class="yuanwen list fayin" onclick="run(this)">子路问政。子曰：“先之劳之<sup yin="">①</sup>。”请益。曰：“无倦。”</div>
<div class="shuoming list">❶先：导，引导，教导。</div>
<div class="shiyi list fayin" onclick="run(this)">子路问如何办政务，孔子说：“自己带头去做，然后役使百姓去做。”子路请求多讲一点，孔子说：“按上面说的去做不要倦怠就行。”</div>
<div class="yuanwen list fayin" onclick="run(this)">仲弓为季氏宰。问政。子曰：“先有司<sup yin="">①</sup>，赦小过，举贤才。”曰：“焉知贤才而举之？”曰：“举尔所知，尔所不知，人其舍诸？”</div>
<div class="shuoming list">❶有司：管理各项具体事物的吏官，主官的下属。</div>
<div class="shiyi list fayin" onclick="run(this)">仲弓去做季氏的管家，问起如何管理政事，孔子说：“对于手下办事的人，宽恕他们的小过错，选拔贤能的人才。”仲弓说：“怎么知道谁是贤才加以推举呢？”孔子说：“推举你了解的啊。你不了解的，别人会不了解吗？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子路曰：“卫君<sup yin="">①</sup>待子而为政，子将奚<sup yin="">②</sup>先？”子曰：“必也正名乎！”子路曰：“有是哉，子之迂也！奚其正？”子曰：“野哉，由也！君子于其所不知，盖阙如<sup yin="">③</sup>也。名不正，则言不顺；言不顺，则事不成；事不成，则礼乐不兴；礼乐不兴，则刑罚不中；刑罚不中，则民无所错<sup yin="措">④</sup>手足。故君子名之必可言也，言之必可行也。君子于其言，无所苟而已矣。”</div>
<div class="shuoming list">❶卫君：卫出公蒯辄。❷奚：何，什么。❸阙如：存疑；对还没搞清楚的疑难问题暂时搁置，不下判断；对缺乏确凿根据的事，不武断，不妄说。阙同“缺”。❹错：同“措”，放置，安排，处置。</div>
<div class="shiyi list fayin" onclick="run(this)">子路说：“如果卫国的君王等着先生去管理政务，先生打算先干什么？”孔子说：“必须先正名分呀！”子路说：“您真的迂腐到这个地步啊！要正什么名分？”孔子说：“您真粗野无礼啊，仲由！君子对自己不明白的，会暂且放到一边，不下断语。名分不正，说话就不顺理；说话不顺理，事情就办不成；事情办不成，礼乐就不兴盛；礼乐不兴盛，刑罚就不会恰当；刑罚不恰当，百姓就手足无措不知该怎么办。所以君子给各类政务政要政职定名一定要准，名分说出来，一定要可行。君子对于自己说的话，不会随随便便的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">樊迟请学稼。子曰：“吾不如老农。”请学为圃。曰：“吾不如老圃。”樊迟出。子曰：“小人哉，樊须也<sup yin="">①</sup>！上好礼，则民莫敢不敬；上好义，则民莫敢不服；上好信，则民莫敢不用情。夫如是，则四方之民襁负其子而至矣<sup yin="">②</sup>，焉用稼？”</div>
<div class="shuoming list">❶樊须：即樊迟。❷襁（qiǎng）：又称背单，背负小孩用的布单或布带。</div>
<div class="shiyi list fayin" onclick="run(this)">樊迟请教怎么种五谷，孔子说：“这我不如老农。”又请教怎么种菜，孔子说：“我不如老菜农。”樊迟出门后，孔子说：“这个樊须真是没有见识的小人啊！当权者热衷礼仪，老百姓没有哪个敢不恭敬的；当权者热衷仁义，老百姓没有哪个敢不服从的；当权者热衷诚信，老百姓没有哪个敢不真心待人。这样做的话，四面八方的人都会背着自己的小孩来投奔，哪里用得着在上的亲自种庄稼呢？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“诵《诗》三百，授之以政，不达<sup yin="">①</sup>；使于四方，不能专对<sup yin="">②</sup>；虽多，亦奚以为<sup yin="">③</sup>？”</div>
<div class="shuoming list">❶达：通达，通晓；会处理，会运用。❷专对：即根据外交的具体情况，随机应变，独立行事，回答问题，办理交涉。❸为：句末语气助词，表示感慨或疑问。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“三百首《诗》背熟了，把政务交给他，却办不了；派他出使四方，却不能独立地应对。（像这样的）即使读得再多，又有什么用呢？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“其身正，不令而行；其身不正，虽令不从。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“当权者如果自身品行端正，那么不用号令，百姓就会去做；如果自身品行不端正，那么即使发了号令，百姓也不会服从。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“鲁卫之政<sup yin="">①</sup>，兄弟也。”</div>
<div class="shuoming list">❶鲁卫之政：鲁国是周公（姬旦）的封地，卫国是周公的弟弟康叔的封地。鲁、卫本兄弟之国，后来衰乱又相似，孔子遂有这样的感叹。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“鲁国、卫国的国政，像兄弟一样（相似）。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子谓卫公子荆：“善居室<sup yin="">①</sup>。始有，曰：‘苟合矣<sup yin="">②</sup>。’少有，曰：‘苟完矣。’富有，曰：‘苟美矣。’”</div>
<div class="shuoming list">❶善句室：善于管理家业和财务经济，会过日子。❷苟：差不多，也算是。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子评论卫国公子荆，说：“公子荆善于管家理财。刚有一点起色，就说，‘不错了。’稍稍增加些家财，又说，‘差不多了。’家道殷实后，又说，‘差不多是完美了。’”</div>
<div class="yuanwen list fayin" onclick="run(this)">子适卫，冉有仆。子曰：“庶矣哉<sup yin="">①</sup>！”冉有曰：“既庶矣，又何加焉？”曰：“富之。”曰：“既富矣，又何加焉？”曰：“教之。”</div>
<div class="shuoming list">❶庶（shù）：多。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子到了卫国，冉有驾车。孔子说：“（卫国）人丁兴旺啊！”冉有问：“人丁兴旺了，进一步该怎么办？”孔子说：“让他们富裕。”又问：“富裕之后，再进一步怎么做了？”孔子说：“教育他们。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“苟有用我者，期<sup yin="jī">jī</sup>月<sup yin="">①</sup>而已可也，三年有成。”</div>
<div class="shuoming list">❶期（jī）月：一年十二个月，即一周年。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“假如有人任用我治理国家，一年便能有所起色，三年便能大见成效。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“‘善人为邦百年，亦可以胜残去杀矣。’诚哉是<sup yin="">①</sup>言也！”</div>
<div class="shuoming list">❶是：代词。这，此。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“‘善人治理国家一百年，也就可以战胜残暴，免除杀戮了。’这话确实正确啊！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“如有王者<sup yin="">①</sup>，必世而后仁<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶王者：能治国安邦、以德行仁德贤明君王。❷世：三十年是一世。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“即使有圣明的君王出现，也必定要经一代人的努力才能建成仁德的社会。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“苟正其身矣，于从政乎何有？不能正其身，如正人何？”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“假如自身品行已经端正了，那么治理政事还会有什么困难呢？假如不能端正自身品行，又怎么去端正别人？”</div>
<div class="yuanwen list fayin" onclick="run(this)">冉子退朝。子曰：“何晏也<sup yin="">①</sup>？”对曰：“有政。”子曰：“其事也，如有政，虽不吾以，吾其与闻之。”</div>
<div class="shuoming list">❶晏：晚，迟。</div>
<div class="shiyi list fayin" onclick="run(this)">冉求退朝回来，孔子说：“怎么回来这么晚呢？”冉求说：“有政事要商讨。”孔子说：“是私事吧。如果有政事，虽然季氏不用我，我也会听说的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">定公问：“一言而可以兴邦，有诸？”孔子对曰：“言不可以若是其几<sup yin="jī">①</sup>也。人之言曰：‘为君难，为臣不易。’如知为君之难也，不几乎一言而兴邦乎？”曰：“一言而丧邦，有诸？”孔子对曰：“言不可以若是其几也。人之言曰：‘予无乐乎为君，唯其言而莫予违也。’如其善而莫之违也，不亦善乎？如不善而莫之违也，不几乎一言而丧邦乎？”</div>
<div class="shuoming list">❶几（jī）：将近，接近。</div>
<div class="shiyi list fayin" onclick="run(this)">鲁定公问：“一句话就可以兴盛国家，有这种事吗？”孔子回答说：“一句话的效力不可能像这样，但接近这样效力的话或许是有的。有人说，‘做国君难，做臣子也不容易。’要是真知道做国君的难处，这不差不多是一句话使国家兴旺了吗？”定公又问：“一句话就使国家灭亡，有这种情况吗？”孔子回答说：“一句话的效力不可能像这样，但接近这样效力的话或许是有的”。有人说道，‘我做国君没有感受到什么快乐，唯一的快乐是我的话没人违抗。’ 要是话说对了没人违抗，不也是好事吗？假如话说错了没人违抗，不就差不多是一句话使国家灭亡了吗？”</div>
<div class="yuanwen list fayin" onclick="run(this)">叶<sup yin="shè"></sup>公问政<sup yin="">①</sup>。子曰：“近者说<sup yin="悦">②</sup>，远者来。”</div>
<div class="shuoming list">❶叶（shè）：姓沈，名诸侯，楚国大夫。❷说：同“悦”。</div>
<div class="shiyi list fayin" onclick="run(this)">叶<sup yin="shè"></sup>公问孔子怎么执政。孔子说：“要使您近处的人高兴愉快，使离您远的人来投奔您。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子夏为莒<sup yin="jǔ">①</sup>父宰，问政。子曰：“无欲速，无见小利。欲速，则不达；见小利，则大事不成。”</div>
<div class="shuoming list">❶莒（jǔ）父：鲁国的一个小城邑。</div>
<div class="shiyi list fayin" onclick="run(this)">子夏担任莒父城的长官，问孔子怎样办好政事。孔子说：“不要只求快，不要贪小利。图快就达不到主要目的，贪小利就办不成大事。”</div>
<div class="yuanwen list fayin" onclick="run(this)">叶<sup yin="shè">shè</sup>公语<sup yin="yù">yù</sup>孔子曰：“吾党有直躬者，其父攘羊，而子证之。”孔子曰：“吾党之直者异于是，父为子隐，子为父隐。直在其中矣<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶在孔子看来，父子互为隐瞒，是天理人情的率直表现，故这么说。</div>
<div class="shiyi list fayin" onclick="run(this)">叶<sup yin="shè"></sup>公对孔子说：“我们这里有位坦白直率的人，他父亲偷了羊，这儿子就去告发。”孔子说：“我们那里坦白直率的人跟您这位不同：父亲替儿子隐瞒，儿子替父亲隐瞒，我们那里的直率就提现在这里了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">樊迟问仁。子曰：“居处恭<sup yin="">①</sup>，执事敬<sup yin="">②</sup>，与人忠<sup yin="">③</sup>。虽之夷狄<sup yin="">④</sup>，不可弃也。”</div>
<div class="shuoming list">❶恭：恭敬庄重。❷敬：严肃认真。❸忠：忠诚恳切。❹之：动词。到，去，往。</div>
<div class="shiyi list fayin" onclick="run(this)">樊迟问什么是仁。孔子说：“平常在家恭敬庄重，（在外）办事严肃认真，对待别人忠诚恳切。（这三种品德）即使到了边远少数民族地方，也是不能丢弃的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子贡问曰：“何如斯可谓之士矣？”子曰：“行己有耻，使于四方，不辱君命，可谓士矣。”曰：“敢问其次？”曰：“宗族称孝焉，乡党称弟<sup yin="悌">①</sup>焉。”曰：“敢问其次？”曰：“言必信，行必果，硁硁<sup yin="kēng">kēng</sup>然<sup yin="">②</sup>小人哉！抑亦可以为次矣。”曰：“今之从政者何如？”子曰：“噫！斗筲<sup yin="shāo">②</sup>之人，何足算也！”</div>
<div class="shuoming list">❶弟：同“悌”，尊敬兄长。❷硁（kēng）：敲打石头的声音。硁硁然：形容浅薄固执的样子。❸斗筲（shāo）：比喻见识和器量狭小。筲，古代的饭筐，能容五升。</div>
<div class="shiyi list fayin" onclick="run(this)">子贡问道：“怎么才配称作土？”孔子说：“独自办事也有廉耻知道反省，出使四方能够不辜负君王的使命，可以称为士了。”子贡问：“请问次一等的。”孔子说：“宗族称赞他孝顺父母，乡亲们夸他尊敬兄长。”子贡问：“请问再次一等的。”孔子说：“说话一定讲信用，做事一定果断，这本是（不懂变通）固执的吓人呀！，不过也可以算是再次一等的士。”子贡问：“现在那些执政的人怎样？”孔子说：“唉！都是些器量狭小的人，哪里值得一提啊！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“不得中行<sup yin="">①</sup>而与<sup yin="yǔ">yǔ</sup>之，必也狂狷<sup yin="juàn">②</sup>乎。狂者进取，狷者有所不为也。”</div>
<div class="shuoming list">❶中行：合乎中庸之道的言行。❷狷（juàn）：指为人耿直拘谨，洁身自好，安分守己，不求有所作为亦不肯同流合污。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“找不到言行合乎中庸之道的人交往，那必定要同激进的人和耿直的人交往了！激进的人一味进取，耿直的人洁身自好。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“南人有言曰：‘人而无恒<sup yin="">①</sup>，不可以作巫医<sup yin="">②</sup>。’善夫！‘不恒其德，或承之羞。’” 子曰：“不占而已矣。”</div>
<div class="shuoming list">❶无恒：古人认为没有恒心是不吉利的。❷巫医：古代称占筮的为“巫”，往往兼作治病的“医”，所以巫医可以通称。这里说“不可以作巫医”，实际指的是巫。《礼记·缁衣》篇说：“南人有言曰：人而无恒，不可为卜筮。”可以为证。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“南方人有句话说：‘做人如果没有恒心，就不可以当巫医。’这句话说得好啊！（《易经》上说）‘不能长久保持德操，难免遭受羞辱。’”孔子说：“这种人不用占卜（也肯定知道是这种结果）了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子和而不同<sup yin="">①</sup>，小人同而不和。”</div>
<div class="shuoming list">❶和：和谐，调和，互相协调。同：相同，同类，同一。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子求和谐共处但不盲目附和，小人盲目附和却不求和谐相处。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子贡问曰：“乡人皆好<sup yin="hào">①</sup>之，何如？”子曰：“未可也。”“乡人皆恶<sup yin="wù">②</sup>之，何如？”子曰：“未可也。不如乡人之善者好之，其不善者恶<sup yin="wù"></sup>之。”</div>
<div class="shuoming list">❶好（hào）：喜爱，称道，赞扬。❷恶（wù）：憎恨，讨厌。</div>
<div class="shiyi list fayin" onclick="run(this)">子贡问道：“全乡人都喜欢他，这人怎样？”孔子说：“还不能认定他好。”“全乡人都讨厌他，这人怎样？”孔子说：“还不能认定他不好。不如乡里的好人喜欢他，坏人讨厌他（这样的人才能认定他好）。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子易事而难说<sup yin="悦"></sup>也<sup yin="">①</sup>。说<sup yin="悦"></sup>之不以道，不说<sup yin="悦"></sup>也；及其使人也，器之。小人难事而易说<sup yin="悦"></sup>也。说<sup yin="悦"></sup>之虽不以道，说<sup yin="悦"></sup>也；及其使人也，求备焉。”</div>
<div class="shuoming list">❶易事：易与共事，给他做事容易。说：同“悦”。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子容易共事，却难以讨好他取悦他。不用正当的方式讨他欢喜，他是不会高兴的。等到他任用人时，他能量才任用。小人难以共事，却容易讨他欢心。即使用不当的方式博他喜欢，他也会高兴。等到他任用人时，却百般挑剔，样样求全责备。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子泰而不骄，小人骄而不泰。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子心情坦荡而不骄傲自大，小人骄傲自大而心情不坦然。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“刚、毅、木<sup yin="">①</sup>、讷<sup yin="nè">②</sup>近仁。”</div>
<div class="shuoming list">❶木：质朴，朴实，憨厚老实。❷讷（nè）：说话迟钝。引申为言语非常谨慎，不肯轻易说话。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“刚强，坚毅，朴实，言语谨慎，这些接近于仁德品德。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子路问曰：“何如斯可谓之士矣？”子曰：“切切偲偲<sup yin="">①</sup>，怡怡如也<sup yin="">②</sup>，可谓士矣。朋友切切偲偲，兄弟怡怡。”</div>
<div class="shuoming list">❶切切偲偲（sīsī）：恳切地责勉、告诫，善意地互相批评；相互切磋，相互督促，和睦相处。❷怡怡：和气，安适，愉快。</div>
<div class="shiyi list fayin" onclick="run(this)">子路问道：“怎样做才可以称为士？”孔子说：“相互之间能恳切批评，和睦相处，可以称为士了。朋友之间相互批评，兄弟之间和睦相处。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“善人教民七年<sup yin="">①</sup>，亦可以即戎矣<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶善人：有作为的领导人。❷即：靠近，从事，参与。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“善人训练老百姓七年，也可以让他们当兵打仗了。”</div>

<div class="yuanwen list fayin" onclick="run(this)">子曰：“以不教民<sup yin="">①</sup>战，是谓弃之。”</div><div class="shuoming list">❶不教民：即“不教之民”。没有经过军事训练的人。</div><div class="shiyi list fayin" onclick="run(this)">孔子说：“用未经军事训练的民众打仗，这可以说是让他们去送命。”</div>`