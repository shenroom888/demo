$.jsname__11 =`
<h3>先进篇</h3>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“先进于礼乐<sup yin="">①</sup>，野人也<sup yin="">②</sup>；后进于礼乐，君子也<sup yin="">③</sup>。如用之，则吾从先进。”</div>
<div class="shuoming list">❶“先进”句：指先在学习礼乐方面有所进益，先掌握了礼乐方面的知识。❷野人：这里指庶民，没有爵禄的平民。与世袭贵族相对。❸君子：这里指有爵禄的贵族，世卿子弟。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“先学习礼乐（而后获得官职）的，是原本无爵无禄普通的人；（先有官位）后学习礼乐的，是卿、士大夫等贵族。如果选用人，我赞成选用先学习礼乐的人。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“从我于陈、蔡者<sup yin="">①</sup>，皆不及门也<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶从我于陈、蔡者：公元前489年，孔子带着颜渊、子路等弟子从陈国到蔡国去，途中曾遭陈国人围困，绝粮七天，弟子们饿得站不起来。后因楚人相助，才摆脱困境。❷及门：在某人门下当学生。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“曾跟随我在陈国、蔡国（经历过艰难困境）的弟子，现在都已不在我的门下了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">德行：颜渊、闵子骞、冉伯牛、仲弓。言语：宰我、子贡。政事：冉有、季路。文学<sup yin="">①</sup>：子游、子夏。</div>
<div class="shuoming list">❶文学：指诗书礼乐等文化知识，即通晓西周文献典籍。</div>
<div class="shiyi list fayin" onclick="run(this)">（孔子弟子中）德行突出的有：颜渊，闵子骞，冉伯牛，仲弓。擅长辞令的有：宰我，子贡。善于处理政事的有：冉有，季路。通晓诗书礼乐等知识的有：子游，子夏。</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“回也，非助我者也，于吾言无所不说<sup yin="悦">①</sup>。”</div>
<div class="shuoming list">❶说：同“悦”。这里是说颜渊对孔子的话从来不提出疑问或反诘。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“颜回呀，他不是对我有所帮助的人，因为他对我所讲的话没有不心悦诚服的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“孝哉，闵子骞！人不间<sup yin="jiàn">jiàn</sup>于其父母昆弟之言<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶间（jiàn）：空隙，此是批判、非议的意思。昆：兄。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“孝顺啊闵子骞！他父母兄弟夸他，别人都不怀疑。”</div>
<div class="yuanwen list fayin" onclick="run(this)">南容三复白圭<sup yin="">①</sup>，孔子以其兄之子妻之<sup yin="">②</sup>。</div>
<div class="shuoming list">❶“白圭”：这里指《诗经·大雅·抑》中有关白圭的四句诗：“白圭之玷，尚可磨也；斯言之玷，不可为也。”意思是白圭（一种珍贵而莹洁的玉）上的污点还能磨掉，人们言语中的错误却是不可挽回的，所以言语必须谨慎。❷妻（qì）：名词用作动词，做妻子。</div>
<div class="shiyi list fayin" onclick="run(this)">南容多次诵读有关“白圭”的那几句诗来告诫自己要谨慎，孔子便将自己哥哥的女儿嫁给了他。</div>
<div class="yuanwen list fayin" onclick="run(this)">季康子问：“弟子孰为好学？”孔子对曰：“有颜回者好学，不幸短命死矣，今也则亡<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶亡：无。</div>
<div class="shiyi list fayin" onclick="run(this)">季康子问：“在里弟子中哪个最好学？” 孔子回答说：“有个叫颜回的弟子最好学，不幸短命死了！现在没有像他那样好学的了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">颜渊死，颜路请子之车以为之椁<sup yin="guǒ">①</sup>。子曰：“才不才<sup yin="">②</sup>，亦各言其子也。鲤也死，有棺而无椁。吾不徒行以为之椁，以吾从大夫之后，不可徒行也。”</div>
<div class="shuoming list">❶椁（guǒ）：古代有地位的人，死后所用棺材至少有两层，里面一层叫棺，外面一层叫椁。❷才：有才能。此指颜渊。不才：没有才能。此指孔鲤。</div>
<div class="shiyi list fayin" onclick="run(this)">颜渊死了，（他父亲）颜路请求孔子把自己的车子卖了为颜渊买一具外椁。孔子说：“无论是有才能还是没有才能，各人讲起来也都是自己的儿子。我儿子鲤死的时候，也只有棺而没有椁。我不能为了给他买椁而卖掉车子步行。因为我以前曾经做过大夫，是不可以步行的”</div>
<div class="yuanwen list fayin" onclick="run(this)">颜渊死。子曰：“噫！天丧予<sup yin="">①</sup>！天丧予！”</div>
<div class="shuoming list">❶丧：亡，使......灭亡。</div>
<div class="shiyi list fayin" onclick="run(this)">颜渊死了。孔子痛呼道：“唉！老天要我命啊！老天要我命啊！”</div>
<div class="yuanwen list fayin" onclick="run(this)">颜渊死，子哭之恸<sup yin="">①</sup>。从者曰：“子恸矣！” 曰：“有恸乎？非夫人之为恸而谁为<sup yin="">②</sup>？”</div>
<div class="shuoming list">❶恸：极度哀痛，悲伤。❷非夫人之为恸而谁为：即“非为夫人恸而为谁”的倒装。“之”是虚词，在语法上只起到帮助倒装的作用。</div>
<div class="shiyi list fayin" onclick="run(this)">颜回死了，孔子哭得十分伤心。随行弟子说：“先生哭得太悲伤了！” 孔子说：“悲伤吗？不为他这样的人悲伤，还为谁悲伤啊！”</div>
<div class="yuanwen list fayin" onclick="run(this)">颜渊死，门人欲厚葬之，子曰：“不可<sup yin="">①</sup>。” 门人厚葬之。子曰：“回也视予犹父也，予不得视犹子也。非我也，夫二三子也。”</div>
<div class="shuoming list">❶不可：孔子认为颜渊家中贫困，死后厚葬不恰当。</div>
<div class="shiyi list fayin" onclick="run(this)">颜回死了，孔子的学生要厚葬他，孔子说：“不可以。” 学生们还是厚葬了颜回。孔子说：“颜回把我看作他父亲，现在我却不能对他像儿子一样（为他的丧事定主意）。（厚葬）这不是我的本意啊，是这些同学们的主意啊！”</div>
<div class="yuanwen list fayin" onclick="run(this)">季路问事鬼神。子曰：“未能事人，焉能事鬼？” 曰：“敢问死。”曰：“未知生，焉知死？”</div>
<div class="shiyi list fayin" onclick="run(this)">子路问怎样侍奉鬼神，孔子说：“连活人都没有侍奉好，还怎么能去侍奉鬼神呢？” 季路又说：“冒昧请教一下，死是怎么回事？” 孔子说：“连生的道理都没搞清楚，怎么能知道死？”</div>
<div class="yuanwen list fayin" onclick="run(this)">闵子侍侧，訚<sup yin="yín">yín</sup>訚<sup yin="">①</sup>如也；子路，行<sup yin="hàng"></sup>行<sup yin="hàng">②</sup>如也；冉有、子贡、侃侃如也，子乐。“若由也，不得其死然。”</div>
<div class="shuoming list">❶闵子：即闵子骞。后人敬称“子”。❷訚訚（yín）：和悦而能中正直言。❸行行（hàng）：形容性格刚强勇猛。</div>
<div class="shiyi list fayin" onclick="run(this)">侍侯在孔子身边，闵子骞是一副恭敬正直的样子；子路，刚强不屈的样子；冉有、子贡，是和颜悦色的样子。孔子感到很高兴。但又担心：“像仲由这样，只怕不能寿终而死啊。”</div>
<div class="yuanwen list fayin" onclick="run(this)">鲁人为长<sup yin="cháng">cháng</sup>府<sup yin="">①</sup>。闵子骞曰：“仍旧贯<sup yin="">②</sup>，如之何？何必改作？” 子曰：“夫人不言，言必有中<sup yin="zhòng">zhòng</sup>。”</div>
<div class="shuoming list">❶长府：鲁国藏财货的库名。❷仍：因，沿袭。</div>
<div class="shiyi list fayin" onclick="run(this)">鲁国人要翻修长府。闵子骞说：“就照老样子，怎么样？为什么一定要改建呢？” 孔子听后说：“闵子骞这个人要么不爱说话，一旦说话就切中要害。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“由之瑟奚为于丘之门<sup yin="">①</sup>？” 门人不敬子路。子曰：“由也升堂矣，未入于室也<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶旧注认为，子路气质刚勇，弹瑟发出的声音缺乏平和安详的音色旋律，所以孔子不喜欢。瑟：古代的一种乐器，与琴相似。❷升堂、入室：这里比喻做学问由浅入深的过程。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“仲由弹瑟的水平，哪能配在我门下弹呢？” 其他弟子因此瞧不起子路。孔子（因此又解释）说：“仲由啊，（学问已经不错，但还没有达到精深的程度，就像）已经登上正厅了，还未能进入内室啊。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子贡问：“师与商<sup yin="">①</sup>也孰贤？” 子曰：“师也过，商也不及。” 曰：“然则师愈与<sup yin="欤"></sup>？” 子曰：“过犹不及。”</div>
<div class="shuoming list">❶师与商：师，颛孙师，字子张。商，卜商，字子夏。</div>
<div class="shiyi list fayin" onclick="run(this)">子贡问：“颛孙师和卜商哪个贤明？” 孔子说：“颛孙师做事过头，卜商做事又达不到要求。” 子贡说：“既然这样，颛孙师贤能一些，是吗？” 孔子说：“过了头和达不到，同样不好。”</div>
<div class="yuanwen list fayin" onclick="run(this)">季氏富于周公，而求也为之聚敛而附益之<sup yin="">①</sup>。子曰：“非吾徒也，小子鸣鼓而攻之，可也。”</div>
<div class="shuoming list">❶指冉求帮助季氏进行田赋改革以增加赋税一事。</div>
<div class="shiyi list fayin" onclick="run(this)">季氏比周公还富，冉求却还为他继续搜刮、积聚，增加他的财富。孔子说：“（冉求）不是我的弟子！弟子们，你们可以擂起鼓来大造声势地去声讨他。”</div>
<div class="yuanwen list fayin" onclick="run(this)">柴也愚<sup yin="">①</sup>，参<sup yin="shēn">shēn</sup>也鲁，师也辟<sup yin="僻"></sup>，由也喭<sup yin="yàn">②</sup>。</div>
<div class="shuoming list">❶柴：指高柴，字子羔。孔子弟子。❷喭（yàn）：刚烈莽撞。</div>
<div class="shiyi list fayin" onclick="run(this)">高柴愚拙，曾参迟钝，颛孙师偏激，仲由莽撞。</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“回也其庶乎，屡空<sup yin="">①</sup>。赐不受命，而货殖焉，亿<sup yin="臆">②</sup>则屡中<sup yin="zhòng">zhòng</sup>。”</div>
<div class="shuoming list">❶屡空：常常缺衣少食。❷亿：同“臆”，猜度。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“颜回（的道德学问）可说是差不多了吧，但他常常缺衣少食。端木赐没有经过准许而去经商投机，猜测行情竟常常猜中。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子张问善人<sup yin="">①</sup>之道。子曰：“不践迹，亦不入于室<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶善人：朱熹认为，“善人”是本质好而未经过学习的人。 ❷入于室：即“到了家”，此处指学问、品德修养。</div>
<div class="shiyi list fayin" onclick="run(this)">子张问成为善人的方法。孔子说：“（善人）不踩着（前人的）足迹走，也就不能达到前人道德修养的境界。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“论笃是与<sup yin="">①</sup>，君子者乎？色庄者乎？”</div>
<div class="shuoming list">❶论笃是与：等于“与论笃”。论笃，言论诚恳笃实的人。与，赞许。“是”无实义，起帮助“论笃”这一宾语提前的语法作用。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“（人们）称赞说话诚实的人，但这个人究竟是真君子呢，还是仅仅表面上伪装庄重正经呢？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子路问：“闻斯行诸？” 子曰：“有父兄在，如之何其闻斯行之？” 冉有问：“闻斯行诸？” 子曰：“闻斯行之。” 公西华曰：“由也问‘闻斯行诸’，子曰：‘有父兄在’；求也问‘闻斯行诸’，子曰：‘闻斯行之’。赤也惑，敢问。” 子曰：“求也退，故进之；由也兼人<sup yin="">①</sup>，故退之。”</div>
<div class="shuoming list">❶兼人：指刚勇，敢作敢为，一个人能顶两个人。</div>
<div class="shiyi list fayin" onclick="run(this)">子路问：“听到道理马上就去做吗？” 孔子说：“有父亲和老兄在你前头呢，怎么能（不经请示）听了就去做呢？” 冉有问：“听到道理马上就去做吗？” 孔子说：“听到了就马上去做！” 公西华说：“仲由问‘听到了就做吗’，老师说‘有父兄在前头’；冉求问‘听到了就做吗’，老师却说‘听到了马上就做’。学生不明白了，请老师解惑。” 孔子说：“冉求有点畏缩，左顾右盼的，所以给他打打气；子路胆大过人，所以要抑制约束他一下。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子畏于匡，颜渊后。子曰：“吾以女<sup yin="汝">rǔ</sup>为死矣！”曰：“子在，回何敢死<sup yin="">①</sup>！”</div>
<div class="shuoming list">❶子在：回何敢死；您如果死了，我才敢为您殉死。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子在匡地被围困，弟子走散了，颜渊最后才返回。孔子惊喜道：“我还以为你死了呢！”颜渊兴奋地说：“老师健在，颜回哪里敢死！”</div>
<div class="yuanwen list fayin" onclick="run(this)">季子然<sup yin="">①</sup>问：“仲由、冉求，可谓大臣与<sup yin="欤">②</sup>？”子曰：“吾以子为异之问，曾<sup yin="">③</sup>由与求之问。所谓大臣者，以道事君，不可则止。今由与求也，可谓具臣矣。”曰：“然则从之者与<sup yin="欤"></sup>？”子曰：“弑父与君，亦不从也。”</div>
<div class="shuoming list">❶季子然：鲁国季氏家族中的人。❷当时仲由、冉求担任季氏的家臣。❸曾：相当于“乃”，却，竟然的意思。</div>
<div class="shiyi list fayin" onclick="run(this)">季子然问：“仲由、冉求能否称得上大臣吗？” 孔子说，“我还以为你问别的什么人啊，原来是问仲由和冉求。我们说的大臣啊，就是用仁道去辅佐君王，如果这样行不通，可以辞职。至于仲由和冉求现今的所作所为，只能算是具备做大臣的才能。” 季子又问：“那么他们会一切听从（季氏）吗？” 孔子说：“如果是弑父杀君的事，他们是不会听从的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">11.25子路使子羔为费鄪<sup yin="bì">bì</sup>宰。子曰：“贼夫人之子。” 子路曰：“有民人焉，有社稷<sup yin="">①</sup>焉，何必读书，然后为学？” 子曰：“是故恶夫佞者。”</div>
<div class="shuoming list">❶社稷：社，土神；稷，谷神。</div>
<div class="shiyi list fayin" onclick="run(this)">子路让子羔做费城的地方官。孔子说：“这是害了人家的子弟。”子路说：“那里有百姓，有土地和五谷（可以学到治民祭神的学问），何必非要读书才算做学问呢？”孔子说：“所以，我讨厌强词夺理狡辩的人。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子路、曾皙、冉有、公西华侍坐<sup yin="">①</sup>。子曰：“以吾一日长<sup yin="zhǎng">zhǎng</sup>乎尔，毋吾以也。居则曰：‘不吾知也！’如或知尔，则何以哉？”子路率尔而对曰：“千乘<sup yin="shèng">shèng</sup>之国，摄乎大国之间，加之以师旅，因之以饥馑；由也为之，比及三年，可使有勇，且知方也。” 夫子哂之<sup yin="">②</sup>。“求！尔何如？”  对曰：“方六七十，如五六十，求也为之，比及三年，可使足民。如其礼乐，以俟君子。” “赤！尔何如？” 对曰：“非曰能之，愿学焉。宗庙之事，如会同<sup yin="">③</sup>，端章甫<sup yin="">④</sup>，愿为小相焉。” “点！尔何如？” 鼓瑟希，铿尔，舍瑟而作，对曰：“异乎三子者之撰。”</div>
<div class="shuoming list">❶曾皙：名点，字子皙，曾参之父，也是孔子的弟子。❷哂：讥讽地微笑。❸会同：古代诸侯不在规定的时间去朝见天子，叫“会”，与其他诸侯一起去朝见，叫“同”。后两君相见也叫“会”。❹端：玄端，当代礼服的名称。章甫：古代礼帽的名称。</div>
<div class="shiyi list fayin" onclick="run(this)">子路、曾皙、冉有、公西华陪坐在孔子身边。孔子说：“因我年岁比你们都大，但你们不要对此介意而不敢讲真话。你们平日闲居时总是说：‘没有人了解啊！’如果有人了解你们（并任用你们），那你们准备怎么办呢？”子路脱口就答道：“如果有一个千乘<sup yin="胜"></sup>之国，夹在大国的中间，遭受外国军队的侵犯，国内又连年闹饥荒，让我去治理这个国家，只需要三年，便可以使那里人人有勇气，个个懂道义。”</div>
<div class="shiyi list fayin" onclick="run(this)">孔子（听了）淡然一笑。（接着又问）“冉求，你打算怎样？” （冉求）回答道：“方圆六七十里，或五六十里的地方，由我去治理，三年后，可使那里的百姓生活富足；至于那里的礼乐教化，就有待贤人君子去施行了。”（孔子又问）“公西赤，你怎么样？” （公西赤）回答道：“不敢说我能担任，只是愿意向这个方面学习。宗庙祭祀的事，或同别的国家举行盟会时，穿上玄端礼服戴上章甫礼帽，我愿当一个小小的司仪。”（孔子又问）“曾点，你怎么样？” 弹瑟的声音稀疏了，“铿”地一声停了下来，（曾点）放下瑟起身回答道：“我的志向跟他们三位不同。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“何伤乎？亦各言其志也！”曰：“莫<sup yin="暮">①</sup>春者，春服既成，冠<sup yin="guàn">guàn</sup>者五六人，童子六七人，浴乎沂<sup yin="">②</sup>，风乎舞雩<sup yin="yú">③</sup>，咏而归。”夫子喟<sup yin="kuì">④</sup>然叹曰：“吾与<sup yin="yǔ">yǔ</sup>点也！”三子者出，曾皙<sup yin="xī">xī</sup>后。曾皙曰：“夫三子者之言何如？” 子曰：“亦各言其志也已矣。” 曰：“夫子何哂由也？” 曰：“为国以礼，其言不让，是故哂之。”“唯求则非邦也与<sup yin="欤"></sup>？” “安见方六七十，如五六十而非邦也者？” “唯赤则非邦也与<sup yin="欤"></sup>？” “宗庙会同，非诸侯而何？赤也为之小，孰能为之大？”</div>
<div class="shuoming list">❶莫：同“暮”。❷沂：水名。❸舞雩（yú）：祭天求雨的地方，有坛有树木。雩，本是求雨的祭名，舞雩时，伴有舞蹈，故称舞雩。❹喟（kuì）：叹息。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“有什么关系呢？只不过各自谈谈自己的打算罢了。”（曾点）说：“暮春时节，春天的衣服已经穿在身上，约上五六个成年人，带上六七个小孩，到沂水中洗一洗，在舞雩台上吹吹风，然后一路唱着歌走回来。” 孔子长叹一声说：“我赞同曾点（的想法）啊!” 子路等三人走出了门，曾皙最后走。（曾皙）问道：“他们三人所说的怎样？”
孔子说：“不过是各人谈谈自己的打算罢了。” （曾皙）问：“那您为什么要笑仲由呢？”</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“治理国家应当讲究礼让，而他谈话却不谦虚，因此要笑他。”（曾皙说）“难道冉求所说的就不是（治理）国家吗？”（孔子说）“哪里有方圆六七十里或五六十里的地方还称不上一个国家的呢？”（曾皙说）“那么公西华不能治国吗？”（孔子说）“宗庙祭祀，同外国盟会，这不是诸侯国的事又是什么？假如连公西华都只能做小事，谁还能做大事呢？（可见他们两人都比仲由谦虚啊）”</div>

`