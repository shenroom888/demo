$.jsname__01 =`
            <h3>学而篇</h3>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“学而时习之，不亦说<sup yin="悦">①</sup>乎？有朋自远方来，不亦乐乎？人不知而不愠<sup yin="愠">②</sup>，不亦君子乎？</div>            
            <div class="shuoming list">❶说（yuè）：通假字同“悦”，高兴，喜悦，愉快。❷愠（yùn）：生气，发怒。❸君子：原义是指有较高社会地位的人，统治阶层的人，引申为道德上有修养的人。这里用的是引申义。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子说：学了知识能反复地温习它，不也是件愉悦的事吗？有志同道合的人远道而来，不也是件快乐的事吗？别人不了解我，我并不怨恨，不也是个有修养的人吗？</div>
            <div class="yuanwen list fayin" onclick="run(this)">有子曰<sup yin="">①</sup>：“其为人也孝弟<sup yin="悌">②</sup>，而好<sup yin="号"></sup>犯上者，鲜<sup yin="险"></sup>矣；不好<sup yin="号"></sup>犯上，而好<sup yin="号"></sup>作乱者，未之有也。君子务本，本立而道生。孝弟<sup yin="悌"></sup>也者，其为仁之本与<sup yin="欤"></sup>！”<sup yin="">③</sup></div>
            <div class="shuoming list">❶有子：姓有，名若。孔子的弟子。❷孝弟：孝，儒家伦理重要德目之一，指子女对长辈的敬爱顺从。弟（t）同“悌”，指弟弟尊重顺从兄长。❸仁：孔子提出的道德伦理的最高标准，也是孔子思想的核心概念，其主要意义是由于意识到人是同类而产生的对他人的同情爱护，即所谓“仁者爱人”。与：同“欤”，语气词。</div>
            <div class="shiyi list fayin" onclick="run(this)">有子说：“（如果）为人孝顺父母，尊重兄长，却喜欢冒犯长辈或上级，这种人是很少有的；不喜欢犯上，却喜欢作乱，这种人是不会有的。君子应致力于根本的工作，根本确立，正德自然产生。孝顺父母，尊重兄长，这些准则应是施行仁道的基础吧！”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“巧言令色<sup yin="">①</sup>，鲜<sup yin="险"></sup>矣仁。”</div>
            <div class="shuoming list">❶巧：好。令色：好的脸色。这里指假装和善。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子说：“花言巧语，面貌伪善的人是很少有仁德的。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">曾子曰<sup yin="">①</sup>：“吾日三省<sup yin="醒"></sup>吾身：为人谋而不忠乎<sup yin="">②</sup>？与朋友交而不信乎？传不习乎<sup yin="">③</sup>？”</div>
            <div class="shuoming list">❶曾子：姓曾，名参（shen），字子舆。孔子的弟子。 ❷忠：儒家德目之一，对他人（特别是对君主） 尽心竭力。❸传：老师的传授。</div><div class="shiyi list fayin" onclick="run(this)">曾子说：“我每天多次反省自己：为别人出主意做事情是否尽力呢？与朋友交往有没有做到以诚相待呢？老师传授的学业有没有经常复习呢？”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“道<sup yin="倒"></sup>千乘<sup yin="胜"></sup>之国<sup yin="">①</sup>，敬事而信，节用而爱人，使民以时。”</div><div class="shuoming list">❶道（dao）：治理。千乘之国：古代四匹马拉一辆兵车称作“一乘”。周制天子地方千里，出兵车万乘；诸侯地方百里，出兵车千乘。“千乘之国”指代诸侯国。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子说：“治理一个实力雄厚的诸侯大国，要严肃认真地处理政事，讲究信用，节省财政开支，爱护臣下，按照农业的忙闲决定何时役使人民。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“弟子<sup yin="">①</sup>人则孝，出则悌，谨而言，泛爱众，而亲仁。行有余力，则以学文 <sup yin="">②</sup>。”</div>
            <div class="shuoming list">❶弟子：年级幼小的人。❷文：《诗》《书》等六艺之文。</div><div class="shiyi list fayin" onclick="run(this)">孔子说：“年轻的人，在父母身边要孝顺父母，离开家要敬重兄长，言语谨慎守信，博爱众人，亲近有仁德的人。做到这些后还有余力，就用来学习文化技艺。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子夏曰<sup yin="">①</sup>：“贤贤易色<sup yin="">②</sup>；事父母，能竭其力；事君，能致其身；与朋友交，言而有信。虽曰未学，吾必谓之学矣。”</div>
            <div class="shuoming list">❶子夏：姓卜，名商，字子夏。孔子的弟子。 ❷贤贤易色：看重德行，轻视表面态度。易：轻视。</div><div class="shiyi list fayin" onclick="run(this)">子夏说：“尊重实际的道德，看轻表面的态度；侍奉父母，能竭尽全力；侍奉君主，能不惜献出生命；结交朋友，说话诚实守信用。这样的人虽然自谦说没有经过学习，我必定说他学习过了。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“君子不重则不威，学则不固<sup yin="">①</sup>。主忠信，无友不如己者<sup yin="">②</sup>，过，则勿惮改。”</div>
            <div class="shuoming list">❶固：无知，鄙陋，顽固。❷毋：不要。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子说：“君子举止如果不庄重，就不会有威严；学问要研究，知道学习才不会无知顽固。要把忠诚和守信放在（待人处事的）主要地位上。不要与德行不如自己的人交朋友。犯了错误，要不怕改正。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">曾子曰：“慎终<sup yin="">①</sup>，追远<sup yin="">②</sup>，民德归厚矣。”</div>
            <div class="shuoming list">❶ 终：寿终，指父母去世。❷远：远祖，祖先。</div>
            <div class="shiyi list fayin" onclick="run(this)">曾子说：“慎重地对待父母的死亡，虔诚地祭祀历代先祖，社会道德就会趋向淳朴厚道了。”</div><div class="yuanwen list fayin" onclick="run(this)">子禽问于子贡曰<sup yin="">①</sup>：“夫子至于是邦也，必问其政，求之于，抑与之与？”子贡曰：“夫子温、良、恭、俭、让以得之<sup yin="">②</sup>。夫子之求之也，其诸异乎人之求之与？”</div>
            <div class="shuoming list">❶ 子禽：姓陈，名亢，字子禽。有人认为是孔子的弟子。子贡：姓端木，名赐，字子贡。孔子的弟子。❷温、良、恭、俭、让：温顺、善良、恭敬、俭朴、谦让。这是孔子的弟子对他的赞誉。</div>
            <div class="shiyi list fayin" onclick="run(this)">子禽问子贡道：“先生到了一个国家，总能听到这个国家的政事，是自己有心去打听的呢，还是人家主动告诉他的呢？”子贡说：“先生是靠为人温和、善良、恭敬、俭朴，谦逊（使人乐意主动向他讲述）而了解到情况的。先生这种取得别人信任而获知政事的方法，也许同别人不一样吧？”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“父在，观其志；父没，观其行；三年无改于父之道<sup yin="">①</sup>，可谓孝矣。”</div>
            <div class="shuoming list">❶道：指父亲在世时所奉行的行为准则、道德规范等。</div>
            <div class="shiyi list fayin" onclick="run(this)">孔子说：“一个人，他父亲在世时，看他的志向主张；其父亲去世后，要看他的行为。如果长时间遵照他父亲生前的做法，就称得上孝了。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">有子曰：“礼之用，和为贵<sup yin="">①</sup>。先王之道<sup yin="">②</sup>，斯为美。小大由之。有所不行，知和而和，不以礼节之，亦 不可行也。”</div>
            <div class="shuoming list">❶和：恰到好处，适中。 ❷先王：指周文王等古代的贤王。</div>
            <div class="shiyi list fayin" onclick="run(this)">有子说：“礼的施行以和谐为美。先王的治国之道，其妙处就在这里。小事大事都要依次而行。但若有行不通的时候，只知道一味求和谐，而不用礼来节制，那也是不行的。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">有子曰：“信近于义<sup yin="">①</sup>，言可复也<sup yin="">②</sup>。恭近于礼，远耻辱也<sup yin="">②</sup>。因不失其亲，亦可宗也。”</div><div class="shuoming list">❶近：符合，接近。 ❷复：实践，实行。 ❸ 远：避免，免去。</div>
            <div class="shiyi list fayin" onclick="run(this)">有子说：“诺言符合义的原则，才可以实践、兑现。对别人恭敬庄重符合礼的原则，才可以免遭耻辱。依靠亲近自己的人，也才有可能靠得住。”</div><div class="yuanwen list fayin" onclick="run(this)">子曰：“君子食无求饱，居无求安，敏于事而慎于言，就有道而正焉<sup yin="">①</sup>，可谓好学也已。”</div>
            <div class="shuoming list">❶就：靠近，接近。</div><div class="shiyi list fayin" onclick="run(this)">孔子说：“君子吃饭不追求足饱，居住不追求安逸，做事敏捷，说话谨慎，主动向有道德的人学习并改正自己的缺点，这就可以称得上好学了。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子贡曰：“贫而不谄，富而无骄，何如？”子曰：“可也。未若贫而乐道，富而好礼者也。”子贡曰：“《诗》云：‘如切如磋，如琢如磨。’其斯之谓与？”子曰：“赐也，始可与言《诗》已矣，告诸往而知来者。”</div>
            <div class="shiyi list fayin" onclick="run(this)">子贡说：“贫穷却不巴结奉承，富有却不骄纵，这样做人怎么样？”孔子说：“可以了。但还比不上贫穷却乐于求道，富有却崇尚礼节的人。”子贡说：“《诗经》上说：‘（君子的自我修养像匠人加工骨器、玉器那样）先锯开锉平，再雕琢磨光。’大楷就是这个意思吧？”孔子说：“呀，现在可以同你谈论《诗经》了，告诉了你一个道理，你能类推悟出新的道理了。”</div>
            <div class="yuanwen list fayin" onclick="run(this)">子曰：“不患人之不已知<sup yin="">①</sup>，患不知人也。”</div>
            <div class="shuoming list">❶ 患：担心，忧愁。</div><div class="shiyi list fayin" onclick="run(this)">孔子说：“不担心别人不了解自己，只忧虑自己不理解别人。”</div>
            `