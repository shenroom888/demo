$.jsname__09 =`
<h3>子罕篇</h3>
<div class="yuanwen list fayin" onclick="run(this)">子罕言利与命与<sup yin="">①</sup>仁。</div>
<div class="shuoming list">❶与：赞同，肯定。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子很少谈论功利，赞同天命和仁德。</div>
<div class="yuanwen list fayin" onclick="run(this)">达巷党<sup yin="">①</sup>人曰：“大哉孔子，博学而无所成名。”子闻之，谓门弟子曰：“吾何执？执御乎，执射乎？吾执御矣。”</div>
<div class="shuoming list">❶达巷党：达巷，地名。党，古代一种居民组织，五百家为一党。</div>
<div class="shiyi list fayin" onclick="run(this)">达巷党这地方的人说：“孔子真伟大啊！学识渊博竟至于没有哪一方面可以使他成名。” 孔子听到后，对弟子们说：“我该专学哪一项技艺呢？驾车呢？还是射箭呢？我还是驾车吧。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“麻冕<sup yin="">①</sup>，礼也；今也纯<sup yin="">②</sup>，俭，吾从众。拜下，礼也；今拜乎上，泰<sup yin="">③</sup>也。虽违众，吾从下。”</div>
<div class="shuoming list">❶麻冕：麻布礼帽。按规定，用做礼帽的麻布要用两千四百根麻线织成，很费工。后改用丝质礼帽，俭省了工力工时。❷纯：黑色的丝。❸泰：骄纵。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“用麻料做礼帽，这是符合礼节的规定的；现在用丝做，这样节省些，我赞同大家的做法。（臣见君时）先在堂下跪拜，（登堂后再拜）这是礼的规定；现在大家直接登堂拜见，太傲慢了，虽然与众不同，我还是主张遵礼，先在堂下跪拜。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子绝四：毋意<sup yin="">①</sup>，毋必<sup yin="">②</sup>，毋固，毋我。</div>
<div class="shuoming list">❶意：同“臆”，主观的想法，缺乏客观证据。❷必：不如变通。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子杜绝了四种缺点：不凭空猜想揣测，不事先定论，不固执已见，不自以为是。</div>
<div class="yuanwen list fayin" onclick="run(this)">子畏于匡<sup yin="">①</sup>，曰：“文王既没，文不在兹乎？天之将丧斯文也，后死者<sup yin="">②</sup>不得与<sup yin="yù">yù</sup>于斯文也；天之未丧斯文也，匡人其如予何？”</div>
<div class="shuoming list">❶子畏于匡：据《史记·孔子世家》记载，孔子在去陈国途中曾路过匡地。匡人以前曾遭鲁国阳虎的残害，而孔子相貌颇像阳光，被匡人误认为阳虎而围困起来。五天后，知道不是阳虎后才放了他。畏：围困。❷后死者：孔子自称。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子在匡地被当地人拘押起来，说：“文王死后，古代文化遗产不就保存在我这里吗？如果上天要灭绝这些文化，我就不会掌握它了；如果上天不想灭绝这些文化，匡地的人又能把我怎么样？”</div>
<div class="yuanwen list fayin" onclick="run(this)">太宰<sup yin="">①</sup>问于子贡曰：“夫子圣者与<sup yin="欤"></sup>？何其多能也？” 子贡曰：“固天纵之将圣，又多能也。” 子闻之，曰：“太宰知我乎？吾少也贱，故多能鄙事。君子多乎哉？不多也。”</div>
<div class="shuoming list">❶太宰：官名。这里的太宰具体指谁，已无法考证。</div>
<div class="shiyi list fayin" onclick="run(this)">太宰问子贡说：“孔先生是圣人吗？为什么他这样多才多艺呢？” 子贡说：“本来嘛老天要让他做圣人，又让他会那么多本事。” 孔子听到后，说：“太宰了解我吗？我小时候贫贱，因此（学会了许多低贱的技能）会做好多小活计。（地位高的）君子技艺会多吗？不会多的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">牢曰<sup yin="">①</sup>：“子云：‘吾不试<sup yin="">②</sup>，故艺。’”</div>
<div class="shuoming list">❶牢：人名，具体情况已无可考证。有人认为是孔子的弟子。❷试：用世，做官。</div>
<div class="shiyi list fayin" onclick="run(this)">牢说：“孔子曾说：‘我因为没有被国家重用，所以学会了一些技艺。’”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“吾有知乎哉？无知也。有鄙夫<sup yin="">①</sup>问于我，空空如也。我叩其两端而竭焉<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶鄙夫：贬称，患得患失的小人，浅薄之人。❷叩其两端：仔细思考、揣摩、推究它涉及到的所有对立面的关系。竭：完全，穷尽。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“我什么都懂吗？不是这样的（我只不过掌握了获得新知的方法罢了），例如，即使一个见识不多的人来问我一个问题，我也可能一无所知的，但我会对那问题仔细加以分析，务求把握到与之有关的所有方面（这样是一定会终于弄明白的）。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“凤鸟不至<sup yin="">①</sup>，河不出图<sup yin="">②</sup>，吾已矣夫！”</div>
<div class="shuoming list">❶凤鸟不至：传说舜和周文王时，都曾有凤凰飞来，所以古人认为凤凰出现就显示天下太平。❷河不出图：传说伏羲氏时，黄河中有一匹龙马，背上呈八卦一般的图纹，伏羲就以这图纹为蓝本创制了八卦。黄河龙马负图出现，古人认为意味着有圣人受命来治理天下。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“凤凰不来人间了，黄河不出八卦图了，我这辈子就这么过去了！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子见齐<sup yin="zī"></sup>衰<sup yin="cuī"></sup>者<sup yin="">①</sup>、冕衣裳者与瞽者，见之，虽少，必作<sup yin="">②</sup>；过之，必趋。</div>
<div class="shuoming list">❶齐衰（zī cuī）：丧服的一种，用麻布做成，在五服（五个等级的丧服）中居第二等，仅次于斩衰。这里泛指丧假。❷作：站起来。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子看见穿丧服的，穿戴礼帽礼服的，以及眼睛失明的人，只要看见，即便他们年纪轻，孔子也一定站起来；从他们身边经过时，一定快步走。</div>
<div class="yuanwen list fayin" onclick="run(this)">颜渊喟然叹曰<sup yin="">①</sup>：“仰之弥高<sup yin="">②</sup>，钻之弥坚；瞻之在前，忽焉在后。夫子循循然善诱人<sup yin="">③</sup>，博我以文，约我以礼，欲罢不能。既竭吾才，如有所立卓尔<sup yin="">④</sup>，虽欲从之，末由也已<sup yin="">⑤</sup>。”</div>
<div class="shuoming list">❶喟：叹气，叹息。❷弥：更加，越发。❸循循然：一步一步有次序地。❹卓尔：高大直立得样子。 ❺末由：指不知从什么地方，不知怎么办，没有办法去达到。末，没有，无。由，途经。</div>
<div class="shiyi list fayin" onclick="run(this)">颜渊深深慨叹说：“（老师的思想学问）越仰望它，越觉得它高，越钻研它，越觉得它深；看它好像在眼前，忽然又像在后面。老师循循善诱地教诲我们，用文化典籍丰富我的知识，用礼约束我的言行，使得我停止学习也不可能。我竭尽了自己的才智，却仍然像有一物矗立在前面。虽然想登上去，却苦于无路可上了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子疾病，子路使门人为臣<sup yin="">①</sup>。病间，曰：“久矣哉，由之行诈也！无臣而为有臣。吾谁欺？欺天乎？且予与其死于臣之手也，无宁死于二三子之手乎！且予纵不得大葬，予死于道路乎？”</div>
<div class="shuoming list">❶子路使门人为臣：古代大夫的丧事，由家臣治办。孔子当时已不是大夫，没有家臣。子路想用大夫之礼为孔子治丧，所以让别的弟子充当家臣。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子病得很重，子路叫别的弟子充当家臣（准备为老师料理丧事）。后来，孔子病好转了，（得知这件事后）说：“仲由搞欺骗已有很长时间了。我没有家臣却装作有家臣，我骗谁呢？骗天吗？再说，我与其由家臣办丧事，不如由你们弟子来办呢！我即使不能死后得到（像大夫那样的）隆重安葬，难道就会死在路上（没人来葬我）吗？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子贡曰：“有美玉于斯，韫椟而藏诸<sup yin="">①</sup>？求善贾<sup yin="gǔ">②</sup>而沽诸？”子曰：“沽之哉！沽之哉！我待贾（gǔ）者也。”</div>
<div class="shuoming list">❶韫椟（yùn dú）：韫，收藏。椟，柜子。以后“韫椟”表示怀才未用。❷贾（gǔ）：商人。沽：卖。</div>
<div class="shiyi list fayin" onclick="run(this)">子贡说：“有块美玉在这里，是装进柜子藏起来呢？还是找个识货的卖掉？” 孔子说：“卖掉吧！卖掉吧！我正在等待识货的人呢！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子欲居九夷<sup yin="">①</sup>。或曰：“陋，如之何？” 子曰：“君子居之，何陋之有？”</div>
<div class="shuoming list">❶久夷：古代对居住在我国东部地区民族的总称。“九”表示多；“夷”是对东部民族带有轻蔑意味的称呼。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子想到九夷去住。有人担心了：“九夷蛮荒无礼，怎么住？” 孔子说：“君子居住到那里，还有什么落后呢？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“吾自卫反<sup yin="返"></sup>鲁<sup yin="">①</sup>，然后乐正，《雅》《颂》各得其所<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶吾自卫反鲁：公元前484年，孔子离开卫国返回鲁国，结束了十四年周游列国的生活。❷《雅》《颂》：是《诗经》中两类诗的名称，又是乐曲的分类名。不同类的诗要配上不同类的乐曲。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“我从卫国返回鲁国后，对乐曲进行了整理订正，使《风》、《雅》、《颂》各归其类。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“出则事公卿，入则事父兄，丧事不敢不勉，不为酒困，何有于我哉<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶何有于我哉：一说，此句意为：我做到了哪些呢？</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“在外侍奉好公卿，在家侍奉好兄长，丧事不敢不尽心，不因为喝酒而伤身误事，这些对我们来说，有什么难做的呢？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子在川上曰：“逝者如斯夫<sup yin="">①</sup>，不舍昼夜<sup yin="">②</sup>！”</div>
<div class="shuoming list">❶逝者：指逝去的岁月、时光。❷舍：止，停留。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子站在河边感叹道：“消逝的时光正像这河水一样啊！日夜不停地流去。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“吾未见好德如好色者也。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“我没有见到过爱好仁德就像喜爱美色那样的人。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“譬如为山，未成一篑<sup yin="">①</sup>，止，吾止也。譬如平地<sup yin="">②</sup>，虽覆一篑，进，吾往也。”</div>
<div class="shuoming list">❶篑（kuì）:装土的筐子。❷平地：这里应视作使动结构，“使地平”的意思。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“譬如用土堆山，还差一筐就可以堆成了，这时候停下来，是自己停的。又譬如用土平地，虽然只倒了一筐，但往前倒土，是自己在前进。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“语<sup yin="yù">yù</sup>之而不惰者，其<sup yin="">①</sup>回也与<sup yin="欤"></sup>！”</div>
<div class="shuoming list">❶其：表示揣测、反诘。莫非，难道，也许。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“听我说话而始终听不懈怠的，大概只有颜回吧！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子谓颜渊，曰：“惜乎！吾见其进也，未见其止也！”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说起颜回，叹道：“可惜（早死）了!我眼看他天天向上，从未见他停滞不前啊。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“苗而不秀者有矣夫！秀而不实者有矣夫<sup yin="">①</sup>！”</div>
<div class="shuoming list">❶据《论语注释》，此章是孔子惋惜颜渊早逝而作。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“庄稼光出苗生长而不开花结穗，有这种情况吧！光开花而不结果实，有这种情况的吧！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“后生可畏，焉知来者之不如今也？四十、五十而无闻焉，斯亦不足畏也已。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“年轻人值得敬畏，哪能预知他们的将来赶不上现在的人呢？（不过）如果到了四十岁、五十岁还没有什么声望，也就不值得敬畏了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“法语之言，能无从乎？改之为贵。巽与之言<sup yin="">①</sup>，能无说<sup yin="悦"></sup>乎？绎之为贵<sup yin="">②</sup>。说而不绎，从而不改，吾末如之何也已矣。”</div>
<div class="shuoming list">❶巽（xun）：同“逊”，谦逊，恭顺。❷绎：分析鉴别。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“符合正道的话，能不听从吗？（但只有按它）改正错误才是可贵的。恭敬好听的话，听了能不高兴吗？但只有分析鉴别一下（真假是非）才是可贵的。只是高兴而不加分析，只是接受而不改正，（这种人）我是拿他没办法的了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“主忠信，毋友不如己者，过则勿惮改<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶见《学而》篇第八章。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“做人重要的是诚实、守信用。不结交德行不如自己的人。有了过错就不怕改正。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“三军<sup yin="">①</sup>可夺帅也，匹夫<sup yin="">②</sup>不可夺志也。”</div>
<div class="shuoming list">❶三军：古制，一万两千五百人为一军。周期，一个大诸侯国可拥有三军。❷匹夫：普通的人，男子汉。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“一国的军队，可以俘获它的主帅；一个普通人，却不能强迫他改变志向。”</div>
<div class="yuanwen list fayin" onclick="run(this)">9.27子曰：“衣<sup yin="yì">yì</sup>敝缊<sup yin="yùn">yùn</sup>袍，与衣<sup yin="yì">yì</sup>狐貉者立，而不耻者，其由也与。‘不忮<sup yin="zhì">①</sup>不求，何用不臧？’”子路终身诵之。子曰：“是道也，何足以臧<sup yin="">②</sup>？”</div>
<div class="shuoming list">❶忮（zhì）：嫉妒。❷臧：善，好。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“穿着破麻布袍子，和穿着狐皮大衣的站在一起，一点也不自卑的，恐怕只有子路了。（《诗经》上说）‘不嫉妒不贪求，为什么不好？’” 子路听后，便一直念着这两句诗。孔子提醒说：“仅做到这样，哪值得称赞？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“岁寒，然后知松柏之后凋<sup yin="">①</sup>也。”</div>
<div class="shuoming list">❶凋：凋谢，草木花叶脱落。松柏树四季常青，经冬不凋。孔子以此为喻，有“烈火见真金”“路遥知马力”“国乱识忠臣”“士穷显节义”的含意。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“严寒的冬季，才知道松柏是最后凋零的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“知<sup yin="智">①</sup>者不惑，仁者不忧，勇者不惧。”</div>
<div class="shuoming list">❶知：同“智”。智、仁、勇是孔子所提倡的三种传统美德。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“聪明的人不疑惑，仁德的人没有忧愁，勇敢的人不畏惧。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“可与共学，未可与适道；可与适道<sup yin="">①</sup>，未可与立；可与立，未可与权<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶适：往。这里含有达到、学到的意思。❷权：本义是秤锤。引申为权衡，随宜而变。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“能在一起学习的人，未必可以一起追求真理；能一起追求真理的，未必能一起确立‘道’的原则；能一起确立‘道’的原则的，未必都能随机应变地运用它。”</div>
<div class="yuanwen list fayin" onclick="run(this)">“唐棣之华<sup yin="花">①</sup>，偏其反而。岂不尔思？室是远而。” 子曰：“未之思也。夫何远之有？”</div>
<div class="shuoming list">❶唐棣：一种植物，属蔷薇科，落叶灌木。数花朵一簇，象征兄弟团结。华：同“花”。</div>
<div class="shiyi list fayin" onclick="run(this)">（古代有首诗这样写道）“唐棣的花朵啊，翩翩地摇摆。难道我不思念你吗？你居住得太遥远了。”孔子说：“这还是没有真正的思念啊，（如果真的思念）有什么遥远呢？”</div>

`