$.jsname__08 =`
<h3>泰伯篇</h3>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“泰伯<sup yin="">①</sup>，其可谓至德也已矣。三以天下让，民无得而称焉。”</div>
<div class="shuoming list">❶泰伯：又作“太伯”，周朝祖先古公亶（dǎn）父的长子。古公亶父欲立幼子季历，泰伯便主动偕弟仲雍出走到江南，成为当地君长。泰伯死后，由仲雍继立，其后人建立吴国。而季历之孙姬发（周武王）伐纣灭商，建立了周朝。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“泰伯，可以说是德行极其高尚了。他多次坚持把君位让给弟弟，人民无法用语言来赞美他。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“恭而无礼则劳，慎而无礼则葸<sup yin="xǐ">①</sup>，勇而无礼则乱，直而无礼则绞<sup yin="">②</sup>。君子笃于亲，则民兴于仁；故旧不遗，则民不偷。”</div>
<div class="shuoming list">❶葸（xǐ）：过分拘谨，胆怯懦弱。❷绞：说话尖酸刻薄，出口伤人；太急切而无容忍。❸笃：诚实，厚待。❹偷：刻薄，冷漠无情。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“注重态度的恭敬庄重却不懂礼，就会徒劳无益；言行谨慎而不合礼制，就显得胆怯懦弱；遇事勇敢而不合礼制，就会犯上作乱；为人直率而不合礼制，就显得尖刻刺人。在上位的人对亲族感情深厚，百姓就会重视仁德；在上位的人不遗弃熟人朋友，百姓就不会冷漠无情。”</div>
<div class="yuanwen list fayin" onclick="run(this)">曾子有疾，召门弟子曰：“启<sup yin="">①</sup>予足！启予手！《诗》云：‘战战兢兢，如临深渊，如履薄冰<sup yin="">②</sup>。’而今而后，吾知免夫。小子<sup yin="">③</sup>！”</div>
<div class="shuoming list">❶启：开。这里指掀开被子看一看。❷战战兢兢，如临深渊，如履薄冰：曾参借用这句话，表明自己一生处处小心谨慎，避免身体受到损伤，算是尽了孝道。据《孝经》记载，孔子曾对曾参说：“身体发肤受之父母，不敢毁伤，孝之始也。”❸小子：称弟子们。这里说完一番话之后再呼弟子们，表示反复叮咛。</div>
<div class="shiyi list fayin" onclick="run(this)">曾子得重病后，召集他门下弟子说：“看看我的脚！看看我的手（有无毁伤之处）！《诗经》上说：‘胆战心惊的，就像面临着深深的水潭，就像脚踩着薄薄的冰层。’从今往后，我这身子是不会受伤了！弟子们！”</div>
<div class="yuanwen list fayin" onclick="run(this)">曾子有疾，孟敬子问之<sup yin="">①</sup>。曾子言曰：“鸟之将死，其鸣也哀；人之将死，其言也善。君子所贵乎道者三：动容貌，斯远暴慢矣；正颜色，斯近信矣；出辞气，斯远鄙倍<sup yin="背"></sup>矣。笾豆之事<sup yin="">②</sup>，则有司存。”</div>
<div class="shuoming list">❶孟敬子：鲁国大夫仲孙捷。❷笾（biān）豆：笾，盛食品用的一种竹器；豆，一种盛食物的器皿。笾、豆常用于祭祀和典礼，“笾豆之事”即指祭祀和礼仪方面的事。</div>
<div class="shiyi list fayin" onclick="run(this)">曾子得了重病，孟敬子去看望。曾子说：“鸟临死的时候，那叫声都哀痛；人临终的时候，那话语都真切。在上位的人处世待人，要注重三个方面：容貌要严肃谦和，这样可以免遭粗暴傲慢；脸色要端庄，这样就能接近诚信；言辞语气要恰当，这样就能避免鄙陋粗野。至于礼仪中的具体事情，则由主管官吏去负责。”</div>
<div class="yuanwen list fayin" onclick="run(this)">曾子曰：“以能问于不能，以多问于寡；有若无，实若虚，犯而不校<sup yin="">①</sup>，昔者吾友尝从事于斯矣。”</div>
<div class="shuoming list">❶校：计较。</div>
<div class="shiyi list fayin" onclick="run(this)">曾子说：“有才能的人却向没有才能的人请教，学识丰富却向知识浅薄的人请教；有知识却像没知识一样，知识充实却像知识空虚一样；被人冒犯而不计较。从前我的一位朋友曾经这样做过。”</div>
<div class="yuanwen list fayin" onclick="run(this)">曾子曰：“可以托六尺之孤<sup yin="">①</sup>，可以寄百里<sup yin="">②</sup>之命，临大节而不可夺也。君子人与<sup yin="欤"></sup>？君子人也！”</div>
<div class="shuoming list">❶六尺之孤：孩子死去父亲，叫“孤”。六尺之孤，指尚未成年而登基接位的年幼君主。❷百里：指代一个诸侯国。</div>
<div class="shiyi list fayin" onclick="run(this)">曾子说：“可以把幼小的君主托付给他，可以把国家的命运托付给他，而临安危存亡的关头而不能动摇屈服。这种人可以称得上是君子吗？是君子啊！”</div>
<div class="yuanwen list fayin" onclick="run(this)">曾子曰：“士不可以不弘毅<sup yin="">①</sup>，任重而道远。仁以为己任<sup yin="">②</sup>，不亦重乎？死而后已，不亦远乎？”</div>
<div class="shuoming list">❶弘毅：刚强，勇敢。弘，广大，开阔，宽广。毅，坚强，果敢，刚毅。❷仁以为已任：“以仁为已任”的倒装句。</div>
<div class="shiyi list fayin" onclick="run(this)">曾子说：“读书人不可以不心胸宽广，意志坚定，（因为他）责任很重，路途遥远。把实现仁德作为自己的责任，这担子还不重吗？为仁奋斗到死才罢休，这路途还不遥远吗？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“兴于《诗》<sup yin="">①</sup>，立于礼<sup yin="">②</sup>，成于乐<sup yin="">③</sup>。”</div>
<div class="shuoming list">❶兴：兴起，勃发，激励；受到《诗经》的感染，而热爱真善美，憎恶假恶丑。❷立：立足于社会，树立道德。❸成：完成，达到。这里指以音乐来陶冶性情，涵养高尚的人格，完成学业，最终达到全社会“礼乐之治”的最高境界。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“从学《诗经》而开始修养，由习礼而得以自立，坚定操守，通过音乐来陶冶性情，成就道德。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“民可使由<sup yin="">①</sup>之，不可使知之。”</div>
<div class="shuoming list">❶由：从，顺从，听从，经由什么道路。孔子认为下层百姓的才智能力、认知水平、觉悟程度各不一样，当政者在实行政策法令时，只能要求他们遵照着去做，而不可以使人人都知道这样做的道理。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“对于百姓，可以使他们（不知不觉）按道理去做，不必让他们听多少道德说教。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“好勇疾贫<sup yin="">①</sup>，乱也；人而不仁，疾之已甚，乱也。”</div>
<div class="shuoming list">❶疾：讨厌，厌恶。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“喜欢逞勇斗很又讨厌自己贫穷的人，就会作乱。一个不仁的人，如果被别人过分厌恶，（导致他自暴自弃）也会作乱。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“如有周公之才之美，使骄且吝<sup yin="">①</sup>，其余不足观也已。”</div>
<div class="shuoming list">❶吝：吝啬，小气，过分爱惜。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“假如有了周公那样好才华，但只要骄傲吝啬，那他其余方面就不值得一提了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“三年学，不至于谷<sup yin="">①</sup>，不易得也。”</div>
<div class="shuoming list">❶谷：古代以谷米为俸禄，此用来指代官吏。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“求学三年，还没有做官的想法，这种人难得呀。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“笃信好学，守死善道。危邦不入，乱邦不居。天下有道则见<sup yin="现">①</sup>，无道则隐。邦有道，贫且贱焉，耻也；邦无道，富且贵焉，耻也。”</div>
<div class="shuoming list">❶见：同“现”，表现，出现，出来。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“（对于道）要坚定信念，努力学习，坚守善道，至死而不变。不进入政局危急的国家，不滞留在政治混乱的国家。天下有道，就出来做官，无道就隐退。国家有道时，自己贫贱不能上进，是可耻的；国家无道时，而自己富贵，也是可耻的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“不在其位，不谋其政<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶谋：参与，考虑，谋划。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“不在那个职位上，就不干预那方面的政事。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“师挚之始<sup yin="">①</sup>，《关雎》之乱<sup yin="">②</sup>，洋洋乎盈耳哉！”</div>
<div class="shuoming list">❶师挚之始：师挚，即太师挚。太师是乐师，这里指鲁国的一位乐师，名挚。“始”是乐曲的开端，古代奏乐，序曲通常由太师演奏。❷《关雎》之乱：《诗经·国风》的第一篇。古代《诗经》中的诗篇是可以用来配乐演唱的。“乱”是乐曲的结束部分。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“从太师挚开始演奏，直到最后演奏《关雎》之曲时，美妙动听的音乐充满了我的耳朵啊！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“狂而不直，侗而不愿<sup yin="">①</sup>，悾悾<sup yin="">②</sup>而不信，吾不知之矣。”</div>
<div class="shuoming list">❶侗（tóng）：幼稚，无知。愿：谨慎老实。❷悾悾（kōng）：诚恳的样子。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“狂妄而不直率，无知而不老实，样子诚恳而不讲信用，我真不知道这是怎样一种人了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“学如不及，犹恐失之。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“学习唯恐学不到，学到了又唯恐忘掉。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“巍巍乎，舜、禹之有天下也<sup yin="">①</sup>，而不与<sup yin="yù">yù</sup>焉！”</div>
<div class="shuoming list">❶禹：传说中古代部落联盟的首领，夏朝开国君主。做舜臣的时候，用疏导方式治水，取得成功，成为古代著名的治水英雄。代舜为首领后，传位其子，建立了中国历史上第一个私有制王朝--夏朝。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“多么崇高啊！舜和禹拥有天下，却一点儿也不谋求私利啊。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“大哉尧之为君也！巍巍乎！唯天为大，唯尧则之。荡荡乎，民无能名焉<sup yin="">①</sup>。巍巍乎其有成功也。焕乎其有文章<sup yin="">②</sup>！”</div>
<div class="shuoming list">❶名：用语言去形容，赞美。❷焕：光辉，光明。文章：指礼乐典章制度。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“尧作为君主，真伟大啊！真崇高啊，只有天最伟大，只有尧可以与天相比。他的恩德多么广博啊，人民无法用语言来称赞他。他的功绩多么崇高啊。他的礼乐典章制度多么光彩灿烂。”</div>
<div class="yuanwen list fayin" onclick="run(this)">舜有臣五人而天下治。武王曰：“予有乱臣<sup yin="">①</sup>十人。”孔子曰：“才难，不其然乎？ 唐虞之际<sup yin="">②</sup>，于斯为盛。有妇人焉，九人而已。三分天下有其二，以服事殷。周之德，其可谓至德也已矣。”</div>
<div class="shuoming list">❶乱臣：“乱，治也”。训估学上称之为“反训”。“乱臣”就是治国之臣。❷唐虞之际：唐，帝尧的部落先居于陶，后徙于唐，称陶唐氏。唐或陶唐皆可指代帝舜。虞，即虞氏，帝舜的部落名，虞可以指代帝舜。唐虞之际，即尧舜之时。</div>
<div class="shiyi list fayin" onclick="run(this)">舜有五位贤臣而使天下太平。武王曾说：“我有十位治理天下的贤臣。”孔子说：“人才难得，难道不是这样吗？唐尧、虞舜那个时代（之后），武王时代人才最茂盛，其中有一个还是女的，男的九个而已。周文王时候三分天下，文王占了两分，仍然向殷商称臣。周文王的德行，真可以说至高无上了！”</div>
<div class="yuanwen list fayin" onclick="run(this)">8.21子曰：“禹，吾无间然矣。菲饮食而致孝乎鬼神，恶衣服而致美乎黼冕<sup yin="">①</sup>，卑宫室而尽力乎沟洫<sup yin="">②</sup>。禹，吾无间然矣。”</div>
<div class="shuoming list">❶黼（fǔ）冕：古代贵族祭祀时的礼服、礼冠。❷沟洫（xù）：田间水道，即沟渠。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“对于禹，我是没有可批评的了。他饮食菲薄，却（用丰盛的祭品）祭尊鬼神；他衣着粗劣，而祭服做得很精美；他居室简陋，却尽全力兴办水利。对于禹，我是挑不出他的缺点了。”</div>

`