$.jsname__17 =`
<h3>阳货篇</h3>
<div class="yuanwen list fayin" onclick="run(this)">阳货欲见孔子<sup yin="">①</sup>，孔子不见，归<sup yin="馈">kuì</sup>孔子豚<sup yin="">②</sup>。孔子时<sup yin="伺">③</sup>其亡也，而往拜之。遇诸途。谓孔子曰：“来！予与尔言。”曰：“怀其宝而迷其邦，可谓仁乎？”曰：“不可。”“好从事而亟失时，可谓知<sup yin="智">智</sup>乎？”曰：“不可！”“日月逝矣，岁不我与<sup yin="yǔ">yǔ</sup>。”孔子曰：“诺，吾将仕矣。”</div>
<div class="shuoming list">❶阳货：又叫阳虎，鲁国大夫季氏的家臣，曾一度把持季氏家的大权和鲁国政权，后因权力斗争失利而逃往齐国、晋国。孔子站在正统立场上，对阳货的所作所为一向持反对态度，称他是“陪臣执国命”，所以不愿见他。❷归（kuì）馈孔子豚：归，同“馈”，赠送。当时礼节规定，大夫赏赐东西给士，如果士大夫能在家当面受赐，过后就要亲自上大夫家拜谢。❸时：同“伺”，窥伺。</div>
<div class="shiyi list fayin" onclick="run(this)">阳货希望孔子去拜访他，孔子不去，阳货就送了一只蒸熟小猪给孔子（使得孔子不得不去他家拜谢）。孔子趁阳货不在家的时候前往拜谢，不巧在路上相遇了。阳货对孔子说：“来！我有话对你说。”就开口问道：“怀藏着仁德才智（不出来做官），却听任他的国家迷途失道，这能算仁吗？”孔子说：“不可以！”（阳货）又问：“一心想做大事却一再错失良机，可以叫做聪明吗？”孔子说：“不能”（阳货）又说：“日子一天天过去，年岁不等人啊！”孔子说：“好的，我准备去做官了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“性相近也<sup yin="">①</sup>，习相远也<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶性：人的本性，性情，先天的智力、气质。❷习相远：指由于社会影响，所受教育不同，习俗、习气的沾染有别，人的后天的行为习惯会有很大差异。这里孔子是勉励人为学，通过学习提高自己的修养。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“人的天性本来是相近的，由于习俗、教育不同便逐渐差得远了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“唯上知<sup yin="智">①</sup>与下愚不移。”</div>
<div class="shuoming list">❶知：同“智”。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“只有上等聪明的人和下等愚笨的人是不可以改变性情的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子之武城<sup yin="">①</sup>，闻弦歌之声。夫子莞尔而笑，曰：“割鸡焉用牛刀<sup yin="">②</sup>？”子游对曰<sup yin="">③</sup>：“昔者偃也闻诸夫子曰：‘君子学道则爱人，小人学道则易使也。’”子曰：“二三子！偃之言是也。前言戏之耳。”</div>
<div class="shuoming list">❶武城：鲁国的一个小城邑，子游当时任武城长官。❷割鸡焉用牛刀：这是个比喻，意思是治理一个小城邑，哪用得着施行礼乐教化。❸子游：孔子弟子言偃的字。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子到武城，听到传来弹琴唱歌的声音，就莞尔一笑，说：“杀鸡哪里用得着牛刀？”子游回答说：“以前我听老师说过：‘君子学了（礼乐的）道理，就会有仁爱之心，老百姓学了（礼乐的）道理，就容易听使唤。’”孔子说：“弟子们！言偃的话是对的啊。我前面那句话是同他开玩笑的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">公山弗扰以费<sup yin="bì">bì</sup>鄪畔<sup yin="叛">①</sup>，召，子欲往。子路不说<sup yin="悦">悦</sup>，曰：“末之也已，何必公山氏之之也？”子曰：“夫召我者，而岂徒哉？如有用我者，吾其为东周乎！”</div>
<div class="shuoming list">❶公山弗扰：又叫公山不狃（niǔ），季氏家臣，鲁定公九年（公元前501年）在费邑反叛季氏。费：季氏封地。畔：同“叛”，谋反。</div>
<div class="shiyi list fayin" onclick="run(this)">公山弗扰占据费邑，反叛季氏，请孔子去帮忙。孔子打算去，子路不高兴了，说：“没地方去也就罢了，何必非要去公山氏那里呢？”孔子说：“那个叫我去的人，难道会白白召我去？如果有人任用我，我或许还能在东方复兴周朝的礼乐制度呢！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子张问仁于孔子。孔子曰：“能行五者于天下，为仁矣。”请问之”。曰：“恭、宽、信、敏、惠。恭则不侮，宽则得众，信则人任焉，敏则有功，惠则足以使人。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">子张问孔子什么是仁。孔子说：“能在天下推行五种美德，就是仁。”子张问哪五种，孔子说：“就是恭敬，宽厚，诚信，勤敏，慈惠。恭敬就不会招来侮辱，宽厚就能得到众人拥护，诚信就会得到别人任用，勤敏就会有成绩，慈惠就足以使唤人。”</div>
<div class="yuanwen list fayin" onclick="run(this)">佛<sup yin="bì">bì</sup>肸<sup yin="xī">①</sup>召，子欲往。子路曰：“昔者由也闻诸夫子曰：‘亲于其身为不善者，君子不入也。’佛<sup yin="bì">bì</sup>肸<sup yin="xī"></sup>以中牟畔<sup yin="叛">叛</sup>，子之往也，如之何？”子曰：“然，有是言也。不曰坚乎，磨而不磷？不曰白乎，涅而不缁？吾岂匏<sup yin="páo">páo</sup>瓜也哉？焉能系而不食？”</div>
<div class="shuoming list">❶佛肸（bìxī）：晋国大夫范氏家臣，中牟（范式封邑）的长官。公元前490年，晋国大夫赵简子借晋候名义攻打范式，围中牟，佛肸抗拒赵简子。佛肸召孔子即在此时。下文所说的“畔”，是指反叛晋候。</div>
<div class="shiyi list fayin" onclick="run(this)">佛肸召请孔子，孔子想去。子路说：“从前我听老师说：‘动手干坏事的人那里，君子不去入伙的。’佛肸在中牟谋反，老师到那里去，干什么呢？”孔子说：“对啊，是有这话。可是，我不也说了坚硬的东西磨而不薄，洁白的东西染而不黑吗？我难道是个苦味的葫芦吗？怎么能只悬挂在那里不被人食用呢？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“由也，女<sup yin="汝">汝rǔ</sup>闻六言六蔽矣乎<sup yin="">①</sup>？”对曰：“未也。”“居，吾语<sup yin="yù">yù</sup>女<sup yin="汝">②</sup>。好<sup yin="hào"></sup>仁不好学，其蔽也愚；好<sup yin="hào"></sup>知<sup yin="智">③</sup>不好学，其蔽也荡；好<sup yin="hào"></sup>信不好学，其蔽也贼；好<sup yin="hào"></sup>直不好学，其蔽也绞；好<sup yin="hào"></sup>勇不好学，其蔽也乱；好<sup yin="hào"></sup>刚不好学，其蔽也狂。”</div>
<div class="shuoming list">❶蔽：同“弊”，弊端，害处。❷居：坐。❸知：同“智”。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“仲由啊，你听说过六种品德、六种弊病吗？”子路答道：“没有。”（孔子说）“坐下，我告诉你。爱好仁德而不爱学习，它的弊病是使人愚笨；爱好聪智而不爱学习，它的弊病是使人轻浮无根底；爱好诚实而不爱学习，它的弊病是伤害大义；爱好直率而不爱学习，它的弊病是语急伤人；爱好勇敢而不爱学习，它的弊病是犯法作乱；爱好刚强而不爱学习，它的弊病是狂妄自大。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“小子何莫学夫《诗》？《诗》，可以兴，可以观<sup yin="">①</sup>，可以群<sup yin="">②</sup>，可以怨<sup yin="">③</sup>。迩之事父<sup yin="">④</sup>，远之事君。多识于鸟兽草木之名。”</div>
<div class="shuoming list">❶观：本义是观察，观看。这是指提高人的观察能力。❷群：使合群。诗离不开人写，多读诗就可以更深切地了解人，懂得如何与人相处、相交，培养人的合群的本领。❸怨：怨恨。《诗经》中有不少怨刺诗，表达对现实的愤懑，抒发人们心中的不平，讽刺不合理的社会现象。读了以后，可以学到用讽刺的方法，用正当的宣泄，来表达心中的怨恨不平的感情。❹迩：近。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“年轻人何不学点《诗》？学点《诗》，可以激发联想，可以锻炼眼力，可以扩大交际，可以抒发不满。近一点，可以更好的孝敬父母；远一点，可以更好的侍奉君王。还可以多多记住鸟兽草木的名称。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子谓伯鱼曰：“女<sup yin="汝">汝rǔ</sup>为《周南》、《召南》矣乎<sup yin="">①</sup>？人而不为《周南》、《召南》，其犹正墙面而立也与<sup yin="欤">欤</sup>！”</div>
<div class="shuoming list">❶《周南》《召南》：《诗经·国风》中前两部分的名称。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子对伯鱼说：“你学《周南》、《召南》了吗？一个人如果不学《周南》《召南》，那他就会像面对着墙壁站立一样（无法行走）了吧！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“礼云礼云，玉帛云乎哉<sup yin="">①</sup>？乐云乐云，钟鼓云乎哉？”</div>
<div class="shuoming list">❶玉帛：指举行礼仪时的玉器、丝织品。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“礼节礼节呀，难道说的只是玉、帛之类的礼物迎来送往吗？音乐音乐呀，难道说的只是钟鼓之类乐器吗？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“色厉而内荏<sup yin="">①</sup>，譬诸小人，其犹穿窬<sup yin="">②</sup>之盗也与<sup yin="欤">欤</sup>！”</div>
<div class="shuoming list">❶荏（rěn）：软弱，懦弱，虚弱。❷窬（yú）：洞，窟窿。从墙上爬过去也叫“窬”。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“样子很凶，心里很怕，如果拿小人打比方，那就像个在墙上打洞的小偷吧。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“乡愿<sup yin="">①</sup>，德之贼也。”</div>
<div class="shuoming list">❶乡愿：乡里的人都称道的老实人实际上是没有原则，不讲操守，讨好一切人，与世俗同流合污的人。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“那种谁也不得罪的老好人，是败坏道德的小人。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“道听而涂<sup yin="途">①</sup>说，德之弃也。”</div>
<div class="shuoming list">❶涂：同“途”。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“把半路上听来的话到处乱传，有修养的人不会这样做。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“鄙夫可与事君也与<sup yin="欤">欤</sup>哉<sup yin="">①</sup>？其未得之也，患得之<sup yin="">②</sup>。既得之，患失之。苟患失之，无所不至矣。”</div>
<div class="shuoming list">❶鄙夫：鄙陋、庸俗、道德品质恶劣的人。❷患得之：实际上是“患不能得之”的意思。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“品德低下恶劣的人，难道可以同他一起侍奉国君吗？他没有得到官职时，担心得不到；已经得到了，又惟恐失掉。如果惟恐失掉，那就什么手段都会施展出来了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">17.16子曰：“古者民有三疾，今也或是之亡无也。古之狂也肆，今之狂也荡；古之矜也廉，今之矜也忿戾；古之愚也直，今之愚也诈而已矣。”</div>
<div class="shuoming list">❶亡：同“无”。❷廉：本义是器物的棱角。这里引申为不可触犯，碰不得，惹不得。❸忿戾：凶恶好争，蛮横无理。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“古代人有三种毛病，现在的人或许是没有这些毛病了。古代的狂人心高志大，不拘小节，现在的狂人却是一味放荡，肆无忌惮；古代自尊自大的人，棱角太露，难以接近，现在自尊自大的人，却是乖戾多怒，与人相争；古代的愚人天真直率，现在的愚人却是惯于欺诈。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“巧言令色，鲜矣仁。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“满口说着动听的话，满脸装着讨好的面孔，（这种人）是很少有仁德的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“恶<sup yin="wù">wù</sup>紫之夺朱也<sup yin="">①</sup>，恶<sup yin="wù">wù</sup>郑声之乱雅乐也<sup yin="">②</sup>，恶<sup yin="wù">wù</sup>利口之覆邦家者。”</div>
<div class="shuoming list">❶恶紫：古人认为紫色是杂色，红色是正色。❷郑声：郑国的乐曲。雅乐：周朝京城正统的乐曲。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“（我）憎恶紫色抢夺红色的地位，憎恶郑国的音乐扰乱了典雅正统的京城音乐，憎恶巧言诡辩而是国家倾覆败亡的人。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“予欲无言。”子贡曰：“子如不言，则小子何述焉？”子曰：“天何言哉？四时行焉，百物生焉，天何言哉<sup yin="">①</sup>？”</div>
<div class="shuoming list">❶本章之旨，似在告诫学生不能只重言不重行。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“我不想说话了。”子贡说：“老师不说话，那么弟子传述什么呢？”孔子说：“天说了什么话呢？四季照常运行，万物依然生长，天说了什么话呢？”</div>
<div class="yuanwen list fayin" onclick="run(this)">孺悲欲见孔子<sup yin="">①</sup>，孔子辞以疾。将命者出户，取瑟而歌，使之闻之<sup yin="">②</sup>。</div>
<div class="shuoming list">❶孺悲：鲁国人，《礼记》上说，他曾从孔子学过士丧礼。❷使之闻之：孔子让孺悲知道自己是故意不见他，是想要他反省有没有不好的地方，努力去改正。</div>
<div class="shiyi list fayin" onclick="run(this)">孺悲来了，想见孔子，孔子托病不出来。传话的刚出门，孔子就取出瑟来边弹边唱，故意让孺悲听到（使他知道不见的原因：孺悲未经人引荐直接登门拜访，不符合士相见礼）。</div>
<div class="yuanwen list fayin" onclick="run(this)">宰我问：“三年之丧，期已久矣。君子三年不为礼，礼必坏；三年不为乐，乐必崩。旧谷既没，新谷既升，钻燧改火<sup yin="">①</sup>，期<sup yin="jī">jī</sup>可已矣。”子曰：“食夫稻，衣<sup yin="yì">yì</sup>夫锦<sup yin="">②</sup>，于女<sup yin="汝">汝rǔ</sup>安乎？”曰：“安！”“女<sup yin="汝">rǔ</sup>安，则为之!夫君子之居丧，食旨不甘，闻乐<sup yin="yuè"></sup>不乐，居处不安<sup yin="">③</sup>，故不为也。今女<sup yin="汝">rǔ</sup>安，则为之！”宰我出。子曰：“予之不仁也！子生三年，然后免于父母之怀。夫三年之丧，天下之通丧也。予也有三年之爱于其父母乎？”</div>
<div class="shuoming list">❶钻燧改火：古人钻木取火，所用之木四季不同。春用榆柳，夏用枣杏，季夏（夏季最后一个月）用桑石，秋用柞楢（zuòyóu），冬用槐檀。❷食夫稻，衣（yì）夫锦：古代水稻的种植面积很小，大米是很珍贵的粮食，居丧者更不宜食；锦，有文采之帛，居丧者不穿，只穿单色布衣。❸居处不安：古代孝子守丧，要住在临时用草料木料搭的棚子里，睡在草席上，用土块做枕头。</div>
<div class="shiyi list fayin" onclick="run(this)">宰我问：“父母去世，守丧三年，为期太长。君子三年不行礼，礼仪肯定废了；三年不奏乐，音乐肯定荒了。旧谷子割掉了，新谷子长起来，钻木取火的木头也轮用了一遍，一年期限也就够了。”孔子说：“（守丧<sup yin="sāng"></sup>不到三年就）吃香白米饭，穿绫罗绸缎，你心安吗？”宰我说：“心安。”孔子说：“你心安，你就去做吧！。君子守孝，吃着美味也没味道；听着音乐也不快乐；住在家里也觉得不安适，所以不那样做。现在你既然心安，你就去做吧！。”宰我就出去了。孔子说：“这个宰我，真不仁啊！孩子出生三年，才能离开父母怀抱。这三年的守孝，是天下通行的。宰我难道没有从他父母怀抱里得到过三年的爱抚吗？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“饱食终日，无所用心，难矣哉！不有博弈者乎<sup yin="">①</sup>？为之，犹贤乎已。”</div>
<div class="shuoming list">❶博弈：古代一种先掷骰（tóu）子再走棋的下棋方法。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“整天吃得饱饱的，什么也不想也不做，这种人难有作为啊！不是有下棋的游戏吗？玩玩这个，也总比设么心思都不用好。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子路曰：“君子尚勇乎？”子曰：“君子义以为上。君子有勇而无义为乱；小人有勇而无义为盗。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">子路问：“君子看重勇武吗？”孔子说：“君子最讲道义。君子光有勇武不讲道义，就会犯上作乱；小人光有勇武不讲道义，就会偷盗抢劫。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子贡曰：“君子亦有恶<sup yin="wù">wù</sup>乎？”子曰：“有恶<sup yin="wù"></sup>：恶<sup yin="wù"></sup>称人之恶者，恶<sup yin="wù"></sup>居下流而讪上者<sup yin="">①</sup>，恶<sup yin="wù"></sup>勇而无礼者，恶<sup yin="wù"></sup>果敢而窒者。”曰：“赐也，亦有恶<sup yin="wù"></sup>乎？”“恶<sup yin="wù"></sup>徼<sup yin="jiāo">jiāo</sup>以为知<sup yin="智">智</sup>者，恶<sup yin="wù"></sup>不孙<sup yin="逊">②</sup>以为勇者，恶<sup yin="wù"></sup>讦<sup yin="">③</sup>以为直者。”</div>
<div class="shuoming list">❶下流：旧本无“流”字，是衍文。❷孙：同“逊”，谦逊。❸讦（jié）：攻击别人的短处，揭发别人的隐私。</div>
<div class="shiyi list fayin" onclick="run(this)">子贡问：“君子也有憎恶的人吗？”孔子说：“有啊，君子憎恶到处说别人缺点的人，憎恶身居下位却诽谤上级的人，憎恶只讲勇敢不讲礼节的人，憎恶只求果敢而不通事理的人。”（孔子反问子贡）说：“赐啊，你也有憎恶的人吗？”（子贡答道）“我憎恶抄袭别人东西还自以为聪明的人，憎恶不谦逊还自以为勇敢的人，憎恶揭发别人隐私还自以为正直的人。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“唯女子与小人<sup yin="">①</sup>，为难养也，近之则不孙<sup yin="逊">逊</sup>，远之则怨。”</div>
<div class="shuoming list">❶女子：女，借用为“奴”；奴子，即奴婢（bì）。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“只有女子和小人是最难相处的。亲近他们，他们就会放肆无理；疏远他们，他们又会抱怨怀恨。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“年四十而见恶<sup yin="wù">wù</sup>焉<sup yin="">①</sup>，其终也已。”</div>
<div class="shuoming list">❶见恶（wù）：被别人所厌恶，所讨厌。见，助词，表示被动。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“到了四十岁还遭人厌恶，他这一辈子大约就完了。”</div>

`