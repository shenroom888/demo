$.jsname__16 =`
<h3>季氏篇</h3>
<div class="yuanwen list fayin" onclick="run(this)">季氏将伐颛臾<sup yin="">①</sup>。冉有、季路见于孔子曰：“季氏将有事于颛臾。”孔子曰：“求！无乃尔是过与<sup yin="欤"></sup>？夫颛臾，昔者先王以为东蒙主<sup yin="">②</sup>，且在邦域之中矣。是社稷之臣也，何以伐为？”冉有曰：“夫子欲之，吾二臣者皆不欲也。”孔子曰：“求，周任有言曰<sup yin="">③</sup>：‘陈力就列，不能者止。’危而不持，颠而不扶，则将焉用彼相<sup yin="xiàng">xiàng</sup>矣？且尔言过矣，虎兕<sup yin="sì">sì</sup>出于柙<sup yin="xiá">xiá</sup>，龟玉毁于椟<sup yin="dú">dú</sup>中，是谁之过与<sup yin="欤"></sup>？”</div>
<div class="shuoming list">❶颛臾（zhuān yú）：春秋时一个小国，鲁国的附庸国。相传为伏羲之后，故城在今山东省蒙阴县。❷东蒙：即东蒙山，一名蒙山，在鲁国东部。❸周任：古代一位有名的史官。❹相：扶瞎子走路的人。</div>
<div class="shiyi list fayin" onclick="run(this)">季氏准备攻打颛臾。冉有、子路两人拜见孔子，说：“季氏准备对颛臾使用武力。”

孔子说：“冉求！恐怕是你的过错吧？颛臾，以前先王曾经让他们负责东蒙山的祭祀，而且他们的领土在我们鲁国的疆域之内，是鲁国的臣属，为什么要攻打它呢？”

冉有说：“季孙要这么干，我和子路两人都是不赞同的。”

孔子说：“冉求！周任说过：‘能发挥作用的，就去任职，不能起作用就不要去。’危险关头不出力，要摔倒了不搀扶，何必要你们来辅佐呢？况且你的话是有问题的。老虎和犀牛从围栏里跑出来，龟壳和美玉在匣子里碎了，是谁的责任呢？”</div>
<div class="yuanwen list fayin" onclick="run(this)">冉有曰：“今夫颛臾，固而近于费<sup yin="bì">①</sup>鄪，今不取，后世必为子孙忧。”孔子曰：“求，君子疾夫舍曰‘欲之’而必为之辞。丘也闻有国有家者，不患寡而患不均，不患贫而患不安<sup yin="">②</sup>。盖均无贫，和无寡，安无倾。夫如是，故远人不服，则修文德以来之；既来之，则安之。今由与求也，相<sup yin="xiàng">xiàng</sup>夫子，远人不服，而不能来也；邦分崩离析，而不能守也；而谋动干戈于邦内。吾恐季孙之忧，不在颛臾，而在萧墙之内也<sup yin="">③</sup>。”</div>
<div class="shuoming list">❶费（bì）：鲁国的一个小城邑，季氏的封地，其地在今山东费县。❷ 不患寡而患不均，不患贫而患不安：上句“寡”字与下句“贫”字互倒。俞樾《古书疑义举��》卷六：“此本作‘不患贫而患不均，不患寡而患不安’”译文据之。❸当时季氏权势很大，把持鲁国政事，鲁哀公想削弱其势力。因颛庾靠近季氏封地，季氏便担心它被哀公利用而对自己不利，所以要攻打它。孔子这句话道破了季氏伐颛庾的真实意图。萧墙，面对宫门的小墙，如同后世的“照壁”，人臣至此便会肃然起敬，故称“萧墙”。萧：同“肃”。“萧墙之内”是暗指鲁君。</div>
<div class="shiyi list fayin" onclick="run(this)">冉有说：“如今的颛臾，城防坚固，紧靠季氏的费地。这次不打下来，对往后的子子孙孙一定是个麻烦。”孔子说：“冉求！君子讨厌那种嘴上说不要这样做却为这样做找借口的人。我听说，不管是诸侯国还是大夫家，都不担心财富少而担心分配不公，不担心人口少而担心不安定。大意是说财富分配公平了，就没有穷人；社会和谐了，就不会缺人；人心安定了，就不会翻天。这样做下去，如果远方的人还不是心悦诚服，就自己好好修身养性，把人家吸引过来。吸引过来后，就让他们安居乐业。而如今子路、冉求你们两个辅佐季先生，远方的人不归附，你们却引不来；国家四分五裂，你们也拢不住；心里盘算的竟然是在国内大动干戈。我担心季孙的麻烦不在颛臾，而在鲁君吧。”</div>
<div class="yuanwen list fayin" onclick="run(this)">孔子曰：“天下有道，则礼乐征伐自天子出；天下无道，则礼乐征伐自诸侯出。自诸侯出，盖十世希<sup yin="">①</sup>不失矣；自大夫出，五世希不失矣；陪臣执国命<sup yin="">②</sup>，三世希不失矣。天下有道，则政不在大夫；天下有道，则庶人不议。”</div>
<div class="shuoming list">❶希：同“稀”，少有。❷陪臣：卿、大夫的家臣。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“天下太平时，礼乐制度和征伐都由天子决定；天下混乱时，礼乐制度和征伐便由诸侯擅自做主。礼乐征伐由诸侯做主，很少有传到十代而政权不丧失的；如果由大夫做主，很少有传到五代而政权不丧失的；如果大夫的家臣操纵了国家政权，那就很少有传到三代而政权不丧失的。天下太平，政权不会落到大夫手中。天下太平，百姓不会议论朝政。”</div>
<div class="yuanwen list fayin" onclick="run(this)">孔子曰：“禄之去公室五世矣<sup yin="">①</sup>，政逮于大夫四世矣，故夫三桓之子孙微矣<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶五世、四世：鲁文公十八年（公元前608年），鲁国大夫襄仲杀文公太子恶而立宣公，从此国家政权落入大夫手中。这种局面，到孔子说这话的时候，已经历了宣公、成公、襄公、昭公、定公这五代；从大夫季氏最初把持政权，至此已经历了文子、武子、平子、桓子这四代。❷三桓：鲁国的孟孙氏，叔孙氏，季孙氏，都是鲁桓公的后代，时称“三桓”。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“鲁君失去政权已经五代了，政权落在大夫手中已经四代了，所以鲁桓公的三房子孙也到衰微的时候了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">孔子曰：“益者三友，损者三友。友直，友谅<sup yin="">①</sup>，友多闻，益矣。友便<sup yin="pián">pián</sup>辟<sup yin="pì">②</sup>，友善柔，友便<sup yin="pián">pián</sup>佞，损矣。”</div>
<div class="shuoming list">❶谅：诚信。❷便辟（pián pì）：习于摆架子装样子，内心却邪恶不正。❸善柔：善于阿谀奉承，内心却无诚信。❹便（pián）佞（nìng）：善于花言巧语，而言不符实。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“有益的朋友有三种，有害的朋友有三种。结交正直、守信、见多识广的朋友，就有好处。结交邪恶、伪善、巧舌如簧的朋友，就有害了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">孔子曰：“益者三乐，损者三乐。乐节礼乐<sup yin="yuè">yuè</sup>，乐道人之善，乐多贤友，益矣。乐骄乐，乐佚游<sup yin="">①</sup>，乐宴乐，损矣。”</div>
<div class="shuoming list">❶佚（yì）：同“逸”，安闲。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“有益的快乐有三种，有害的快乐有三种。以遵守规矩为快乐，以称道别人的长处为快乐，以多交有贤良的朋友为快乐，是有益的。以骄纵恣意妄为当快乐，以纵情游荡当快乐，以宴饮纵欲当快乐，是有害的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">孔子曰：“侍于君子有三愆<sup yin="">①</sup>：言未及之而言谓之躁，言及之而不言谓之隐，未见颜色而言谓之瞽<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶愆（qiān）：过失。❷瞽（gǔ）：瞎子，这里有盲目的意思。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“陪君子说话，有三种过失要注意：还没轮到你讲话你抢先说，这叫急躁；该你讲话的时候你不说，这叫隐瞒；不看对方脸色随意说话，这叫盲目。”</div>
<div class="yuanwen list fayin" onclick="run(this)">孔子曰：“君子有三戒：少之时，血气未定<sup yin="">①</sup>，戒之在色；及其壮也，血气方刚，戒之在斗；及其老也，血气既衰，戒之在得<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶未定：未成熟，未固定。❷得：泛指对于名誉、地位、钱财、女色等等的贪欲。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子有三点要避免：少年时代，血气未定，要避免迷恋情色；到了壮年，血气方刚，要避免争强好胜；等到老了，血气衰弱了，要避免倚老卖老，什么都伸手要。”</div>
<div class="yuanwen list fayin" onclick="run(this)">孔子曰：“君子有三畏<sup yin="">①</sup>：畏天命，畏大人<sup yin="">②</sup>，畏圣人之言。小人不知天命而不畏也，狎大人<sup yin="">③</sup>，侮圣人之言。”</div>
<div class="shuoming list">❶畏：怕。这里指心存敬畏。❷大人：在高位的贵族、官僚。❸狎（xiá）：狎侮，不尊重。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子有三件敬畏的事：敬畏天命，敬畏身居高位的人，敬畏圣人的话。小人不知天命，所以不敬畏天命，不尊重地位高的人，轻视圣人的话。”</div>
<div class="yuanwen list fayin" onclick="run(this)">孔子曰：“生而知之者，上也；学而知之者，次也；困而学之，又其次也；困而不学，民斯为下矣。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“生来就知道的，是上等的；学了才知道的，是次一等的；有了困惑再去学习的，是又次一等的；有了困惑也不学习，老百姓一般就是这种最下等的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">孔子曰：“君子有九思：视思明，听思聪，色思温，貌思恭，言思忠，事思敬，疑思问，忿思难<sup yin="">①</sup>，见得思义。”</div>
<div class="shuoming list">❶难（nàn）：这里指发怒可能带来的灾难、留下的后患。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子要在九个方面反省自己：有没有看清楚，有没有听明白，脸色是不是温和，态度是不是恭敬，说话有没有负责，做事合不合人心，不懂的会不会问清楚，发火时会不会考虑后果，见到可得的东西时就想得来是否合理。”</div>
<div class="yuanwen list fayin" onclick="run(this)">孔子曰：“见善如不及，见不善如探汤<sup yin="">①</sup>。吾见其人矣，吾闻其语矣。隐居以求其志，行义以达其道<sup yin="">②</sup>。吾闻其语矣，未见其人也。”</div>
<div class="shuoming list">❶探汤：把手伸到滚烫的水里。这里指要赶紧躲避开。❷达：达到，全面贯彻。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“见到好的行为唯恐自己不能做到，见到不好的行为就像把手伸进开水锅一样，我见到过这样的人，也听到过这样的话。能不受外界影响以坚持自己的原则，做对社会有利的事来实现自己的追求，我听过这样的话，没有见过这样的人。”</div>
<div class="yuanwen list fayin" onclick="run(this)">齐景公有马千驷，死之日，民无德而称焉。伯夷、叔齐饿于首阳之下，民到于今称之。其斯之谓与<sup yin="欤">①</sup>？</div>
<div class="shuoming list">❶这一句的意思与上文衔接不上，旧注多疑是缺漏或错简造成的。</div>
<div class="shiyi list fayin" onclick="run(this)">齐景公有马四千匹，到死那天，老百姓没有谁说他好的。伯夷、叔齐在首阳山下挨饿，老百姓至今都夸他好。就是说的这个吧。</div>
<div class="yuanwen list fayin" onclick="run(this)">16.13陈亢（gāng）问于伯鱼曰：“子亦有异闻乎？”对曰：“未也。尝独立，鲤趋而过庭，曰：‘学诗乎？’对曰：‘未也。’‘不学诗，无以言。’鲤退而学诗。他日又独立，鲤趋而过庭，曰：‘学礼乎？’对曰：‘未也。’‘不学礼，无以立。’鲤退而学礼。闻斯二者。”陈亢（gāng）退而喜曰：“问一得三，闻诗、闻礼，又闻君子之远其子也。”</div>
<div class="shuoming list">❶陈亢：姓陈，名亢，字子禽。伯鱼：孔子的儿子，名鲤。</div>
<div class="shiyi list fayin" onclick="run(this)">陈亢问孔子的儿子伯鱼：“你有没有（在您父亲那里）听到特别的教诲？”

伯鱼说：“没有。他曾经一个人站在庭院，看见我快步走过，就问我：‘学《诗》了吗？’我说：‘没有。’他说：‘不学《诗》，不知道怎么和人交流。’我就回去学《诗》。还有一天，他又一个人站在庭院，看见我快步走过，就问我：‘学《礼》了吗？’我说：‘没有。’他说：‘不学《礼》，不知道怎么在社会上立足。’我就回去学《礼》。只听到这两点。”

陈亢回来后高兴地说：“问一件事，有三点收获：知道要学《诗》，知道要学《礼》，还知道君子并不偏爱自己的孩子。”</div>
<div class="yuanwen list fayin" onclick="run(this)">16.14邦君之妻，君称之曰夫人，夫人自称曰小童；邦人称之曰君夫人，称诸异邦曰寡小君；异邦人称之亦曰君夫人。</div>
<div class="shuoming list">❶邦君：指诸侯国的国君。❷小童：谦称。犹说自己无知如童子。</div>
<div class="shiyi list fayin" onclick="run(this)">国君的妻子，国君尊称她为“夫人”，夫人自己谦称“小童”。本国人尊称她为“君夫人”，在外国人面前则谦称她“寡小君”。外国人称呼她，也是尊称“君夫人”。</div>

`