$.jsname__06 =`
<h3>雍也篇</h3>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“雍也，可使南面<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶南面：就是脸超南。古代以坐北朝南为尊位、正位。从君王、诸侯、将、相到地方军政长官，坐堂听政，都是面南而坐。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“冉雍啊，他可以做一方的长官。”</div>
<div class="yuanwen list fayin" onclick="run(this)">仲弓问子桑伯子<sup yin="">①</sup>。子曰：“可也，简。” 仲弓曰：“居敬<sup yin="">②</sup>而行简，以临其民，不亦可乎？居简而行简，无乃大<sup yin="太">③</sup>简乎？” 子曰：“雍之言然！”</div>
<div class="shuoming list">❶仲弓：冉雍，字仲弓。子桑伯子：人名，身世不详。❷敬：独处时严肃而恭敬。❸大：同“太”。</div>
<div class="shiyi list fayin" onclick="run(this)">仲弓问孔子桑伯子怎么样，孔子回答说：“（他办事）简要不繁琐。” 仲弓问：“以严肃认真而办事求简的态度来治理百姓，不也可以吗？但是立足于简易省事而办事求简，岂不是太简单了吗？” 孔子说：“你说的很对。”</div>
<div class="yuanwen list fayin" onclick="run(this)">哀公问：“弟子孰为好学？” 孔子对曰：“有颜回者好学，不迁怒<sup yin="">①</sup>，不贰过<sup yin="">②</sup>。不幸短命死矣。今也则亡<sup yin="无">③</sup>，未闻好学者也。”</div>
<div class="shuoming list">❶迁怒：指自己不如意时，对别人发火生气；或受了甲的气，却转移目标，拿乙去出气。❷贰：二，再一次，重复。❸亡：同“无”。</div>
<div class="shiyi list fayin" onclick="run(this)">哀公问：“在你的弟子中，谁最好学？” 孔子回答说：“有个叫颜回的好学，他从不把怒气发泄在别人身上，同样的错误从不犯两次。不幸的是短命死了！现在没有这样的人了，没听说有好学的人了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子华<sup yin="">①</sup>使于齐，冉子为其母请粟。子曰：“与之釜<sup yin="">②</sup>。”请益。曰：“与之庾<sup yin="yǔ">yǔ</sup>。”冉子与之粟五秉。子曰：“赤之适齐也，乘肥马，衣<sup yin="yì">yì</sup>轻裘。吾闻之也：君子周急不继富。”
</div>
<div class="shuoming list">❶子华：即公西华，姓公西，名赤，字子华。孔子弟子。❷釜：与下文的庾、秉，皆古代量器名。一釜，合当时的六斗四升（仅狗一人一月食用）；一庾，合当时的二斗四升；一秉，和当时的十六斛（hú），一斛为十斗。</div>
<div class="shiyi list fayin" onclick="run(this)">公西华出使到齐国了，冉有替公西华的母亲向孔子请求补贴点小米。孔子说：“给他六斗四升。” 冉有请求多给一些，孔子说：“再给她二斗四升。” 冉有却给了八十石小米。孔子说：“公西华出使到齐国，乘坐肥壮的马驾的车子，穿着轻暖的皮袍。我听说过这么一句话：‘君子救济困窘急迫的人，而不应该给富裕的人增加财富‘’’。</div>
<div class="yuanwen list fayin" onclick="run(this)">原思<sup yin="">①</sup>为之宰，与之粟九百，辞。子曰：“毋！以与尔邻里乡党<sup yin="">②</sup>乎。”</div>
<div class="shuoming list">❶原思：人名，姓原，名宪，字子思。孔子弟子。宰：官名，殷代始置，掌管家务和家奴。春秋时沿用，卿大夫总管家务的家臣，卿大夫所属私邑的长官，也都称“宰”。❷邻里乡党：都是古代居民组织的名称，五家为邻，二十五家为里，一万二千五百家为乡，五百家为党。</div>
<div class="shiyi list fayin" onclick="run(this)">原思在孔子家管家，孔子给他九百斗小米，原思推辞不肯接受。孔子说：“不要推辞了，拿去分给你的邻里同乡吧。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子谓仲弓曰：“犁牛之子骍且角<sup yin="">①</sup>。虽欲勿用，山川其舍诸<sup yin="">②</sup>？”</div>
<div class="shuoming list">❶骍（xīng）：本义是赤色马，这里只指赤色。❷本章旨意，有两种理解。一说仲弓的父亲地位低贱，而其本人却是“可使南面”的人才；此章以耕牛为喻，耕牛虽不可作为牺牲用来祭祀，但其子若狗条件用作祭祀，山川之神还是会接受的。用以说明：像仲弓这样的人才，不能因其父亲低贱而舍弃不用。一说仲弓有治民的才干，曾任季氏的总管，但他对于选贤举才标准太严，故孔子以此晓喻之。两说实有相通之处，即选用人才，一要就实论事，二要坚持标准。今从前说。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子讲到仲弓，说：“耕牛所生的一头牛犊，毛色通红，两角端正饱满，虽想不用它作祭祀，难道山川之神会舍弃（享用）它吗？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“回也，其心三月<sup yin="">①</sup>不违仁，其余则日月<sup yin="">②</sup>至焉而已矣。”</div>
<div class="shuoming list">❶三月：不是具体指三个月，而是泛指较长的时间。❷日月：一天，一月。泛指较短的时间，偶尔。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“颜回呀，他的思想长时间不离开仁，其余的人（心里想着仁德时间）不过一天或个把月那么短暂罢了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">季康子<sup yin="">①</sup>问：“仲由可使从政也与<sup yin="欤"></sup>？”子曰：“由也果，于从政乎何有？”曰：“赐<sup yin="">②</sup>也可使从政也与<sup yin="欤"></sup>？”曰：“赐也达<sup yin="">③</sup>，于从政乎何有？”曰：“求也可使从政也与<sup yin="欤"></sup>？”曰：“求也艺，于从政乎何有<sup yin="">④</sup>？”</div>
<div class="shuoming list">❶季康子：鲁国贵族，曾做鲁国正卿。❷赐：即端木赐，姓端木，名赐，字子贡。孔子弟子。❸达：通事理。❹有：困难。</div>
<div class="shiyi list fayin" onclick="run(this)">季康子问孔子说：“可以让仲由从政吗？” 孔子说：“仲由呀，办事果断，从政有什么困难呢？” 又问：“能让端木赐从政吗？” 孔子说：“端木赐呀，他能通达人情事理，从政有什么困难呢？” 又问：“能让冉求从政吗？” 孔子说：“冉求呀，他多才多艺，从政有什么困难呢？”</div>
<div class="yuanwen list fayin" onclick="run(this)">季氏使闵子骞为费<sup yin="bì">bì</sup>宰<sup yin="">①</sup>。闵子骞曰：“善为我辞焉！如有复我者，则吾必在汶<sup yin="wèn">wèn</sup>上矣<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶闵子骞：孔子弟子，姓闵，名损，字子骞。相传是有名的孝子，受到孔子的赞誉。费（bì）：邑名，故城在今山东省费县西北。❷汶（wèn）：河流名，即今山东大汶河，当时流经齐、鲁两国的交界处。</div>
<div class="shiyi list fayin" onclick="run(this)">季氏派人请闵子骞做费地的县长，闵子骞对来人说：“请替我婉言谢绝吧！要是再来说这事，那我一定逃到汶水北边（的齐国）去了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">伯牛<sup yin="">①</sup>有疾，子问之，自牖<sup yin="">②</sup>执其手，曰：“亡之，命矣夫！斯人也而有斯疾也！斯人也而有斯疾也！”</div>
<div class="shuoming list">❶伯牛：孔子弟子，姓冉，名耕，字伯牛。❷牖（yǒu）：窗户。</div>
<div class="shiyi list fayin" onclick="run(this)">伯牛得了重病，孔子去慰问他，从南窗口外握住他的手，说：“不行了，命里注定吧！这样的人竟得了这样的病！这样的人竟会得这样的病！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“贤哉，回也！一箪<sup yin="">①</sup>食，一瓢饮，在陋巷，人不堪其忧，回也不改其乐。贤哉，回也！”</div>
<div class="shuoming list">❶箪（dān）：盛饭的圆形竹器。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“多么贤德啊，颜回！一竹筒饭，一瓢水，住在破旧狭小的巷子里，别人忍受不了那（清苦带来的）忧愁，而颜回却不改变他（修学求道）的乐处。多么贤德啊，颜回！”</div>
<div class="yuanwen list fayin" onclick="run(this)">冉求曰：“非不说<sup yin="悦">①</sup>子之道，力不足也。” 子曰：“力不足者，中道而废，今女<sup yin="汝">rǔ</sup>画。”</div>
<div class="shuoming list">❶说：同“悦”，喜欢，爱慕。</div>
<div class="shiyi list fayin" onclick="run(this)">冉求说：“并不是不乐意照老师的做，实在是弟子能力有限。”孔子说：“如果能力不够，至少走到中途走不动了才停下来，而现在你是自己划定了一个界限，原地不动！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子谓子夏曰：“女<sup yin="汝">rǔ</sup>为君子儒<sup yin="">①</sup>，无为小人儒。”</div>
<div class="shuoming list">❶儒：读书的人，儒生、学者。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子对子夏说：“你要做一个有道德修养的君子式的学者，不要做缺少道德修养的小人式的学者。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子游为武城<sup yin="">①</sup>宰。子曰：“女<sup yin="汝">rǔ</sup>得人<sup yin="">②</sup>焉尔乎？”曰：“有澹台灭明者<sup yin="">③</sup>，行不由径，非公事，未尝至于偃之室也。”</div>
<div class="shuoming list">❶武城：鲁国的一个小城邑，位于今山东费县境内。❷人：人才。❸澹（tán）台灭明：武城人，姓澹台，名灭明，字子羽。后来成为孔子弟子。</div>
<div class="shiyi list fayin" onclick="run(this)">子游当武城的长官后，孔子问他：“（在这里）你发现人才了吗？” 子游说：“有个叫澹台灭明的人，（他处事行正道）走路不抄小路，不为公事从来不曾到过我屋里。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“孟之反<sup yin="">①</sup>不伐，奔而殿<sup yin="">②</sup>，将入门，策其马，曰：‘非敢后也，马不进也。’”</div>
<div class="shuoming list">❶孟之反：鲁国大夫，名侧，字反之。❷奔而殿：奔，逃跑；殿：殿后。鲁哀公十一年（公园前484年），鲁国与齐国打仗，鲁国右翼军败退时，孟之反在最后掩护。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“孟之反不喜欢自我夸耀，军队败退时，他殿后掩护，将入城门时，（故意）鞭打着马，说：‘不是我敢殿后啊，是马不肯往前跑啊！’”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“不有祝鮀之佞<sup yin="nìng">①</sup>，而有宋朝<sup yin="zhāo">②</sup>之美，难乎免于今之世矣。”</div>
<div class="shuoming list">❶祝鮀（tuó）：卫国人，字子鱼，以口才著称。佞（nìng）：有口才。❷宋朝：宋国的公子朝，以美貌著称。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“没有祝鮀的口才，仅有宋朝的美貌，在如今的世道难免要倒霉的！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“谁能出不由户<sup yin="">①</sup>？何莫由斯道<sup yin="">②</sup>也？”</div>
<div class="shuoming list">❶户：门。❷斯道：这条路。指孔子所主张的仁义之道。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“谁能出家不经过房门的？那为什么不走人生正道呢？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“质胜文则野<sup yin="">①</sup>，文胜质则史<sup yin="">②</sup>。文质彬彬<sup yin="">③</sup>，然后君子。”</div>
<div class="shuoming list">❶质：质朴的内容，内在的思想感情。孔子认为，仁义是质。文：文采，华丽的装饰，外在的礼仪。孔子认为，礼乐是文。❷史：本义是宗庙里掌礼仪的祝官，官府里掌文书的史官。这里指像“史”那样，言辞华丽，虚浮铺陈，心里并无诚意。含有浮夸虚伪的贬义。❸彬彬：文质兼备相称；文与质互相融合，配合恰当。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“质朴胜过文采，便像个（不开化的）乡下人显得粗俗野蛮；文采胜过质朴，便像个言辞浮夸的史官。文采和质朴配合恰当，才像个君子。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“人之生也直，罔<sup yin="">①</sup>之生也幸而免。”</div>
<div class="shuoming list">❶罔：欺骗，不直。此指不正直的人。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“一个人因为正直而在世上能够生存，不正直的人也活着，不过是侥幸躲避了灾祸。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“知之者，不如好<sup yin="hào">①</sup>之者；好<sup yin="hào"></sup>之者，不如乐之者。”</div>
<div class="shuoming list">❶好（hào）：喜爱。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“（对于学问、道德）懂得它的人比不上爱好它的人，爱好它的人不如以研究它为乐的人。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“中人以上，可以语<sup yin="">①</sup>上也；中人以下，不可以语上也。”</div>
<div class="shuoming list">❶语：告，讲，说。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“中等以上才智的人。可以对他讲高深的道理；中等以下才智的人，不可以对他讲高深的道理。”</div>
<div class="yuanwen list fayin" onclick="run(this)">樊迟问知<sup yin="智">zhì</sup>。子曰：“务民之义<sup yin="">①</sup>，敬鬼神而远之，可谓知<sup yin="智">zhì</sup>矣。” 问仁。曰：“仁者先难而后获，可谓仁矣。”</div>
<div class="shuoming list">❶务民之义：致力于与人事相适宜的事。务，致力，从事；义，合宜，适宜。</div>
<div class="shiyi list fayin" onclick="run(this)">樊迟问什么是智慧。孔子说：“致力于对人有益被老百姓称道德事，敬重鬼神却远离它，可以算聪明了。” 樊迟又问什么是仁德，孔子说：“仁人遇到难事抢在别人之先去做，有了成果在人之后获得，可以算是有仁德的人了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“知<sup yin="智">zhì</sup>者乐水<sup yin="">①</sup>，仁者乐山<sup yin="">②</sup>；知<sup yin="智">zhì</sup>者动，仁者静；知<sup yin="智">zhì</sup>者乐，仁者寿。”</div>
<div class="shuoming list">❶知者乐水：水流动而不板滞，随岸赋形，与智者相似，故曰。❷仁者乐山：山形巍然，屺立而不动摇，与仁者相似，故曰。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“智者喜欢水，仁者喜爱山。智者活跃，仁者沉静。智者心情愉快舒畅，仁者健康长寿。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“齐一变，至于鲁；鲁一变，至于道<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶道：这里指儒家所推崇的古代先王的治国之道。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“齐国（的政治）变革一下，就可以达到鲁国的水平。鲁国（的政治）一变革，就进而符合先王施行的仁义之道了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“觚<sup yin="">①</sup>不觚，觚哉！觚哉！”</div>
<div class="shuoming list">❶觚（gū）：一种酒器，四方有棱，一面有供手拿的“耳”，容积二升。大约孔子见到的觚已不符合觚的形制了，故发此慨叹，以批评名实相违的现象。不觚：不像觚，觚作动词。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“觚不像觚了，这怎么是觚呀？这能叫觚吗？”</div>
<div class="yuanwen list fayin" onclick="run(this)">宰我问曰：“仁者，虽告之曰：‘井有仁焉<sup yin="">①</sup>’。其从之也？” 子曰：“何为其然也？君子可逝也<sup yin="">②</sup>，不可陷也；可欺也，不可罔也<sup yin="">③</sup>。”</div>
<div class="shuoming list">❶井有仁：井里掉进一个人。❷逝：往，去。❸罔：诬罔，被无理陷害，愚弄。</div>
<div class="shiyi list fayin" onclick="run(this)">宰我问道：“一个仁者，假如有人告诉他说：‘井里有人掉下去了。’他会跟着下去吗？” 孔子说：“为什么他要这样做呢？君子可以前去并想办法（救他），却不可以跳入井；君子可以被人欺骗，却不能受人愚弄。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子博学于文，约之以礼，亦可以弗畔<sup yin="叛">①</sup>矣夫！”</div>
<div class="shuoming list">❶畔：同“叛”，违背。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子广泛学习文化知识，用礼约束自己，也就可以做到不背离君子之道了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子见南子<sup yin="">①</sup>，子路不说<sup yin="悦">yuè</sup>。夫子矢<sup yin="">②</sup>之曰：“予所否者，天厌之！天厌之！”</div>
<div class="shuoming list">❶南子：卫灵公的妻子，有淫乱行为，名声不好，但在卫国势力很大，左右着卫国的政治。❷矢：同“誓”，发誓。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子去见南子，子路不高兴。孔子对此发誓说：“我的行为要是不合礼，让上天厌弃我！让上天厌弃我！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“中庸之为德也<sup yin="">①</sup>，甚至矣乎！民鲜久矣。”</div>
<div class="shuoming list">❶中庸：“中”即中正、中和；“庸”即平常。“中庸”即“用中为常道也”。其主要特点是反对过与不及和保持对立面的和谐，是孔子学说的最高道德标准。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“中庸作为道德，算是最高层次的了！但是人们缺少中庸这种道德，已经很久了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子贡曰：“如有博施于民而能济众，何如？可谓仁乎？” 子曰：“何事于仁，必也圣乎！尧舜<sup yin="">①</sup>其犹病诸！夫仁者，己欲立而立人，己欲达而达人。能近取譬<sup yin="">②</sup>，可谓仁之方也已。”</div>
<div class="shuoming list">❶尧舜：传说中的古代部落联盟的首领，儒家把他们看作理想的圣王。❷近取譬：以自己打比方，即推已及人之意。</div>
<div class="shiyi list fayin" onclick="run(this)">子贡说：“如果有人能够对民众广施恩惠，普济患难，这个人怎么样？可以称为仁吗？”孔子说：“何止是仁啊！那一定是圣人了，连尧舜都发愁做不到啊！所谓仁者，就是自己想要站立得住，也要让别人站立得住；自己想要前途通达，也要使别人前途通达。能由自己推及到别人身上，可以说这就是实施仁道的办法了。”</div>

`