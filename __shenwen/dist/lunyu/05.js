$.jsname__05 =`
				<h3>公治长篇</h3>
			<div class="yuanwen list fayin" onclick="run(this)">子谓公冶长<sup yin="">①</sup>：“可妻<sup yin="气">②</sup>也，虽在缧绁<sup yin="">③</sup>之中，非其罪也！”以其子妻<sup yin="气"></sup>之。</div>
			<div class="shuoming list">❶公治长（cháng）：姓公治，名长。孔子弟子。❷妻（qì）。 ❸缧绁（léixiè）：捆绑罪人的绳索，借指牢狱。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说起公冶长，“可以把女儿嫁给他。他虽然在坐牢，但他并没有罪。”后来果然把自己的女儿嫁给了公冶长。</div>
			<div class="yuanwen list fayin" onclick="run(this)">子谓南容<sup yin="">①</sup>，“邦有道，不废；邦无道，免于刑戮。”以其兄之子妻<sup yin="气">qì</sup>之。</div>
			<div class="shuoming list">❶南容：姓南容，名适（kuò）,字子容。孔子弟子。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子提到南容，“国家政治清明，他不会被废弃不用；国家无道，政治黑暗，他能避免遭受刑罚。” （孔子）便把侄女嫁给了他。</div>
			<div class="yuanwen list fayin" onclick="run(this)">子谓子贱<sup yin="">①</sup>，“君子哉若人！鲁无君子者，斯焉取斯？”</div>
			<div class="shuoming list">❶子贱：姓宓（fú），名不齐，字子贱。孔子弟子。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子赞叹宓子贱：“真是君子啊这个人！假如说鲁国无君子，这个人哪里学来这么好的品德？”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子贡问曰：“赐也何如？” 子曰：“女<sup yin="汝"></sup>，器也。” 曰：“何器也？” 曰：“瑚琏<sup yin="">①</sup>也。”</div>
			<div class="shuoming list">❶瑚琏：古代宗庙中盛黍稷用的容器，很尊贵。后来用它比喻有治国才能的人。</div>
			<div class="shiyi list fayin" onclick="run(this)">子贡问道：“您对我有什么看法呢？” 孔子说：“你像一件器皿。” 子贡问：“什么器皿？” 孔子说：“就是宗庙里装黍稷的瑚琏。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">或曰：“雍<sup yin="">①</sup>也仁而不佞。” 子曰：“焉用佞<sup yin="">②</sup>？御人以口给，屡憎于人。不知其仁，焉用佞？”</div>
			<div class="shuoming list">❶雍：即冉雍，字子弓。孔子弟子。❷佞（nìng）：能言善辩，口才好。❸</div>
			<div class="shiyi list fayin" onclick="run(this)">有人说：“冉雍有仁德，可是没口才。” 孔子说：“要口才做什么？对人尖嘴利舌的，老让人讨厌。他仁不仁我不晓得，但是要口才干什么呢？”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子使漆雕开仕<sup yin="">①</sup>。对曰：“吾斯之未能信。”子说<sup yin="悦">②</sup>。</div>
			<div class="shuoming list">❶漆雕开：姓漆雕，名开，字子开。孔子弟子。❷说（yuè）：同“悦”。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子叫漆雕开去做官。漆雕开答复说：“我对这事还没有自信。”孔子听了很高兴。</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“道不行，乘桴浮于海。从我者，其由与<sup yin="欤">①</sup>？”子路闻之喜。子曰：“由也好勇过我，无所取材<sup yin="">②</sup>。”</div>
			<div class="shuoming list">❶其：语气词，表揣测。与同“欤”。❷无所取材：这句有多种解释。有认为孔子既嘉子路之勇于任事而又惜其才无取用之所。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“我的主张在这里行不通，就乘木排漂浮到海外去。能随从我去的，大概只有仲由吧！” 子路听了这话很高兴。孔子又说：“仲由呀，你的勇敢精神超过了我，（可惜）没有谁来取用你的才干。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">孟武伯问：“子路仁乎？” 子曰：“不知也。” 又问。子曰：“由也，千乘之国，可使治其赋也，不知其仁也。” “求也何如？” 子曰：“求也，千室之邑，百乘之家<sup yin="">①</sup>，可使为之宰也，不知其仁也。” “赤也何如<sup yin="">②</sup>？” 子曰：“赤也，束带立于朝，可使与宾客言也，不知其仁也。”</div>
			<div class="shuoming list">❶家：周天子或诸侯分封给卿大夫土地、人民，形成一个由卿大夫统治的政治经济实体，这叫作“家”。❷赤：即公西赤，字子华，孔子弟子。</div>
			<div class="shiyi list fayin" onclick="run(this)">孟武伯问：“子路算得上仁德吗？”孔子说：“不知道。” 孟武伯又问，孔子说：“仲由啊，千辆兵车的国家，可以派他去执掌军事工作。我不知道他是否做到了仁。” （孟武伯又问）“那，冉求怎么样？” 孔子说：“冉求嘛，千户人家的大邑，百辆兵车的大夫封地，可以派他去担任总管。至于他是否做到了仁，我就不清楚了。” （孟武伯又问）“公西赤怎么样？” 孔子说：“公西赤嘛，可以让他穿着礼服立在朝廷上，接待来宾办理交涉，我也不知道他是否做到了仁。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子谓子贡曰：“女<sup yin="汝">rǔ</sup>与回也，孰愈<sup yin="">①</sup>？” 对曰：“赐也何敢望回<sup yin="">②</sup>？回也闻一以知十，赐也闻一以知二。” 子曰：“弗如也。吾与<sup yin="">③</sup>女<sup yin="汝">rǔ</sup>弗如也。”</div>
			<div class="shuoming list">❶愈：胜过，更好，更强。❷望：比。❸与：赞同，同意。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子问子贡说：“你和颜回哪个更强？” 子贡说：“我哪里敢和颜回比？颜回听了一分道理，能从中退出十分道理；我呢，听了一分道理，只能悟出二分道理。” 孔子（慨叹）说：“比不上啊，我同意你说的比不上他啊。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">5.10宰予昼寝。子曰：“朽木不可雕也，粪土之墙不可杇<sup yin="圬">①</sup>也；于予与何诛<sup yin="">②</sup>？” 子曰：“始吾于人也，听其言而信其行；今吾于人也，听其言而观其行。于予与改是。”</div>
			<div class="shuoming list">❶杇（wū）：同“圬”，粉刷墙壁。❷与：同“欤”。在这里表停顿。诛：责备。</div>
			<div class="shiyi list fayin" onclick="run(this)">宰予大白天睡觉。孔子说：“烂木头没法雕琢，粪土墙没法粉刷，对宰予，还能用什么话来责备呢？” 孔子又说：“起先我对人是听他怎么说就相信他怎么做，现在我对人是听他怎么说又看他怎么做。是宰予这件事让我改变态度的。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“吾未见刚者。”或对曰：“申枨<sup yin="chéng">①</sup>。” 子曰：“枨<sup yin="chéng">chéng</sup>也欲，焉得刚。”</div>
			<div class="shuoming list">❶申枨（chéng）：姓申，名枨，字周。孔子弟子。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“我没见过刚强的人。” 有人就指出：“申枨是一个。”孔子说：“申枨有欲望，怎么能刚强。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子贡曰：“我不欲人之加诸<sup yin="">①</sup>我也，吾亦欲无加诸人。” 子曰：“赐也，非尔所及也。”</div>
			<div class="shuoming list">❶诸：“之于”的合音。</div>
			<div class="shiyi list fayin" onclick="run(this)">子贡说：“我不喜欢别人强加于我，我也不想强加于人。” 孔子说：“赐啊，这种境界你还没有达到。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子贡曰：“夫子之文章，可得而闻也；夫子之言性与天道<sup yin="">①</sup>，不可得而闻也。”</div>
			<div class="shuoming list">❶天道：先秦时期，“天道”通常指自然的变化及其规律，天道与人类社会吉凶祸福有密切的关系。</div>
			<div class="shiyi list fayin" onclick="run(this)">子贡说：“老师传授的有关古代文献的学问，我们可以学到领会到；老师有关人性和天道的论述，我们就没办法学到领会了。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子路有闻，未之能行，唯恐有<sup yin="又">①</sup>闻。</div>
			<div class="shuoming list">❶有：同“又”。</div>
			<div class="shiyi list fayin" onclick="run(this)">子路听了一个道理，（如果）还没有能实行它，就担心又听到新的道理。</div>
			<div class="yuanwen list fayin" onclick="run(this)">子贡问曰：“孔文子何以谓之‘文’也<sup yin="">①</sup>？” 子曰：“敏而好学，不耻下问，是以谓之‘文’也。”</div>
			<div class="shuoming list">❶孔文子：卫国大夫孔圉（yǔ），“文”是谥号，“子”是尊称。</div>
			<div class="shiyi list fayin" onclick="run(this)">子贡问道：“孔文子这个人凭什么给他‘文’的谥号？” 孔子说：“他聪敏好学，能向学问、地位比自己低的人请教而不认为羞辱，因此称他为‘文’。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子谓子产<sup yin="">①</sup>：“有君子之道四焉：其行己也恭，其事上也敬，其养民也惠，其使民也义。”</div>
			<div class="shuoming list">❶子产：名侨，字子产。春秋时郑国的贤相，著名的政治家。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子评价子产：“子产具有君子的四个方面特点：自己的言行举止庄重谦逊，侍奉君上恭敬谨慎，教养人民多给恩惠，使用民力合乎道义。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“晏平仲<sup yin="">①</sup>善与人交，久而敬之。”</div>
			<div class="shuoming list">❶晏平仲：齐国有名的大夫，做过齐景公的宰相。姓晏，名婴，字仲，“平”是谥号。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“晏平仲善于和人交往，相处越久，人家越敬重他。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“臧文仲居蔡<sup yin="">①</sup>，山节藻棁<sup yin="zhuō">②</sup>，何如其知<sup yin="智"></sup>也。”</div>
			<div class="shuoming list">❶臧文仲：鲁国大夫臧孙辰。“文”，谥号。“仲”，排行。蔡：大龟，以其产于蔡地而得名。古人用龟甲占卜，以大龟为国宝。❷节：柱头的牛拱。棁（zhuō）：屋梁上的短柱。山节藻棁，是天子祖庙的装饰，臧文仲用来装饰龟的住处，是对神的谄媚。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“臧文仲收藏一只大乌龟，（给它住的房子）斗拱上雕刻着山岳的形状，短柱上画着藻草的图案，这个人的聪明怎样呢？”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子张问曰：“令尹子文三仕为令尹<sup yin="">①</sup>，无喜色；三已之，无愠色。旧令尹之政，必以告新令尹。何如？” 子曰：“忠矣。” 曰：“仁矣乎？” 曰：“未知，焉得仁？” “崔子弑齐君<sup yin="">②</sup>，陈文子有马十乘<sup yin="shèng">③</sup>，弃而违之。至于他邦，则曰：‘犹吾大夫崔子也。’违之。之一邦，则又曰：‘犹吾大夫崔子也。’违之。何如？” 子曰：“清矣。”曰：“仁矣乎？” 曰：“未知，焉得仁？”</div>
			<div class="shuoming list">❶令尹子文：楚国宰相，姓斗，字子文。令尹，官名，春秋、战国时楚国所设，为楚国的最高官职，掌军政大权。❷崔子：之崔杼（zhù），齐国大夫。❸陈文子：齐国大夫，名须无。崔杼杀了齐庄公后，他便离开齐国，两年后又回到齐国。</div>
			<div class="shiyi list fayin" onclick="run(this)">子张问道：“令尹子文多次当令尹，脸上不显得高兴；多次被免职，脸上不显出怨恨。（每次免职时）必定把自己原有的政事告诉给新的令尹。由此看来这个人怎么样？”孔子说：“可算是忠了。”（子张）问道：“能称得上仁吗？”（孔子）说：“不知道。（就这一点来说）哪能称得上仁？”（子张又问）“崔杼犯上，杀了齐君，陈文子便舍弃了自己所有的四十匹马，离开了齐国到了别的国家。他到了另一个国家，说：‘（这里执政的人）就像我们齐国大夫崔杼。’于是离开这个国家。又到了另一个国家，他又说：‘（这里执行的人）就像我们齐国大夫崔杼。’于是又离开。这个人怎么样？”</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“可算是清白了。”（子张）问：“能称得上仁吗？” 孔子说：“不知道。（就这一点来说）哪能称得上仁呢？”</div>
			<div class="yuanwen list fayin" onclick="run(this)">季文子<sup yin="">①</sup>三思而后行。子闻之，曰：“再，斯可矣。”</div>
			<div class="shuoming list">❶季文子：鲁国大夫，姓季孙，名行父，“文”是谥号。</div>
			<div class="shiyi list fayin" onclick="run(this)">季文子凡事要反复琢磨多次才付诸行动。孔子听到后，说：“想两遍就够了。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“宁武子<sup yin="">①</sup>，邦有道，则知<sup yin="智">②</sup>；邦无道，则愚。其知<sup yin="智"></sup>可及也，其愚不可及也。”</div>
			<div class="shuoming list">❶宁武子：卫国大夫，姓宁，名俞，“武”是谥号。❷知：同“智”。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“宁武子这人，在国家政治清明时就显出聪明；国家无道时就装出愚笨的样子。他的聪明别人可以达到，他假装愚笨就没有人比得上了。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子在陈，曰：“归与<sup yin="欤"></sup>！归与<sup yin="欤"></sup>！吾党<sup yin="">①</sup>之小子狂简，斐然成章<sup yin="">②</sup>，不知所以裁<sup yin="">③</sup>之。”</div>
			<div class="shuoming list">❶与：同“欤”。吾党：我的故乡（鲁国）。古代五百家为一党。❷章：文采，文学，文章。❸裁：节制，控制，指导。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子在陈国，说：“回家吧!回家吧！我们家乡那些学子豪气冲天，简单率真，文采扬扬，我不知道怎样约束他们。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“伯夷、叔齐，不念旧恶<sup yin="">①</sup>，怨是用希<sup yin="稀">②</sup>。”</div>
			<div class="shuoming list">❶伯夷、叔齐：商纣王时，孤竹国君的两个儿子。孤竹国君死后，他们互相让位，后两人都投奔到周。到周后，反对周武王起兵伐纣。武王灭商后，两人逃避到首阳山，不食周栗而死。❷希：同“稀”，少。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“伯夷、叔齐不怀恨不记仇，怨恨他们的人就少。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“孰谓微生高<sup yin="">①</sup>直？或乞醯<sup yin="">②</sup>焉，乞诸其邻而与之。”</div>
			<div class="shuoming list">❶微生高：鲁国人，姓微生，名高。孔子学生。他跟一个女子在桥下约会，那女的没来，大水却来了，他也不逃走，最后抱着桥柱被淹死了。❷醯（xī）：醋。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“谁说微生高耿直？有人向他讨点醋，他（不直说自己没有，却暗地里）到邻居家讨了点来给那人。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“巧言，令色，足恭，左丘明<sup yin="">①</sup>耻之，丘亦耻之。匿怨而友其人，左丘明耻之，丘亦耻之。”</div>
			<div class="shuoming list">❶左丘明：鲁国的史官，有人认为就是《左传》的作者。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“花言巧语，满脸堆笑，过分恭顺，这种做法左丘明认为可耻，我也认为可耻。心里怨恨一个人，却又装得像个朋友，这种做法左丘明认为可耻，我也认为可耻。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">颜渊、季路侍<sup yin="">①</sup>。子曰：“盍<sup yin="">②</sup>各言尔志。” 子路曰：“愿车马、衣<sup yin="yì">yì</sup>轻裘，与朋友共，敝之而无憾。” 颜渊曰：“愿无伐善<sup yin="">③</sup>，无施劳<sup yin="">④</sup>。” 子路曰：“愿闻子之志。” 子曰：“老者安之，朋友信之，少者怀之。”</div>
			<div class="shuoming list">❶季路：即子路。因侍于季氏，又称季路。❷盍：何不。❸伐：夸耀，自夸。❹施：表白。</div>
			<div class="shiyi list fayin" onclick="run(this)">颜渊、季路在一旁侍奉老师。孔子问：“何不谈谈各自的志向呢？” 子路说：“愿意把车马、穿的衣服与朋友共享，用破旧了也不遗憾。” 颜渊说：“不把自己的好挂在嘴上；不表白自己的功劳。” 子路对孔子说：“想听听老师的志向。” 孔子说：“（我的志向是）使老年人能够安乐，使朋友们信任我，使年轻人怀念我。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“已<sup yin="">①</sup>矣乎，吾未见能见其过而内自讼<sup yin="">②</sup>者也。”</div>
			<div class="shuoming list">❶已：罢了，算了。后面的“矣”“乎”，都是表示绝望的感叹助词。❷讼：责备，争辩是非。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“算了吧，我没见过发现自己有过失而自我责备的。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“十室之邑<sup yin="">①</sup>，必有忠信如丘者焉，不如丘之好学也。”</div>
			<div class="shuoming list">❶十室之邑：极言其小。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“哪怕十户人家的小村子，也必定会有像我这样忠诚守信的，只是没有我这么好学罢了。”</div>
			
`