$.jsname__07 =`
<h3>述而篇</h3>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“述而不作，信而好古，窃比于我老彭<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶老彭：人名。有人认为是商代的贤大夫，有的认为指老子和彭祖两人，有人说是殷商时代的彭祖，还有人说是孔子同时代的一个人。众说纷纭，终无定论。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“传授、阐述（古代文化）而不创新，相信、热爱古代文化，我私下把自己和老彭相比。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“默而识<sup yin="志">①</sup>之，学而不厌，诲人不倦，何有于我哉？”</div>
<div class="shuoming list">❶识（zhì）：记住。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“把所学的知识默默地记住，勤奋学习永不满足，教导别人不知疲倦，（这些）对于我来说，做到了哪些呢？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“德之不修，学之不讲，闻义不能徙<sup yin="">①</sup>，不善不能改，是吾忧也。”</div>
<div class="shuoming list">❶徙（xǐ）：迁移。这里有“照着......做”的意思。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“品德不加以修养，学问不勤于研究，听了符合道义的事不能照着去做，有了错误不能改正，这些都是我忧虑的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子之燕居<sup yin="">①</sup>，申申如也<sup yin="">②</sup>，夭夭如也<sup yin="">③</sup>。</div>
<div class="shuoming list">❶燕：同“宴”，安逸，闲适。❷申申：整饬chì的样子。❸夭夭：舒畅的样子。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子在家闲居时，衣冠整洁舒展，神态安详坦然。</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“甚矣吾衰也！久矣吾不复梦见周公<sup yin="">①</sup>！”</div>
<div class="shuoming list">❶周公：姓姬，名旦，周文王之子，周武王之弟。曾辅佐周成王执政，制定了周代的礼乐制度。是孔子所崇仰的古代圣人。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“我衰老得多么严重呀！，很长时间我没有梦见周公了！”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“志于道<sup yin="">①</sup>，据于德<sup yin="">②</sup>，依于仁<sup yin="">③</sup>，游于艺<sup yin="">④</sup>。”</div>
<div class="shuoming list">❶志：立志。❷据：执行、坚守。❸依：依据。❹艺：即六艺，指礼、乐、射、御、书、数六种科目。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“立志在道上，执守在德上，依凭在仁上，游娱在艺中。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“自行束脩<sup yin="">①</sup>以上，吾未尝无诲焉。”</div>
<div class="shuoming list">❶束脩（xiū）：一束干肉（十条）。脩，干肉。古人初次见面时，带着礼物赠给对方，十条干肉是很薄的见面礼。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“（只要是）自愿送我十条以上干肉（作为见面薄礼）的，我从来没有不给予教诲的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“不愤不启<sup yin="">①</sup>，不悱不发<sup yin="">②</sup>，举一隅不以三隅反<sup yin="">③</sup>，则不复也。”</div>
<div class="shuoming list">❶愤：思考问题有疑难之处，苦思冥想，而仍然没想通，仍然领会不了的样子。❷悱：想说而不能明确地表达，说不出来的样子。❸隅：角落。比喻从已知的一点，去进行推论，由此及彼，触类旁通。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“（教导学生时）不到他苦思冥想而想不通时，不去开导他；不到心里想说而表达不出时，不去启发他。提示给他某一方面，他却不能推知出其他几个方面，我就不再去教他。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子食于有丧者之侧<sup yin="">①</sup>，未尝饱也。</div>
<div class="shuoming list">❶有丧着：有丧事的人。孔子在有丧事的人面前，因同情失去亲人的人，食欲不振，吃饭无味。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子在有丧事的人旁边吃饭，从来没有吃饱过。</div>
<div class="yuanwen list fayin" onclick="run(this)">子于是日哭<sup yin="">①</sup>，则不歌。</div>
<div class="shuoming list">❶哭：指给别人吊丧时哭泣。一日之内，由于心里悲痛，余哀未忘，就不会再唱歌了。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子在吊丧这天哭泣过，便不再在这一天唱歌。</div>
<div class="yuanwen list fayin" onclick="run(this)">子谓颜渊曰：“用之则行，舍之则藏，惟我与尔有是夫！”子路曰：“子行三军<sup yin="">①</sup>，则谁与？”子曰：“暴虎冯<sup yin="píng">píng</sup>河<sup yin="">②</sup>，死而无悔者，吾不与也。必也临事而惧，好<sup yin="hào">hào</sup>谋而成者也。”</div>
<div class="shuoming list">❶三军：周制天子六家，诸侯大国三军，一军为一万二千五百人。春秋时大国多设三军，三军之名称，各国不同，有的称中军、上军、下军，有的称中军、左军，右军、这里统称军队。❷暴虎冯（píng）河：暴虎，空手和老虎搏斗；冯河，不借助舟船涉河。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子对颜渊说：“出仕就去实行我的主张，否则就把它收藏起来，等待时机，只有我和你能做到这点罢！” 子路说：“（如果）老师统帅三军，那么找谁与您一起共事？” 孔子说：“空手打猛虎、徒步过大河，就算丢了性命也不后悔，我不同这样的人共事。与我共事的人必须是遇事警惧，善于谋划以求成功的人。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“富而可求也，虽执鞭之士<sup yin="">①</sup>，吾亦为之。如不可求，从吾所好。”</div>
<div class="shuoming list">❶虽执鞭之士：指手里拿着皮鞭的下等差役。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“如果财富可以（正当）求得的话，即使做下等差役，我也愿意担任。如果不能（正当）求得，那么我还是做我喜欢做的事。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子之所慎：齐<sup yin="斋">①</sup>，战，疾。</div>
<div class="shuoming list">❶齐：同“斋”，即斋戒。古人在祭祀前，必先整洁身心，以示虔诚。其内容包括沐浴更衣，不饮酒，不吃荤，不与妻妾同居等项，这叫斋戒。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子小心谨慎对待的事是：斋戒，战争，疾病。</div>
<div class="yuanwen list fayin" onclick="run(this)">子在齐闻《韶》<sup yin="">①</sup>，三月不知肉味，曰：“不图<sup yin="">②</sup>为乐之至于斯也！”</div>
<div class="shuoming list">❶《韶》：见《八佾篇》第二十五章注。❷不图：没料到。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子在齐国听了《韶》乐后，很长时间内连吃肉都觉得没味，说：“没想到《韶》乐美妙到这种程度。”</div>
<div class="yuanwen list fayin" onclick="run(this)">冉有曰：“夫子为卫君<sup yin="">①</sup>乎？” 子贡曰：“诺，吾将问之。” 入，曰：“伯夷，叔齐<sup yin="">②</sup>何人也？” 曰：“古之贤人也。” 曰：“怨乎？” 曰：“求仁而得仁，又何怨？” 出，曰：“夫子不为也。”</div>
<div class="shuoming list">❶卫君：指卫出公蒯辄（kuǎi zhé）。他父亲是卫灵公的太子蒯聩，曾因得罪灵公而避难到晋国。灵公死后，立蒯辄为国君。后来，晋国为了寻找机会侵略卫国，故意把蒯聩送回国去，争夺君位，遭到蒯辄拒绝。冉有所问即指此事。❷伯夷，叔齐：商纣时孤竹国君的两个儿子。父亲临终前曾定叔齐继位；父亲死后，叔齐遵循长子继位的惯例让位给伯夷，而伯夷不肯违弃父亲遗命，便逃走，叔齐也跟着一起逃走。</div>
<div class="shiyi list fayin" onclick="run(this)">冉有问子贡说：“老师会帮助卫君（争王位）吗？” 子贡说：“好，我去问问老师。” 进去问孔子说：“伯夷、叔齐是怎样的人？” 孔子说：“古代的贤人。” 子贡又问：“他们互让君位而出逃，心里怨悔吗？” 孔子说：“他们追求仁道并得到了仁道，又有什么怨恨的？” 子贡于是出来，对冉有说：“老师是不会帮助卫君的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“饭<sup yin="">①</sup>疏食，饮水，曲肱<sup yin="">②</sup>而枕之，乐亦在其中矣。不义而富且贵，于我如浮云。”</div>
<div class="shuoming list">❶饭：吃。❷肱：由肩到胳膊肘的部位，一般也泛指胳膊。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“吃粗粮，喝凉水，胳膊一弯就是枕头，乐趣也就在其中了。用不符合道义的手段而享受富贵，对我来说就像天上的浮云似的（毫不相干）。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“加我数年，卒以学《易》<sup yin="">①</sup>，可以无大过矣。”</div>
<div class="shuoming list">❶《易》：即《易经》，古代占卜用书。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“让我多活几年，到五十岁时学习《易经》，就可以不犯大的过错了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子所雅言<sup yin="">①</sup>，《诗》、《书》、执礼，皆雅言也。</div>
<div class="shuoming list">❶雅言：春秋时代各地语言并不统一，但仍有在较大范围内通行的语言，即以陕西语音为标准的“官话”，当时称为“雅言”。译文以“普通话”之意。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子会说普通话，读《诗》，读《书》，行礼，都说普通话。</div>
<div class="yuanwen list fayin" onclick="run(this)">叶<sup yin="shè">shè</sup>公<sup yin="">①</sup>问孔子于子路，子路不对。子曰：“女<sup yin="汝">rǔ</sup>奚不曰，其为人也，发愤忘食，乐以忘忧，不知老之将至云尔。”</div>
<div class="shuoming list">❶叶公：楚国大夫沈诸梁，字子高，因在叶（shè）地当长官，故称叶公。</div>
<div class="shiyi list fayin" onclick="run(this)">叶<sup yin="shè"></sup>公向子路打听孔子的为人，子路没有回答他。孔子（得知后）对子路说：“你怎么不说他这个人啊，发愤用功忘了吃饭，心境快乐忘了忧愁，连自己快要衰老了也不觉得，就这样说好了。’”</div>
<div class="yuanwen list fayin" onclick="run(this)">7.19子曰：“我非生而知之者，好古<sup yin="">①</sup>，敏以求之者也。”</div>
<div class="shuoming list">❶古：古代文化。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“我不是生来就什么都懂的人，而是喜好古代文化又勤奋学习的人。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子不语怪、力、乱、神<sup yin="">①</sup>。</div>
<div class="shuoming list">❶怪、力、乱、神：怪异、暴力、悖乱、鬼神。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子不谈怪异、暴力、悖乱、鬼神。</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“三人行，必有我师焉：择其善<sup yin="">①</sup>者而从之，其不善者而改之。”</div>
<div class="shuoming list">❶善：优点，好的方面。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“几个人一起走路，其中必定有值得我学习的。我选取他的优点去学习，对于他们的缺点，我便引以为戒加以改正。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“天生德于予，桓魋<sup yin="">①</sup>其如予何？”</div>
<div class="shuoming list">❶桓魋（tuí）：宋国的司马向魋，因是宋桓公后代，故又称桓魋。《史记·孔子世家》记载，公元前492年，孔子路过宋国，与弟子们在大树下演习礼仪。桓魋砍倒了大树，并要杀孔子，孔子在离开宋国的途中向弟子们讲了这句话。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“上天把这样的品德赋予我，桓魋又能把我怎么样？”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“二三子<sup yin="">①</sup>以我为隐乎？吾无隐乎尔。吾无行而不与二三子者，是丘也。”</div>
<div class="shuoming list">❶二三子：这里是孔子客气地称呼弟子们。“二三”表示约数。“子”是尊称。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“大家觉得我有什么瞒着没教，是吧？对你们我没什么瞒的。我没有哪件事不跟你们在一起过，这就是我。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子以四教：文<sup yin="">①</sup>，行<sup yin="">②</sup>，忠<sup yin="">③</sup>，信<sup yin="">④</sup>。</div>
<div class="shuoming list">❶文：文化知识，历史文献。❷行：道德修养，社会实践。❸忠：忠诚老实。❹信：讲信用，言行一致。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子从四个方面教导学生：典籍文献，道德实践，对人忠诚，诚实守信。</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“圣人，吾不得而见之矣；得见君子者，斯可矣。” 子曰：“善人，吾不得而见之矣！得见有恒者，斯可矣。亡<sup yin="">①</sup>无而为有，虚而为盈<sup yin="">②</sup>，约而为泰<sup yin="">③</sup>，难乎有恒矣。”</div>
<div class="shuoming list">❶亡：同“无”。❷盈：丰满，充实。❸约：穷困。泰：宽裕，豪华，奢侈。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“圣人，我不能见到了！能见到君子，就可以了。”孔子说：“善人，我不能见到了！能见到一心向善、坚持学好的，就可以了。（如果一个人）没有却假装有，空虚却假装充实，穷困却假装富足，就很难做到有恒心了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子钓而不纲<sup yin="">①</sup>，弋不射宿<sup yin="">②</sup>。</div>
<div class="shuoming list">❶纲：本意是提网的大绳。这里指在河流的水面上横着拉一根大绳，上面系有许多鱼钩以钓鱼。❷弋（yì）：用带绳的箭射鸟。这种箭发出去以后，还能靠绳收回再连续用。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子（只用鱼竿）钓鱼，却不（用带大绳的网）捕鱼；射鸟（只射飞鸟）不射栖息在巢中的鸟。</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“盖有不知而作之者，我无是也。多闻，择其善者而从之；多见而识<sup yin="zhì">①</sup>之；知之次也。”</div>
<div class="shuoming list">❶识（zhì）：记住。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“大概有自己无所知却要凭空创立新说的人，我没有这种本事。（我只是）多听听（各种见解），选择其中好的来学习；多看看，然后记在心里。这样学来的知识在智能上（比生而知之）是仅次一等的了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">互乡<sup yin="">①</sup>难与言，童子见，门人惑。子曰：“与其进也，不与其退也，唯何甚？人洁己以进，与<sup yin="yù">②</sup>其洁也，不保<sup yin="">③</sup>其往也。”</div>
<div class="shuoming list">❶互乡：地名，所在地已无可考证。❷与（yù）：赞成。❸保：拘守，计较。</div>
<div class="shiyi list fayin" onclick="run(this)">互乡的人，别人很难和他们谈话的，可是互乡有个年轻人却来拜见了孔子，孔子的门人感到很奇怪。孔子说：“赞许人家进步，不赞成人家退步，何必做得过分呢？人家洁身自好以求进步，就应当赞成他的洁净，不能老是计较他的过去。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“仁远乎哉？我欲仁，斯仁至矣。”</div>
<div class="shuoming list">❶ ❷ ❸</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“仁德离我们很远吗？我想达到仁，仁就到了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">陈司败问<sup yin="">①</sup>：“昭公知礼乎<sup yin="">②</sup>？” 孔子曰：“知礼。” 孔子退，揖巫马期而进之<sup yin="">③</sup>，曰：“吾闻君子不党<sup yin="">④</sup>，君子亦党乎？君取于吴，为同姓，谓之吴孟子<sup yin="">⑤</sup>。君而知礼，孰不知礼？” 巫马期以告。子曰：“丘也幸，苟有过，人必知之。”</div>
<div class="shuoming list">❶陈司败：陈国大夫。司败，官名。一说是齐人，姓陈，名司败。❷昭公：即鲁昭公，鲁国国君。❸巫马期：姓巫马，名施，字子期。孔子的弟子。❹党：偏袒，袒护。 ❺吴孟子：古代礼法规定“同姓不婚”。鲁国与吴国同为姬姓国家，按礼法鲁君不能娶吴国女子为婚。春秋时代国君夫人的称呼，一般是在她自己姓的前面加上她自己国家的国名。鲁昭公所娶的吴国女子，本当称“吴姬”，鲁昭公为掩饰自己违背“同姓不婚”的错误，便将这个女子改称为吴孟子。</div>
<div class="shiyi list fayin" onclick="run(this)">陈司败问孔子：“昭公懂礼吗？” 孔子说：“懂礼。” 孔子走后，陈司败对巫马期做个揖，请他上前来，对他说：“我听说君子不偏袒，莫非君子也偏袒吗？鲁君从吴国娶来夫人，夫妇同姓，不便称夫人为吴姬而称吴孟子。鲁君要是懂礼，还有谁不懂礼呢？” 巫马期后来把这话转告了孔子。孔子说：“我孔丘真是幸运，一有差错，别人一定会（指出）让我知道。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子与人歌而善，必使反<sup yin="">①</sup>之，而后和<sup yin="hè">②</sup>之。</div>
<div class="shuoming list">❶反：反复，再一次。❷和（hè）：跟随着唱，应和，唱和。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子和人唱歌，如果人家唱得好，一定请人家再唱一遍，然后自己跟着他一起唱。</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“文，莫<sup yin="">①</sup>吾犹人也，躬行君子，则吾未之有得。”</div>
<div class="shuoming list">❶莫：推测之词。大概，或者，也许。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“理论知识，也许我和普通人差不多。至于做身体力行的君子，那我还没有什么所得。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“若圣与仁，则吾岂敢！抑<sup yin="">①</sup>为之不厌，诲人不倦，则可谓云尔<sup yin="">②</sup>已矣。” 公西华曰：“正唯弟子不能学也。”</div>
<div class="shuoming list">❶抑：转折语气词。然则，抑或，或许。❷云尔：这样，如此。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“要说圣和仁，我哪里敢当。只不过努力朝这个目标去做，永不满足；教人家朝这个目标走不嫌烦，倒还算得上。”公西华说：“这正是弟子学不来的。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子疾病，子路请祷。子曰：“有诸？” 子路对曰：“有之。《诔》<sup yin="">①</sup>曰：‘祷尔于上下神祇。’” 子曰：“丘之祷久矣。”</div>
<div class="shuoming list">❶《诔》（lěi）：向鬼神祈祷的文章。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子得了重病，子路请求（代老师）祈祷。孔子问道：“有这回事吗？”子路说：“有的。《诔》文上说：‘为你向天神地神祈祷。’”孔子说：“我早就（以自己平时的作为）祈祷过了。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“奢则不孙<sup yin="逊">①</sup>，俭则固。与其不孙<sup yin="逊"></sup>也，宁固<sup yin="">②</sup>。”</div>
<div class="shuoming list">❶孙：同“逊”。❷固：鄙陋。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“奢侈就显得骄纵不谦逊，俭省了便显得固陋。与其不谦逊，宁可固陋。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子坦荡荡，小人长<sup yin="常"></sup>戚戚<sup yin="">①</sup>。”</div>
<div class="shuoming list">❶戚戚：忧愁、悲伤的样子。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子（总是）心胸宽广坦坦荡荡，敢做敢当；小人患得患失，唉声叹气。”</div>
<div class="yuanwen list fayin" onclick="run(this)">子温而厉<sup yin="">①</sup>，威而不猛，恭而安。</div>
<div class="shuoming list">❶厉：严肃。</div>
<div class="shiyi list fayin" onclick="run(this)">孔子温和而又严肃，威严却不凶猛，庄重而又安详。</div>

`