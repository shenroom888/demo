$.jsname__04 =`
			<h3>里仁篇</h3>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“里<sup yin="">①</sup>仁为美。择不处<sup yin="">②</sup>仁，焉得知<sup yin="智">③</sup>？”</div>
			<div class="shuoming list">❶里：邻里。周制，五家为邻，五邻（二十五家）为里。这里用动作词，居住。 ❷处：居住，在一起相处。 ❸焉：怎么，哪里，哪能。知：同“智”。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“人如果能居住在有仁德的地方才算好。选择不行仁道的地方居住，哪能算是聪明呢？”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“不仁者不可以久处约<sup yin="">①</sup>，不可以长处乐。仁者安仁，知<sup yin="智">②</sup>者利仁。”</div>
			<div class="shuoming list">❶约：穷困。❷知：同“智”。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“不仁的人不可以长久忍受穷困，不可以长久享受富贵。仁者靠仁安身立命，智者用仁名利双收。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“唯仁者能好<sup yin="号">①</sup>人，能恶<sup yin="悟">②</sup>人。”</div>
			<div class="shuoming list">❶好（hào）：喜爱，喜欢。 ❷恶（wù）：厌恶，讨厌。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“唯有仁者能够真心喜欢人的优点，真心讨厌人的缺点。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“苟<sup yin="">①</sup>志于仁矣，无恶<sup yin="悟">②</sup>也。”</div>
			<div class="shuoming list">❶苟：假如，如果。志：立志。 ❷恶（wù）：坏，坏事。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“一旦真下决心做好人，就不会有使人厌恶的事情发生了。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“富与贵，是人之所欲也；不以其道得之，不处也。贫与贱，是人之所恶也；不以其道得之<sup yin="">①</sup>，不去也。君子去仁，恶乌乎成名？君子无终食之间违仁，造次必于是，颠沛必于是。”</div>
			<div class="shuoming list">❶得之：杨伯峻说“得之”应该改为“去之”。可从。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“生活富裕，地位显贵，是人人愿意的，但若不是从正道得来，君子不会要的；生活贫穷，地位低贱，是人人讨厌的，但若不是凭正道摆脱，君子不会干的。君子而失去仁德，怎么能称作君子呢？君子连一顿饭功夫都不违背仁德，匆忙紧急时一定这样，遭遇不顺时也一定这样。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“我未见好仁者，恶不仁者。好仁者，无以尚<sup yin="">①</sup>之；恶不仁者，其为仁矣，不使不仁者加乎其身。有能一日用其力于仁矣乎？我未见力不足者。盖<sup yin="">②</sup>有之矣，我未之见也。”</div>
			<div class="shuoming list">❶尚：超过。 ❷盖：语气词。表示肯定的语气。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“我没见过喜欢仁德的人，也没见过讨厌不仁德的人。喜欢仁德的人，觉得没有什么可以超越对仁德的爱；厌恶不仁的人，他行仁德，是不让不仁德的东西沾染到自己身上，对自己有不好的影响。有没有人能够下一天功夫真正实践仁德呢？我没见过实行仁德而力气不够的人。或许有这种情况吧，只是我不曾见过。</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“人之过也，各于其党。观过，斯知仁<sup yin="">①</sup>矣。”</div>
			<div class="shuoming list">❶仁：同“人”。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“人的过错，总是和他同类的人所犯的错误是一样的。考察一个人所犯的错误，就能了解他属于哪类人。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“朝闻<sup yin="">①</sup>道，夕死可矣。”</div>
			<div class="shuoming list">❶闻：听到，知道，懂得。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“早晨明晓了道理，纵然当晚死去也值得了。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“士<sup yin="">①</sup>志于道，而耻恶衣恶食者，未足与议也。”</div>
			<div class="shuoming list">❶士：读书人，一般的知识分子，小官吏。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“一个读书人如果有志于探索真理，却又因为穿得破、吃得差而羞耻，那还不是跟他讨论大道理的时候。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子之于天下也，无适也，无莫也<sup yin="">①</sup>，义之与比<sup yin="">②</sup>。”</div>
			<div class="shuoming list">❶无适也，无莫也：无可无不可，没有一成不变的。适：可以。莫：不可以。 ❷义之与比：与义靠近，向义靠拢，也就是“与义比之”。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子对于天下的事物，没有必须怎样做，也没有一定不能怎么做，要看它与义接近的程度，怎样适合情理就怎么去做。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子怀德，小人怀土；君子怀刑<sup yin="">①</sup>，小人怀惠。”</div>
			<div class="shuoming list">❶：刑：指法度，典范。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子关注德行，小人关心土地；君子关注法度，小人关心好处。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“放<sup yin="仿">①</sup>于利而行，多怨。”</div>
			<div class="shuoming list">❶放（fǎng）：同“仿”，仿照，仿效，依照。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“唯利是图，（由于欲望总是难以满足心中）怨气就多。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“能以礼让<sup yin="">①</sup>为国乎？何有！不能以礼让为国，如礼何？”</div>
			<div class="shuoming list">❶礼让：按照周礼，注重礼仪和谦让。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“能够以礼让治国吗？那是不难做到的！不能以礼让治国，搞那些礼节又有什么用呢？”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“不患无位，患所以立<sup yin="">①</sup>；不患莫己知，求为可知也。”</div>
			<div class="shuoming list">❶立：站得住脚，有职位，在社会有立足之地。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“不担心没职位，只担心在位没本事；不发愁没人了解自己，只发愁没什么本事让人了解。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“参<sup yin="深"></sup>乎！吾道一以贯之。”曾子曰：“唯。”子出，门人问曰：“何谓也？”曾子曰：“夫子之道，忠恕而已矣<sup yin="">①</sup>！”</div>
			<div class="shuoming list">❶忠恕而已矣：孔子提倡仁，要求退己及人，设身处地为他人着想，忠、恕都体现了这种精神。不过“忠”是从积极方面来说，要求为人着想，替人做事尽心尽力；“恕”是从消极方面来说，要求对别人能将心比心，宽厚体谅。孔子所说的“已欲立而立人，已欲达而达人”“已所不欲，勿施于人”，可分别看作是对“忠、恕”的注释。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“曾参<sup yin="身"></sup>呀！我的思想可以用一个观点来贯穿。” 曾子说：“是的”。 孔子离开后，其他弟子问（曾子）：“所说的观点是什么呀？” 曾子说：“我们老师的思想，（贯穿着）忠和恕罢了。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“君子喻<sup yin="">①</sup>于义，小人喻于利。”</div>
			<div class="shuoming list">❶喻：知道，明白，懂得。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子明白的是道义，小人懂得的是私利。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“见贤思齐<sup yin="">①</sup>焉，见不贤而内自省<sup yin="醒">②</sup>也。”</div>
			<div class="shuoming list">❶齐：平等，向...看齐。 ❷省：反省，内省，检查自己的思想行为。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“看见贤人就想着向他看齐，看见不贤的就心中反省自己（检查自己有没有和他一样的错误）。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“事父母几<sup yin="姬">①</sup>谏，见志不从，又敬不违，劳而不怨。”</div>
			<div class="shuoming list">❶几（jī）：委婉，轻微。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“好好侍奉父母，提建议要恭敬柔和；假如父母一时不乐意接受，态度仍然要恭敬，虽不轻易放弃自己建议的初衷，却仍然精心服侍，毫无怨言。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“父母在，不远游<sup yin="">①</sup>，游必有方<sup yin="">②</sup>。”</div>
			<div class="shuoming list">❶游：离家出游。如“游学”“游宦”。 ❷游必有方：指让父母知道所游的确定地方，而不要无固定地方随处漂泊，致使父母挂念担心。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“父母在世时，（子女）不要长时间离家远行，（如果不得已）要离家远行，必须要有一定的去向。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“三年无改于父之道，可谓孝矣。”</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“如果三年内不改变他父亲生前的道德规范，就称得上孝了。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“父母之年，不可不知也。一则以喜，一则以惧<sup yin="">①</sup>。”</div>
			<div class="shuoming list">❶惧：父母年纪大了就必然日益衰老、接近死亡，故忧惧担心。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“父母的年纪，是不能不记得的，一方面（因他们健康长寿而）高兴，一方面（又为他们的日益衰老而）担忧。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“古者言之不出，耻躬之不逮<sup yin="">①</sup>也。”</div>
			<div class="shuoming list">❶逮：赶上。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“古人不轻易开口说话，是因为有耻辱感，怕自己说了做不到。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“以约失<sup yin="">①</sup>之者鲜矣！”</div>
			<div class="shuoming list">❶失：过失，犯错误。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“因为约束自己而犯过失的人太少啦！”</div>
			<div class="yuanwen list fayin" onclick="run(this)">4.24子曰：“君子欲讷<sup yin="">①</sup>于言而敏于行。”</div>
			<div class="shuoming list">❶讷（nè）：本义是说话言语迟钝。这里指说话谨慎，留有分寸。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“君子说话应该谨慎郑重，做事应该勤奋敏捷。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子曰：“德不孤，必有邻<sup yin="">①</sup>。”</div>
			<div class="shuoming list">❶邻：邻人，邻居。这里指思想品格一致，志向相同，能共同合作的人。</div>
			<div class="shiyi list fayin" onclick="run(this)">孔子说：“有道德的人不会孤单的，必定会有人同他为伴。”</div>
			<div class="yuanwen list fayin" onclick="run(this)">子游曰：“事君数<sup yin="烁">①</sup>，斯辱矣；朋友数<sup yin="烁"></sup>，斯疏矣。”</div>
			<div class="shuoming list">❶数（shuò）：屡次。</div>
			<div class="shiyi list fayin" onclick="run(this)">子游道：“侍奉君主，如果劝说太频繁，就容易遭到侮辱；劝朋友太多，也容易被朋友疏远。”</div>

`