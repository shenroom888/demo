
	//1.获取设备信息的md5加密信息(设备信息可随意)
	function SignatureNonce(){
		//机器：使用 nodejs 中自带os模块 const os = require('os'); const machine = os.hostname();
		var machine = 'DESKTOP-UL9OE0S';  
		//进程id： nodejs中直接获取 const pid = process.pid;
		var pid = 16076;
		//随机数
		var val = Math.floor(Math.random() * 1000000000000);
		//计数器
		var counter = 0;
		//生成uid
		var uid = machine+pid+val+counter;
		/*
		MD5加密 使用 nodejs 中自带crypto模块, 
		const crypto = require('crypto');
		var shasum = crypto.createHash('md5');
		shasum.update(uid);
		var md5str = shasum.digest('hex');
		*/
		var md5str = hex_md5(uid);
	    return md5str;
	}
	//2.获取时间字符串
	function timestamp() {
	  var date = new Date();
	  var YYYY = date.getUTCFullYear();
	  var MM = ('00'+(date.getUTCMonth() + 1)).substr(-2,2);
	  var DD = ('00'+date.getUTCDate()).substr(-2,2);
	  var HH = ('00'+date.getUTCHours()).substr(-2,2);
	  var mm = ('00'+date.getUTCMinutes()).substr(-2,2);
	  var ss = ('00'+date.getUTCSeconds()).substr(-2,2);
	  // 删除掉毫秒部分
	  return YYYY+'-'+MM+'-'+DD+'T'+HH+':'+mm+':'+ss+'Z';
	}
	//3.字符转码
	function encode(str) {
	  var result = encodeURIComponent(str);
	  return result.replace(/!/g, '%21')
	    .replace(/'/g, '%27')
	    .replace(/\(/g, '%28')
	    .replace(/\)/g, '%29')
	    .replace(/\*/g, '%2A');
	}
	//4.将对象转成2维数组
	function normalize(params) {
	  var list = [];
	  var keys = [];
	  for(var k in params){
	  	keys.push(k)
	  }
	  keys.sort();	          //必须要排序
	  for (var i = 0; i < keys.length; i++) {
	    var key = keys[i];
	    var value = params[key];
	    list.push([encode(key), encode(value)]);
	  }
	  return list;
	}
	//5.拼接参数用于GET请求
	function canonicalize(normalized) {
	  var fields = [];
	  for (var i = 0; i < normalized.length; i++) {
	    var [key, value] = normalized[i];
	    fields.push(key + '=' + value);
	  }
	  return fields.join('&');
	}

	//获取 token 封装
	function _getToken(obj,callback){
	  var AccessKeyId = obj.AccessKeyId;
	  var accessKeySecret = obj.accessKeySecret;
	  var endpoint = obj.endpoint
	  var apiVersion = obj.apiVersion

	  var data = {
	    AccessKeyId: AccessKeyId || 'LTAI4GA8EJzG1YRBjAon8ZnZ',
	    Action:'CreateToken',
	    Format:'JSON',
	    SignatureMethod:'HMAC-SHA1',
	    SignatureVersion:'1.0', 
	    SignatureNonce: SignatureNonce(),
	    Timestamp:timestamp(),
	    Version:'2019-02-28',
	  }

	  var normalized = normalize(data);
	  var canonicalized = canonicalize(normalized);
	  var stringToSign = 'GET&'+encode('/')+'&'+encode(canonicalized);
	  var key = accessKeySecret+'&';    //'K7N3clREGMJZKaGNCqDtW4uVLkGGzR' + '&';
	  var Signature = b64_hmac_sha1(key, stringToSign);
	  normalized.push(['Signature', encode(Signature+'=')]);
	  var url = endpoint+'/?'+canonicalize(normalized);

	  var xhr = new XMLHttpRequest();
	  xhr.onload = function(e){
	    var res = e.target.response
	    if(callback) callback(JSON.parse(res));
	  }
	  xhr.onerror = function(er){
	    if(callback) callback(er)
	  }
	  xhr.open('get',url);
	  xhr.send()
	}

	function getToken(obj,callback){
		var _token = localStorage._token;
		try{
			_token = JSON.parse(_token);
			if(_token.accessKeyId != obj.accessKeyId) throw 1;
			var res = _token.res;
			var time = res.Token.ExpireTime;
			if(Date.now()/1000 - time > 0) throw 2;
			if(callback) callback(res);
		}catch(err){
			_getToken(obj, function(res){
				var _res = {
					res:res,
					accessKeyId:obj.accessKeyId
				}
				localStorage._token = JSON.stringify(_res);
				if(callback) callback(res);
			})	
		}
	}

	function getUrl(text){
		var args = {
			accessKeyId: 'LTAI4GA8EJzG1YRBjAon8ZnZ',
			accessKeySecret: 'K7N3clREGMJZKaGNCqDtW4uVLkGGzR',
			endpoint: 'http://nls-meta.cn-shanghai.aliyuncs.com',
			apiVersion: '2019-02-28'	
		}
		var appkey = "yGjmWIi96ZsVQmrz";
		var token = ''
			getToken(args,function(res){
				
				token = res.Token.Id;
			})
	
		var str = 'appkey='+appkey
					+'&token='+token
					+'&text='+text         //文本内容
					+'&format='+'wav'      //pcm、wav、mp3，默认是 pcm
					+'&sample_rate='+16000 //16000Hz、8000Hz，默认是16000Hz
					+'&voice='+'xiaoyun'   //发音人，默认是xiaoyun
					+'&volume='+100        //音量，范围是0~100，默认50
					+'&speech_rate='+-100; //语速，范围是-500~500，默认是0
					+'&pitch_rate='+0;     //语调，范围是-500~500，默认是0

		 return "https://nls-gateway.cn-shanghai.aliyuncs.com/stream/v1/tts?"+str;
	}



